package parameters;

import java.util.HashMap;
import java.util.Map;

import dataset.Dataset;

public final class ParametersHyrecCork implements Parameters {


	private final int k;
	private final int B;
	private final String dataName;
	private final Dataset dataset_approx;
	private final Dataset dataset_exact;
	
	private int iterations=30;
	private final int nb_proc;
	private int r=0;
	private double delta = 0.001;
	
	public ParametersHyrecCork(int k, int B, String dataName, Dataset dataset_exact, Dataset dataset_approx, int nb_proc) {
		this.k = k;
		this.B = B;
		this.dataName = dataName;
		this.dataset_exact = dataset_exact;
		this.dataset_approx = dataset_approx;
		this.nb_proc = nb_proc;
	}
	
	public int k() {
		return k;
	}
	
	public int B() {
		return B;
	}
	
	public Dataset dataset_approx() {
		return dataset_approx;
	}
	
	public Dataset dataset_exact() {
		return dataset_exact;
	}
	
	public int nb_proc() {
		return nb_proc;
	}
	
	public void set_iterations(int iterations) {
		this.iterations = iterations;
	}
	
	public int iterations() {
		return iterations;
	}
	
	public void set_r(int r) {
		this.r = r;
	}
	
	public int r(){
		return r;
	}
	
	public void set_delta(double delta) {
		this.delta = delta;
	}
	
	public double delta() {
		return delta;
	}

	
//	public String ParametersToString() {
//		String s = "{";
//		Map<String,String> map = ParametersToMap();
//		boolean b = true;
//		for(String name: map.keySet()) {
//			if (b) {
//				s = s + name + "," + map.get(name);
//				b = false;
//			}
//			else {
//				s = s + ",\n" + name + "," + map.get(name);
//			}
//		}
//		s = s + "}";
//		return s;
//	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetName, dataName);
		dic.put(util.Names.k, Integer.toString(k));
		dic.put(util.Names.k+"_", Integer.toString(B));
		dic.put(util.Names.nb_proc, Integer.toString(nb_proc));
		dic.put(util.Names.iter, Integer.toString(iterations));
		dic.put(util.Names.r, Integer.toString(r));
		dic.put(util.Names.delta, Double.toString(delta));
		return dic;
	}
	
	@Override
	public Map<String,String> DatasetToMap() {
		Map<String,String> map = alterMapParameters(dataset_approx.ParametersToMap());
		map.putAll(dataset_exact.ParametersToMap());
		return map;
	}
	@Override
	public String DatasetToString() {
		Map<String,String> map = alterMapParameters(dataset_approx.ParametersToMap());
		map.putAll(dataset_exact.ParametersToMap());
		return Parameters.ParametersToString(map);
	}
	

	private final static Map<String,String> alterMapParameters(Map<String,String> map) {
		Map<String,String> new_map = new HashMap<String,String>();
		for (String param: map.keySet()) {
			if(!param.equals(util.Names.datasetName))
				new_map.put(param+"_", map.get(param));
		}
		return new_map;
	}
}
