package parameters;

import java.util.HashMap;
import java.util.Map;

import dataset.Dataset;

public final class ParametersHyrec implements Parameters {


	private final int k;
	private final String dataName;
	private final Dataset dataset;
	
	private int iterations=30;
	private final int nb_proc;
	private int r=0;
	private double delta = 0.001;
	
	public ParametersHyrec(int k, String dataName, Dataset dataset, int nb_proc) {
		this.k = k;
		this.dataName = dataName;
		this.dataset = dataset;
		this.nb_proc = nb_proc;
	}
	
	public int k() {
		return k;
	}
	
	public Dataset dataset() {
		return dataset;
	}
	
	public int nb_proc() {
		return nb_proc;
	}
	
	public void set_iterations(int iterations) {
		this.iterations = iterations;
	}
	
	public int iterations() {
		return iterations;
	}
	
	public void set_r(int r) {
		this.r = r;
	}
	
	public int r(){
		return r;
	}
	
	public void set_delta(double delta) {
		this.delta = delta;
	}
	
	public double delta() {
		return delta;
	}

	
//	public String ParametersToString() {
//		String s = "{";
//		Map<String,String> map = ParametersToMap();
//		boolean b = true;
//		for(String name: map.keySet()) {
//			if (b) {
//				s = s + name + "," + map.get(name);
//				b = false;
//			}
//			else {
//				s = s + ",\n" + name + "," + map.get(name);
//			}
//		}
//		s = s + "}";
//		return s;
//	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetName, dataName);
		dic.put(util.Names.k, Integer.toString(k));
		dic.put(util.Names.nb_proc, Integer.toString(nb_proc));
		dic.put(util.Names.iter, Integer.toString(iterations));
		dic.put(util.Names.r, Integer.toString(r));
		dic.put(util.Names.delta, Double.toString(delta));
		return dic;
	}
	
	@Override
	public Map<String,String> DatasetToMap() {
		return dataset.ParametersToMap();
	}
	@Override
	public String DatasetToString() {
		return dataset.ParametersToString();
	}
	
}
