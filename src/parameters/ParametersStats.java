package parameters;

import java.util.LinkedList;
import java.util.List;

public final class ParametersStats {
	
	public static boolean measure_time; //final?
	public static boolean measure_scanrate; //final?
	
	private long time = 0;
	private long nb_comp = 0; //total scanrate
	private List<Long> iter = new LinkedList<Long>(); //total scanrate at each iteration
	private List<Long> iter_changes = new LinkedList<Long>(); //nb of changes in the KNN at each iteration (restart from 0 at each iter)

	
	
	private long[] time_proc;
	private long time_creating_bucket=0;
	private long time_filling_bucket=0;
	private long time_distribution_bucket=0;
	private long time_merging_bucket=0;
	
	//Time duration of the comparisons ?

	
	public ParametersStats(boolean time, boolean scanrate) {
		ParametersStats.measure_time = time;
		ParametersStats.measure_scanrate = scanrate;
	}

	
	public boolean has_time() {
		return ParametersStats.measure_time;
	}
	public void init_time() {
		this.time = 0;
	}
	public void init_time(long time_) {
		this.time = time_;
	}
	public void increase_time(long time_) {
		this.time = this.time + time_;
	}
	public long get_time() {
		return this.time;
	}


	
	public boolean has_scanrate() {
		return ParametersStats.measure_scanrate;
	}
	public void init_scanrate() {
		this.nb_comp = 0;
	}
	public void init_scanrate(long nb_comp) {
		this.nb_comp = nb_comp;
	}
	public long get_scanrate() {
		return nb_comp;
	}
	public void increase_scanrate(long nb) {
		nb_comp = nb_comp + nb;
	}
	public void inc() {
		nb_comp++;
	}
	
	
	public void add_iter() {
		iter.add(nb_comp);
	}
	public List<Long> get_iter() {
		return iter;
	}
	
	
	public void add_iter_changes(long changes) {
		iter_changes.add(changes);
	}
	public List<Long> get_iter_changes() {
		return iter_changes;
	}

	public String TimeToString() {
		return ("{\"" + util.Names.time + "\": " + Long.toString(time) + "}");
	}

	public String ScanRateToString() {
		return ("{\"" + util.Names.nb_comp + "\": " + Long.toString(nb_comp) + "}");
	}
	
	public String CompByIterToString() {
		String s = "[";
		
		boolean b = true;
		for(int i = 0; i < iter.size(); i++) {
			if (b) {
				s = s + "\n{\""+Integer.toString(i)+"\": " + Long.toString(iter.get(i))+"}";
				b = false;
			}
			else {
				s = s + ",\n{\""+Integer.toString(i)+"\": " + Long.toString(iter.get(i))+"}";
			}
		}
		
		s = s + "\n]";
		return s;
	}
	
	public String ChangesByIterToString() {
		String s = "[";
		
		boolean b = true;
		for(int i = 0; i < iter_changes.size(); i++) {
			if (b) {
				s = s + "\n{\""+Integer.toString(i+1)+"\": " + Long.toString(iter_changes.get(i))+"}";
				b = false;
			}
			else {
//				System.out.println(s);
//				System.out.println(iter.size());
//				System.out.println(iter_changes.size());
//				System.out.println(i);
//				System.out.println(iter);
//				System.out.println(iter_changes);
				s = s + ",\n{\""+Integer.toString(i+1)+"\": " + Long.toString(iter_changes.get(i))+"}";
			}
		}
		
		s = s + "\n]";
		return s;
	}

	public void init_time_proc(int nb_proc) {
		time_proc = new long[nb_proc];
		for(int i=0; i<nb_proc;i++) {
			time_proc[i]=0;
		}
	}
	public void set_time_proc(int proc, long time) {
		time_proc[proc]=time;
	}
	
	public boolean has_time_proc() {
		return time_proc != null;
	}

	public String TimeNbProcToString() {
		String s = "[";
		
		boolean b = true;
		for(int i = 0; i < time_proc.length; i++) {
			if (b) {
				s = s + "\n{\""+Integer.toString(i)+"\": " + Long.toString(time_proc[i])+"}";
				b = false;
			}
			else {
				s = s + ",\n{\""+Integer.toString(i)+"\": " + Long.toString(time_proc[i])+"}";
			}
		}
		
		s = s + "\n]";
		return s;
	}
	
	
	public void set_time_creating_bucket(long time) {
		this.time_creating_bucket = time;
	}
	public void set_time_filling_bucket(long time) {
		this.time_filling_bucket = time;
	}
	public void set_time_merging_bucket(long time) {
		this.time_merging_bucket = time;
	}
	
	public long get_time_creating_bucket() {
		return this.time_creating_bucket;
	}
	public long get_time_filling_bucket() {
		return this.time_filling_bucket;
	}
	public long get_time_merging_bucket() {
		return this.time_merging_bucket;
	}

	public void set_time_distribution_bucket(long time) {
		this.time_distribution_bucket = time;
	}
	public long get_time_distribution_bucket() {
		return this.time_distribution_bucket;
	}


	public String timeBucketToString() {
		String s = "{\n";
		
		s = s + "creating_time: " + Long.toString(time_creating_bucket) + "\n";
		s = s + "filling_time: " + Long.toString(time_filling_bucket) + "\n";
		s = s + "distribution_time: " + Long.toString(time_distribution_bucket) + "\n";
		s = s + "merging_time: " + Long.toString(time_merging_bucket) + "\n";
		
		s = s + "}";
		return s;
	}
	
	
}
