package parameters;

import java.util.HashMap;
import java.util.Map;

import dataset.Dataset;

public final class ParametersBruteForce implements Parameters {

	private final int k;
	private final String dataName;
	private final Dataset dataset;
	
	private final int nb_proc;
	
	public ParametersBruteForce(int k, String dataName, Dataset dataset, int nb_proc) {
		this.k = k;
		this.dataName = dataName;
		this.dataset = dataset;
		this.nb_proc = nb_proc;
	}
	
	public int k() {
		return k;
	}
	
	public Dataset dataset() {
		return dataset;
	}
	
	public int nb_proc() {
		return nb_proc;
	}
	
//	public String ParametersToString() {
//		String s = "{";
//		Map<String,String> map = ParametersToMap();
//		boolean b = true;
//		for(String name: map.keySet()) {
//			if (b) {
//				s = s + name + ": " + map.get(name);
//				b = false;
//			}
//			else {
//				s = s + ",\n" + name + ": " + map.get(name);
//			}
//		}
//		s = s + "}";
//		return s;
//	}

	@Override
	public final Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetName, dataName);
		dic.put(util.Names.k, Integer.toString(k));
		dic.put(util.Names.nb_proc, Integer.toString(nb_proc));
		return dic;
	}
	
	@Override
	public final Map<String,String> DatasetToMap() {
		return dataset.ParametersToMap();
	}
	@Override
	public final String DatasetToString() {
		return dataset.ParametersToString();
	}
}
