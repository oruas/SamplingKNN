package matrixFactorization;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class MatrixFactorization {

	private final double[][] U;
	private final double[][] V;
	
	private final double[] bias_users;
	private final double[] bias_items;
	
	private final int[] ratings_number_users;
	private final int[] ratings_number_items;
	
	private final double mu;
	
	private final int k;
	
	private final double delta;
	private final double min_error;
	private final double gamma;
	
	private final Set<Ratings> ratings;
	
	public MatrixFactorization(String fileName, int k, int nb_users, int nb_items, double delta, double gamma, double min_error) throws IOException {
		this.k = k;
		this.delta = delta;
		this.gamma = gamma;
		this.min_error = min_error;
		U = new double[nb_users][k];
		V = new double[nb_items][k];
		bias_users = new double[nb_users];
		bias_items = new double[nb_items];
		ratings_number_users = new int[nb_users];
		ratings_number_items = new int[nb_items];
		for(int row_index = 0; row_index < nb_users; row_index++) {
			for(int column_index = 0; column_index < k; column_index++) {
				U[row_index][column_index] = 1;
			}
			bias_users[row_index] = 0;
			ratings_number_users[row_index] = 0;
		}
		for(int row_index = 0; row_index < nb_items; row_index++) {
			for(int column_index = 0; column_index < k; column_index++) {
				V[row_index][column_index] = 1;
			}
			bias_items[row_index] = 0;
			ratings_number_items[row_index] = 0;
		}
		ratings = new HashSet<Ratings>();
		
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		while ((ligne=io1.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			String[] rating_results = ligne.split(util.Names.datasetSplit);
			int user = Integer.parseInt(rating_results[0]);
			int item = Integer.parseInt(rating_results[1]);
			double rating = (Double.parseDouble(rating_results[2]));
			ratings.add(new Ratings(user,item,rating));
		}
		io1.close();
		
		double mean = 0;
		for(Ratings rating: ratings) {
			mean = mean + rating.getR();
//			bias_users[rating.getU()] = bias_users[rating.getU()] + (rating.getR() + rating.getR());
//			bias_items[rating.getI()] = bias_items[rating.getI()] + (rating.getR() + rating.getR());
		}
		mu = mean / ((double) ratings.size());
		double mu_square = mu * mu;
		for(Ratings rating: ratings) {
			bias_users[rating.getU()] = bias_users[rating.getU()] + (rating.getR() + rating.getR() - mu_square);
			bias_items[rating.getI()] = bias_items[rating.getI()] + (rating.getR() + rating.getR() - mu_square);
			ratings_number_users[rating.getU()] = ratings_number_users[rating.getU()] + 1;
			ratings_number_items[rating.getI()] = ratings_number_items[rating.getI()] + 1;
		}
		for(int index = 0; index < nb_users; index++) {
			bias_users[index] = Math.sqrt(bias_users[index] / ((double) ratings_number_users[index]));
		}
		for(int index = 0; index < nb_items; index++) {
			bias_items[index] = Math.sqrt(bias_items[index] / ((double) ratings_number_items[index]));
		}
		
		SGD();
	}
	
	

	
	public final double get_rating(int u, int i) {
		double score = mu + bias_users[u] + bias_items[i] + get_rating_without_bias(u,i);
		return score;
	}
	
	public final double get_rating_without_bias(int u, int i) {
		double score = 0;
		for(int index = 0; index < k; index++) {
			score = score + U[u][index] * V[i][index];
		}
		return score;
	}
	
	private final double get_error(Ratings r) {
		return (r.getR() - get_rating(r.getU(),r.getI()));
	}
	
	
	private final void SGD() {
		while(!stop()) {
			for(Ratings rating: ratings) {
				double error = get_error(rating);
				modify(rating.getU(), rating.getI(), error);
			}
		}
	}
	
	
	private final void modify(int u, int i, double error){
		for(int index = 0; index < k; index++) {
			double p = U[u][index];
			double q = V[i][index];
			U[u][index] = p + gamma * ((error * q) + (delta * p));
			V[i][index] = q + gamma * ((error * p) + (delta * q));
		}
	}
	
	
	private final boolean stop() {
		return (get_errors_ratings() + (delta * get_norms()) < min_error);
	}
	
	
	
	private final double get_errors_ratings() {
		double error = 0;
		for(Ratings rating: ratings) {
			double local_error = get_error(rating);
			error = error + (local_error * local_error);
		}
		return error;
	}
	
	
	private final double get_norms() {
		return (get_norm_square_U() + get_norm_square_V() + get_norm_square_bias_users() + get_norm_square_bias_items());
	}
	
	private final double get_norm_square_U(){
		double norm = 0;
		int nb_users = U.length;
		for(int row_index = 0; row_index < nb_users; row_index++) {
			for(int column_index = 0; column_index < k; column_index++) {
				norm = norm + (U[row_index][column_index]*U[row_index][column_index]);
			}
		}
		return norm;
	}
	
	private final double get_norm_square_V(){
		double norm = 0;
		int nb_users = V.length;
		for(int row_index = 0; row_index < nb_users; row_index++) {
			for(int column_index = 0; column_index < k; column_index++) {
				norm = norm + (V[row_index][column_index]*V[row_index][column_index]);
			}
		}
		return norm;
	}
	
	private final double get_norm_square_bias_users() {
		double norm = 0;
		for(int index = 0; index < bias_users.length; index++) {
			norm = norm + (bias_users[index] * bias_users[index]);
		}
		return norm;
	}
	
	private final double get_norm_square_bias_items() {
		double norm = 0;
		for(int index = 0; index < bias_items.length; index++) {
			norm = norm + (bias_items[index] * bias_items[index]);
		}
		return norm;
	}
	
}
