package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class DatasetMinMax implements Dataset{
	private final Map<Integer, Integer> dataset;
	
	private final double initValue;

	public String structure() {
		return util.Names.MinMax;
	}
	

	public DatasetMinMax( Map<Integer, Integer> dataset, double initValue, int[] users) {
		this.dataset = new HashMap<Integer, Integer>();
		this.initValue = initValue;
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetMinMax(dataset,initValue,users));
	}


	public DatasetMinMax(String fileName, double initValue) throws IOException { //Add double initValue = 0?
		dataset = new HashMap<Integer, Integer>();
		
		this.initValue = initValue;
		
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		double rating;
		while ((ligne=io1.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (dataset.get(user) == null && rating > initValue + 3.0) {
				dataset.put(user, 0);
			}
			if(rating > initValue + 3.0) {
				int size = dataset.get(user);
				dataset.put(user,size+1);
			}
			
		}
		io1.close();

	}

	public DatasetMinMax(String fileName) throws IOException { //Add double initValue = 0?
		this(fileName,0.0);
	}


	@Override
	public double getRating(int user, int item) {
		double rating = initValue;
		return rating;
	}

	@Override
	public boolean hasRated(int user, int item) {
		return true;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	JACCARD
	
	@Override
	public double sim(int user1, int user2) {
		double max = dataset.get(user1);
		double min = dataset.get(user2);
		
		if (max < min) {
			double aux = min;
			min = max;
			max = aux;
		}
		double sim = min / max;
		return sim;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	COSINE SIMILARITY
	
//	@Override
//	public double sim(int user1, int user2) {
//
//		Map<Integer, Double> profile1 = dataset.get(user1);
//		Map<Integer, Double> profile2 = dataset.get(user2);
//		double sim = 0;
////		double dist1 = 0;
////		double dist2 = 0;
//		
//		
//		double dist1 = norm.get(user1);
//		double dist2 = norm.get(user2);
//		
//		
//		double common = 0;
//
//		for(int item : profile1.keySet()) {
//			double r1 = profile1.get(item);
////			dist1 = dist1 + (r1 * r1);
//			if(r1 != initValue) { //TO change into r1 != initValue
//				double r2 = 0;
//				if(profile2.get(item)!=null && profile2.get(item)!=initValue) {
//					r2 = profile2.get(item);
//				}
//				common = common + r1 * r2;
////				common = common + r1 * profile2.getOrDefault(item, 0.0);
//			}
//		}
////		for(int item : profile2.keySet()) {
////			double r2 = profile2.get(item);
////			dist2 = dist2 + (r2 * r2);
////		}
//		if (dist1 != 0 && dist2 != 0) {
////			sim = common / Math.sqrt(dist1 * dist2);
//			
//			//
//			sim = common / (dist1 * dist2);
//		}
////		System.out.println(sim);
//		return sim;
//	}

//	@Override
//	public double sim(Object profile, int user1, int user2) {
//
//		@SuppressWarnings("unchecked")
//		Map<Integer, Double> profile1 = (Map<Integer, Double>) profile;
//		Map<Integer, Double> profile2 = dataset.get(user2);
//		double sim = 0;
////		double dist1 = 0;
////		double dist2 = 0;
//		
//		//
//		double dist1 = norm.get(user1);
//		double dist2 = norm.get(user2);
//		
//		double common = 0;
//
//		for(int item : profile1.keySet()) {
//			double r1 = profile1.get(item);
////			dist1 = dist1 + (r1 * r1);
//			if(r1 != initValue) { //TO change into r1 != initValue
//				double r2 = 0;
//				if(profile2.get(item)!=null) {
//					r2 = profile2.get(item);
//				}
//				common = common + r1 * r2;
////				common = common + r1 * profile2.getOrDefault(item, 0.0);
//			}
//		}
////		for(int item : profile2.keySet()) {
////			double r2 = profile2.get(item);
////			dist2 = dist2 + (r2 * r2);
////		}
//		if (dist1 != 0 && dist2 != 0) {
////			sim = common / Math.sqrt(dist1 * dist2);
//			sim = common / (dist1 * dist2);
//		}
//		return sim;
//	}
//
//	@Override
//	public double sim(Object profile, int user2) {
//
//		@SuppressWarnings("unchecked")
//		Map<Integer, Double> profile1 = (Map<Integer, Double>) profile;
//		Map<Integer, Double> profile2 = dataset.get(user2);
//		double sim = 0;
//		double dist1 = 0;
////		double dist2 = 0;
//		
//		//
////		double dist1 = norm.get(user1);
//		double dist2 = norm.get(user2);
//		
//		double common = 0;
//
//		for(int item : profile1.keySet()) {
//			double r1 = profile1.get(item);
//			dist1 = dist1 + (r1 * r1);
//			if(r1 != initValue) { //TO change into r1 != initValue
//				double r2 = 0;
//				if(profile2.get(item)!=null) {
//					r2 = profile2.get(item);
//				}
//				common = common + r1 * r2;
////				common = common + r1 * profile2.getOrDefault(item, 0.0);
//			}
//		}
////		for(int item : profile2.keySet()) {
////			double r2 = profile2.get(item);
////			dist2 = dist2 + (r2 * r2);
////		}
//		if (dist1 != 0 && dist2 != 0) {
////			sim = common / Math.sqrt(dist1 * dist2);
//			sim = common / (Math.sqrt(dist1) * dist2);
//		}
//		return sim;
//	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		return (new HashSet<Integer>());
	}

	
	@Override
	public double getInitValue() {
		return initValue;
	}
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetMinMax);
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}


}
