package dataset.gen;

import java.io.*;

public final class RewriteRatingsRawDataset {


	//Add 5 to each line of the dataset
	public static final void addGoodRating(String input, String output) throws IOException {
		String mySplit = util.Names.datasetSplit;
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
//			ligne = ligne.replace(split, mySplit);
			ligne = ligne + mySplit + "5";
			if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}		
		io.close();
		mbrWriter.close();
	}
	
	//Add (u,v) for each (v,u) link, (u,v) is a line "u v"
	public static final void fromUndirectedToDirected(String input, String output) throws IOException {
		String mySplit = util.Names.datasetSplit;
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
			String[] users = ligne.split(mySplit);
			ligne = ligne + "/n" + users[1] + mySplit + users[0];
			if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}		
		io.close();
		mbrWriter.close();
	}
	
	
	
	//AddRaw file should have the format:
	//User_separator_carac1_carac2... if split == _
	//Output scheme:
	//user item rating
	//if index_rating == -1 then ratings is automatically put at 5
	public final static void rewrite(String AddRaw, String separator, String split/*, int index_user*/, int index_item, int index_rating, String AddOutput, String fileName) throws IOException {
		String mySplit = util.Names.datasetSplit;
		InputStream ips = new FileInputStream(AddRaw);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		File file = new File(AddOutput);
		file.mkdirs();
		BufferedWriter writer= new BufferedWriter(new FileWriter(AddOutput + fileName));
		boolean not_first_line = false;
		while ((ligne=io.readLine())!=null){		
			ligne = ligne.replace(separator, "");
			String[] rating_results = ligne.split(split);
			String new_line;
			if(index_rating != -1) {
				new_line = rating_results[0] + mySplit + rating_results[index_item] + mySplit + rating_results[index_rating];
			}
			else {
				new_line = rating_results[0] + mySplit + rating_results[index_item] + mySplit + "5";
			}
			if (not_first_line) {
				writer.newLine();
			}
			else {
				not_first_line = true;
			}
			writer.write(new_line);
		}
		io.close();
		writer.close();
	}


	//AddRaw file should have the format:
	//carac0_carac1_carac2... if split == _
	//Output scheme:
	//user item rating
	//if index_rating == -1 then ratings is automatically put at 5
	public final static void rewrite(String AddRaw, String split, int index_user, int index_item, int index_rating, String AddOutput, String fileName) throws IOException {
		String mySplit = util.Names.datasetSplit;
		InputStream ips = new FileInputStream(AddRaw);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		File file = new File(AddOutput);
		file.mkdirs();
		BufferedWriter writer= new BufferedWriter(new FileWriter(AddOutput + fileName));
		boolean not_first_line = false;
		while ((ligne=io.readLine())!=null){
//			if(!ligne.startsWith("user")) {
			String[] rating_results = ligne.split(split);
			String new_line;
			if(index_rating != -1) {
				new_line = rating_results[index_user] + mySplit + rating_results[index_item] + mySplit + rating_results[index_rating];
			}
			else {
				new_line = rating_results[0] + mySplit + rating_results[index_item] + mySplit + "5";
			}
			if (not_first_line) {
				writer.newLine();
			}
			else {
				not_first_line = true;
			}
			writer.write(new_line);
//			}
		}
		io.close();
		writer.close();
	}
	
	
	
	
	public final static void removeBadLines(String AddRaw, String AddOutput, String fileName) throws IOException {
		String mySplit = util.Names.datasetSplit;
		InputStream ips = new FileInputStream(AddRaw);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		File file = new File(AddOutput);
		file.mkdirs();
		BufferedWriter writer= new BufferedWriter(new FileWriter(AddOutput + fileName));
		boolean not_first_line = false;
		while ((ligne=io.readLine())!=null){
			String[] rating_results = ligne.split(mySplit);
			try {
//				Long.parseLong(rating_results[0]);
//				Long.parseLong(rating_results[1]);
				Long.parseLong(rating_results[0]);
				Long.parseLong(rating_results[1]);
				Double.parseDouble(rating_results[2]);
//			if(rating_results.length == 3) {
				if (not_first_line) {
					writer.newLine();
				}
				else {
					not_first_line = true;
				}
				writer.write(ligne);
			}catch (Exception e) {
				System.out.println(ligne);
//				e.printStackTrace();
			}
//			else {
//				System.out.println(ligne);
//			}
		}
		io.close();
		writer.close();
		System.out.println("done!");
	}

}
