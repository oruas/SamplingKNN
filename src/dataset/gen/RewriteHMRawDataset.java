package dataset.gen;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class RewriteHMRawDataset {

	
	public final static void rewriteDataset(String input, String split, int index_user, int index_item, int index_rating, String output_map1, String output_map2, String output) throws IOException {
		String mySplit = util.Names.datasetSplit;
		
		Map<String, Integer> map_users = new HashMap<String, Integer>();
		Map<String, Integer> map_items = new HashMap<String, Integer>();
		
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] lignes;
		while ((ligne=io.readLine())!=null){
			lignes = ligne.split(split);
			if (map_users.get(lignes[index_user])==null) {
				map_users.put(lignes[index_user], map_users.size());
			}
			if (map_items.get(lignes[index_item])==null) {
				map_items.put(lignes[index_item], map_items.size());
			}
		}		
		io.close();
		io = null;
		ipsr = null;
		ips = null;
		
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output_map1));
		boolean first_line = true;
		for (String user_string : map_users.keySet()){
			ligne = user_string + mySplit + map_users.get(user_string);
			if (first_line){mbrWriter.write(ligne); first_line = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}
		mbrWriter.close();
		
		mbrWriter = new BufferedWriter(new FileWriter(output_map2));
		first_line = true;
		for (String item_string : map_items.keySet()){
			ligne = item_string + mySplit + map_items.get(item_string);
			if (first_line){mbrWriter.write(ligne); first_line = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}
		mbrWriter.close();
		
		
		ips = new FileInputStream(input);
		ipsr = new InputStreamReader(ips);
		io = new BufferedReader(ipsr);
		mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
			lignes = ligne.split(split);
			if(index_rating != -1) {
				ligne = map_users.get(lignes[index_user]) + mySplit + map_items.get(lignes[index_item]) + mySplit + lignes[index_rating];
			}
			else {
				ligne = map_users.get(lignes[index_user]) + mySplit + map_items.get(lignes[index_item]) + mySplit + "5";
			}
			if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}		
		io.close();
		mbrWriter.close();
	}
	
	
	
	
	public final static void rewriteDatasetLongToInt(String input, String output_map1, String output_map2, String output) throws IOException {
		String mySplit = util.Names.datasetSplit;
		int index_user = 0;
		int index_item = 1;
		int index_rating = -1;
		
		Map<String, Integer> map_users = new HashMap<String, Integer>();
		Map<String, Integer> map_items = new HashMap<String, Integer>();
		
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] lignes;
		while ((ligne=io.readLine())!=null){
			lignes = ligne.split(mySplit);
			if (map_users.get(lignes[index_user])==null) {
				map_users.put(lignes[index_user], map_users.size());
			}
			if (map_items.get(lignes[index_item])==null) {
				map_items.put(lignes[index_item], map_items.size());
			}
		}		
		io.close();
		io = null;
		ipsr = null;
		ips = null;
	
		
		System.out.println("mapping done");
		
		if(map_users.size()<0) {
			System.out.println("Too many users");
		}
		if(map_items.size()<0) {
			System.out.println("Too many items");
		}
		
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output_map1));
		boolean first_line = true;
		for (String user_string : map_users.keySet()){
			ligne = user_string + mySplit + map_users.get(user_string);
			if (first_line){mbrWriter.write(ligne); first_line = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}
		mbrWriter.close();
		
		mbrWriter = new BufferedWriter(new FileWriter(output_map2));
		first_line = true;
		for (String item_string : map_items.keySet()){
			ligne = item_string + mySplit + map_items.get(item_string);
			if (first_line){mbrWriter.write(ligne); first_line = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}
		mbrWriter.close();
		
		
		ips = new FileInputStream(input);
		ipsr = new InputStreamReader(ips);
		io = new BufferedReader(ipsr);
		mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
			lignes = ligne.split(mySplit);
			if(index_rating != -1) {
				ligne = map_users.get(lignes[index_user]) + mySplit + map_items.get(lignes[index_item]) + mySplit + lignes[index_rating];
			}
			else {
				ligne = map_users.get(lignes[index_user]) + mySplit + map_items.get(lignes[index_item]) + mySplit + "5";
			}
			if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
			else{mbrWriter.write("\n"+ligne);}	
		}		
		io.close();
		mbrWriter.close();
	}
}
