package dataset.gen;

import java.io.*;

public final class AlterRawDataset {

	public final static void removeCommentsChangeSplitAddGoodRating(String input, String output, String commentSign, String split, boolean undirected) throws IOException {
		removeCommentsChangeSplitAddGoodRating(input,output,commentSign,"",split,1,-1,undirected);
	}
	public final static void removeCommentsChangeSplitAddGoodRating(String input, String output, String commentSign, String separator, String split, boolean undirected) throws IOException {
		removeCommentsChangeSplitAddGoodRating(input,output,commentSign,separator,split,1,-1,undirected);
	}
	public final static void removeCommentsChangeSplitAddGoodRating(String input, String output, String commentSign, String separator, String split, int index_item, boolean undirected) throws IOException {
		removeCommentsChangeSplitAddGoodRating(input,output,commentSign,separator,split,index_item,-1,undirected);
	}
	public final static void removeCommentsChangeSplitAddGoodRating(String input, String output, String commentSign, String separator, String split/*, int index_user*/, int index_item, int index_rating, boolean undirected) throws IOException {
		String mySplit = util.Names.datasetSplit;
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
			if(!ligne.startsWith(commentSign)) {

				if(!separator.equals("")) {
					ligne = ligne.replace(separator, "");
				}
				String[] rating_results = ligne.split(split);
				//				if(rating_results.length == 2) {
				String new_line;
				if(index_rating > 0) {
					new_line = rating_results[0] + mySplit + rating_results[index_item] + mySplit + rating_results[index_rating];
				}
				else {
					new_line = rating_results[0] + mySplit + rating_results[index_item] + mySplit + "5";
//					new_line = rating_results[index_item] + mySplit + rating_results[0] + mySplit + "5"; //In the case it is ITEM SEPARATOR USER
				}
				if (first_lineTrainingSet){mbrWriter.write(new_line); first_lineTrainingSet = false;}
				else{mbrWriter.write("\n"+new_line);}
				/*
				 * If the graph is undirected, we write i j r and j i r
				 * The case index_rating > 0 is unlikely if we are dealing with an undirected graph...
				 */
				if (undirected) {
					if(index_rating > 0) {
						new_line = rating_results[index_item] + mySplit + rating_results[0] + mySplit + rating_results[index_rating];
					}
					else {
						new_line = rating_results[index_item] + mySplit + rating_results[0] + mySplit + "5";
					}
					mbrWriter.write("\n"+new_line);
				}
				//				}
			}
		}		
		io.close();
		mbrWriter.close();
	}

	public final static void removeCommentsDataset(String input, String output, String commentSign) throws IOException {
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
			if(!ligne.startsWith(commentSign)) {
				if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
				else{mbrWriter.write("\n"+ligne);}	
			}
		}		
		io.close();
		mbrWriter.close();
	}

	public final static void transformAmazonDataset(String input, String output) throws IOException {
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
			if(ligne.startsWith("product/productId")) {
				String item = ligne.replace("product/productId: ", "");
				ligne=io.readLine();
				String user = ligne.replace("review/userId: ", "");
				ligne=io.readLine();
				String score = ligne.replace("review/score: ", "");
				ligne = user + " " + item + " " + score;
				if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
				else{mbrWriter.write("\n"+ligne);}	
			}
		}		
		io.close();
		mbrWriter.close();
	}

	public final static void filterDatasetAmazon(String input, String output) throws IOException {
		String[] filters = {"product/productId", "review/userId", "review/score"};
		filterDataset(input, filters, output);
	}

	public final static void filterDataset(String input, String[] filters, String output) throws IOException {
		InputStream ips = new FileInputStream(input);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output));
		boolean first_lineTrainingSet = true;
		while ((ligne=io.readLine())!=null){
			boolean b = false;
			for (String filter: filters) {
				b = ligne.startsWith(filter);
				if (b) break;
			}
			if(b) {
				if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
				else{mbrWriter.write("\n"+ligne);}	
			}
		}		
		io.close();
		mbrWriter.close();
	}
}
