package dataset.gen;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
//import org.apache.commons.math3.

import org.apache.commons.math3.distribution.ZipfDistribution;


public final class CreateFakeDataset {

	public final static void main(String[] args) throws IOException {
		String Add = "./Files/Datasets/FakeDataset/";
		String output = "u.data";
		int nb_users = 31 * 300;
		int nb_items = 20;
		int size_cluster = 31;
		main2(Add,output,nb_users,nb_items,size_cluster);
		CrossValidationTestSetGen.testset8020gen(Add + output, Add + "TestSet");
	}

	public final static void main2(String Add, String output, int nb_users, int nb_items, int size_cluster) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		String mySplit = util.Names.datasetSplit;
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(Add + output));
		boolean first_lineTrainingSet = true;

		for(int user=0; user<nb_users; user++) {
			for(int index = 0; index < nb_items; index++) {
				int item = (user / size_cluster) * nb_items + index;
				String new_line = user + mySplit + item + mySplit + "5";
				if (first_lineTrainingSet){mbrWriter.write(new_line); first_lineTrainingSet = false;}
				else{mbrWriter.write("\n"+new_line);}
			}
		}



		mbrWriter.close();
	}
	


//    public static long inverseCdfFast(final double p, final double s, final double N) {
//        if (p > 1d || p < 0d)
//            throw new IllegalArgumentException("p must be between 0 and 1");
//
//        final double tolerance = 0.01d;
//        double x = N / 2;
//
//        final double D = p * (12 * (Math.pow(N, 1 - s) - 1) / (1 - s) + 6 - 6 * Math.pow(N, -s) + s - Math.pow(N, -1 - s) * s);
//
//        while (true) {
//            final double m    = Math.pow(x, -2 - s);
//            final double mx   = m   * x;
//            final double mxx  = mx  * x;
//            final double mxxx = mxx * x;
//
//            final double a = 12 * (mxxx - 1) / (1 - s) + 6 * (1 - mxx) + (s - (mx * s)) - D;
//            final double b = 12 * mxx + 6 * (s * mx) + (m * s * (s + 1));
//            final double newx = Math.max(1, x - a / b);
//            if (Math.abs(newx - x) <= tolerance)
//                return (long) newx;
//            x = newx;
//        }
//    }
	
	public final static double[] get_distrib_zipf(int nb, double s) {
		double[] distrib = new double[nb];
		ZipfDistribution zipf = new ZipfDistribution(nb,s);
		for(int i=0; i<nb;i++) {
			distrib[i] = zipf.cumulativeProbability(i+1);
		}
		return distrib;
	}

	

	public final static void generate_fake_zipf_av(String Add, String output, long nb_ratings_av, int nb_users, double s_u, int nb_items, double s_i) throws IOException {
		
//		System.out.println("enter");
		
		Map<Integer,Set<Integer>> dataset = new HashMap<Integer,Set<Integer>>();
		Random random = new Random();
//		ZipfDistribution zipf_u = new ZipfDistribution(nb_users,s_u);
//		ZipfDistribution zipf_i = new ZipfDistribution(nb_items,s_i);
		double[] zipf_d_u = get_distrib_zipf(nb_users,s_u);
		System.out.println("distrib u ok");
		double[] zipf_d_i = get_distrib_zipf(nb_items,s_i);
		System.out.println("distrib i ok");
		
		long nb_ratings = ((long) nb_users) * nb_ratings_av;
		
		int user;
		int rated_item;
		double x;
		
//		System.out.println("entering the loop");
		
		for(user=0; user<nb_users; user++) {
			Set<Integer> u_profile = new HashSet<Integer>();
//			System.out.println("entering the second loop");
			while(u_profile.size()<20) {
				rated_item=0;
				x = random.nextDouble();
//				System.out.println("starting founding the item");
//				while(zipf_i.cumulativeProbability(rated_item)<x) {
//					rated_item++;
//				}
				while(zipf_d_i[rated_item]<x) {
					rated_item++;
				}
//				rated_item = (int) inverseCdfFast(x, s_i, nb_items);
//				System.out.println("item found!");
				u_profile.add(rated_item);
//				System.out.println("user "+user+" item " + rated_item + " done");
			}
			dataset.put(user, u_profile);
//			System.out.println("user "+user+" done");
		}
//		System.out.println("20 rating each done");
		
		for(long rating=0; rating < nb_ratings - 20*nb_users; rating++) {
			user=0;
			x = random.nextDouble();
//			user = (int) inverseCdfFast(x, s_u, nb_users);
//			if(dataset.get(user).size() == nb_items) {
//			break;
//		  	}
//			while(zipf_u.cumulativeProbability(user)<x) {
//				user++;
//			}
			while(zipf_d_u[user]<x) {
				user++;
			}
//			boolean b=false;
//			while(!b) {
				rated_item=0;
				x = random.nextDouble();
//				rated_item = (int) inverseCdfFast(x, s_i, nb_items);
//				while(zipf_i.cumulativeProbability(rated_item)<x) {
//					rated_item++;
//				}
				while(zipf_d_i[rated_item]<x) {
					rated_item++;
				}
//				b = dataset.get(user).add(rated_item);
				dataset.get(user).add(rated_item);
//			}
		}
		printDataset(dataset,Add,output);
//		System.out.println("Dataset done!");
	}
	
	
	public final static void generate_fake_zipf(String Add, String output, long nb_ratings, int nb_users, double s_u, int nb_items, double s_i) throws IOException {
		
//		System.out.println("enter");
		
		Map<Integer,Set<Integer>> dataset = new HashMap<Integer,Set<Integer>>();
		Random random = new Random();
//		ZipfDistribution zipf_u = new ZipfDistribution(nb_users,s_u);
//		ZipfDistribution zipf_i = new ZipfDistribution(nb_items,s_i);
		double[] zipf_d_u = get_distrib_zipf(nb_users,s_u);
//		System.out.println("distrib u ok");
		double[] zipf_d_i = get_distrib_zipf(nb_items,s_i);
//		System.out.println("distrib i ok");
		
		
		int user;
		int rated_item;
		double x;
		
//		System.out.println("entering the loop");
		
		for(user=0; user<nb_users; user++) {
			Set<Integer> u_profile = new HashSet<Integer>();
//			System.out.println("entering the second loop");
			while(u_profile.size()<20) {
				rated_item=0;
				x = random.nextDouble();
//				System.out.println("starting founding the item");
//				while(zipf_i.cumulativeProbability(rated_item)<x) {
//					rated_item++;
//				}
				while(zipf_d_i[rated_item]<x) {
					rated_item++;
				}
//				rated_item = (int) inverseCdfFast(x, s_i, nb_items);
//				System.out.println("item found!");
				u_profile.add(rated_item);
//				System.out.println("user "+user+" item " + rated_item + " done");
			}
			dataset.put(user, u_profile);
//			System.out.println("user "+user+" done");
		}
//		System.out.println("20 rating each done");
		
		for(long rating=0; rating < nb_ratings - 20*nb_users; rating++) {
			user=0;
			x = random.nextDouble();
//			user = (int) inverseCdfFast(x, s_u, nb_users);
//			if(dataset.get(user).size() == nb_items) {
//			break;
//		  	}
//			while(zipf_u.cumulativeProbability(user)<x) {
//				user++;
//			}
			while(zipf_d_u[user]<x) {
				user++;
			}
			boolean b=false;
			while(!b) {
				rated_item=0;
				x = random.nextDouble();
//				rated_item = (int) inverseCdfFast(x, s_i, nb_items);
//				while(zipf_i.cumulativeProbability(rated_item)<x) {
//					rated_item++;
//				}
				while(zipf_d_i[rated_item]<x) {
					rated_item++;
				}
				b = dataset.get(user).add(rated_item);
			}
		}
		printDataset(dataset,Add,output);
//		System.out.println("Dataset done!");
	}
	
	
	public final static void generate_fake(String Add, String output, int nb_users, int average_profile_size, int profile_standard_deviation, int nb_items, int item_standard_deviation) throws IOException {
		Map<Integer,Set<Integer>> dataset = new HashMap<Integer,Set<Integer>>();
		Random random = new Random();
		int nb_ratings;
		int rated_item;
		
		
		for(int user=0; user<nb_users; user++) {
			nb_ratings = (int) ((random.nextGaussian() * profile_standard_deviation) + average_profile_size);
			dataset.put(user, new HashSet<Integer>());
			while (nb_ratings < 20) {
				nb_ratings = (int) ((random.nextGaussian() * profile_standard_deviation) + average_profile_size);
			}
			for(int i=0; i<nb_ratings; i++) {
				rated_item = (int) (item_standard_deviation * random.nextGaussian() + (nb_items-1)/2);
				while (rated_item < 0 || nb_items <= rated_item) {
					rated_item = (int) (item_standard_deviation * random.nextGaussian() + (nb_items-1)/2);
				}
				dataset.get(user).add(rated_item);
			}
		}
		printDataset(dataset,Add,output);
	}
	
	
	public static void printDataset(Map<Integer,Set<Integer>> dataset, String Add, String fileName) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(Add+fileName));
		double rating;
		String line;
		boolean first_line = true;
		for (int user: dataset.keySet()) {
			for (int item: dataset.get(user)) {
				rating = 5;
				line = user + " " + item + " " + rating;
				if(first_line) {
					first_line = false;
				}
				else {
					line = "\n" + line ;
				}
				mbrWriterOutput.write(line);
			}
		}
		mbrWriterOutput.close();
	}
}
