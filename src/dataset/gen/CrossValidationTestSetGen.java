package dataset.gen;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public final class CrossValidationTestSetGen {

	public static void testset8020gen(String fileNameInput, String fileNameOutput) throws IOException{
		String testset0 = fileNameOutput + "0";
		String testset1 = fileNameOutput + "1";
		String testset2 = fileNameOutput + "2";
		String testset3 = fileNameOutput + "3";
		String testset4 = fileNameOutput + "4";
		InputStream ips = new FileInputStream(fileNameInput);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		Random randomGenerator = new Random();
//		(new File(testset0)).mkdirs();
//		(new File(testset1)).mkdirs();
//		(new File(testset2)).mkdirs();
//		(new File(testset3)).mkdirs();
//		(new File(testset4)).mkdirs();
		BufferedWriter mbrWriter0 = new BufferedWriter(new FileWriter(testset0 + ".data"));
		BufferedWriter mbrWriter1 = new BufferedWriter(new FileWriter(testset1 + ".data"));
		BufferedWriter mbrWriter2 = new BufferedWriter(new FileWriter(testset2 + ".data"));
		BufferedWriter mbrWriter3 = new BufferedWriter(new FileWriter(testset3 + ".data"));
		BufferedWriter mbrWriter4 = new BufferedWriter(new FileWriter(testset4 + ".data"));
		boolean first_line0 = true;
		boolean first_line1 = true;
		boolean first_line2 = true;
		boolean first_line3 = true;
		boolean first_line4 = true;
		while ((ligne=io.readLine())!=null){
			int random = randomGenerator.nextInt(100);				
			if (random < 20) {
				if (first_line1){mbrWriter1.write(ligne); first_line1 = false;}
				else{mbrWriter1.write("\n"+ligne);}
				if (first_line2){mbrWriter2.write(ligne); first_line2 = false;}
				else{mbrWriter2.write("\n"+ligne);}
				if (first_line3){mbrWriter3.write(ligne); first_line3 = false;}
				else{mbrWriter3.write("\n"+ligne);}
				if (first_line4){mbrWriter4.write(ligne); first_line4 = false;}
				else{mbrWriter4.write("\n"+ligne);}
			}
			if (random >= 20 && random < 40) {
				if (first_line0){mbrWriter0.write(ligne); first_line0 = false;}
				else{mbrWriter0.write("\n"+ligne);}
				if (first_line2){mbrWriter2.write(ligne); first_line2 = false;}
				else{mbrWriter2.write("\n"+ligne);}
				if (first_line3){mbrWriter3.write(ligne); first_line3 = false;}
				else{mbrWriter3.write("\n"+ligne);}
				if (first_line4){mbrWriter4.write(ligne); first_line4 = false;}
				else{mbrWriter4.write("\n"+ligne);}
			}
			if (random >= 40 && random < 60) {
				if (first_line0){mbrWriter0.write(ligne); first_line0 = false;}
				else{mbrWriter0.write("\n"+ligne);}
				if (first_line1){mbrWriter1.write(ligne); first_line1 = false;}
				else{mbrWriter1.write("\n"+ligne);}
				if (first_line3){mbrWriter3.write(ligne); first_line3 = false;}
				else{mbrWriter3.write("\n"+ligne);}
				if (first_line4){mbrWriter4.write(ligne); first_line4 = false;}
				else{mbrWriter4.write("\n"+ligne);}
			}
			if (random >= 60 && random < 80) {
				if (first_line0){mbrWriter0.write(ligne); first_line0 = false;}
				else{mbrWriter0.write("\n"+ligne);}
				if (first_line1){mbrWriter1.write(ligne); first_line1 = false;}
				else{mbrWriter1.write("\n"+ligne);}
				if (first_line2){mbrWriter2.write(ligne); first_line2 = false;}
				else{mbrWriter2.write("\n"+ligne);}
				if (first_line4){mbrWriter4.write(ligne); first_line4 = false;}
				else{mbrWriter4.write("\n"+ligne);}
			}
			if (random >= 80) {
				if (first_line0){mbrWriter0.write(ligne); first_line0 = false;}
				else{mbrWriter0.write("\n"+ligne);}
				if (first_line1){mbrWriter1.write(ligne); first_line1 = false;}
				else{mbrWriter1.write("\n"+ligne);}
				if (first_line2){mbrWriter2.write(ligne); first_line2 = false;}
				else{mbrWriter2.write("\n"+ligne);}
				if (first_line3){mbrWriter3.write(ligne); first_line3 = false;}
				else{mbrWriter3.write("\n"+ligne);}
			}

		}
		io.close();
		mbrWriter0.close();
		mbrWriter1.close();
		mbrWriter2.close();
		mbrWriter3.close();
		mbrWriter4.close();
	}
	

	public static void refineDataSetMinRatings(String fileNameInput, String separator, String split, String fileNameOutput, int min_ratings) throws IOException{
//		Map<Integer, Integer> dataset = new HashMap<Integer, Integer>();
		Map<Integer, HashSet<Integer>> dataset = new HashMap<Integer, HashSet<Integer>>();
		
//		this.initValue = 0;
//		System.out.println(initValue);
		
		InputStream ips = new FileInputStream(fileNameInput);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		while ((ligne=io.readLine())!=null){
			ligne = ligne.replace(separator, "");
			String[] rating_results = ligne.split(split);
			int user = Integer.parseInt(rating_results[0]);
//			System.out.println(rating);
			if (dataset.get(user) == null) {
//				dataset.put(user, 0);
				dataset.put(user, new HashSet<Integer>());
			}
//			dataset.put(user, dataset.get(user)+1);
			dataset.get(user).add(Integer.parseInt(rating_results[1]));
			
			//
//			if(norm.get(user) == null) {
//				norm.put(user, 0.);
//			}
//			norm.put(user, norm.get(user) + (rating * rating));
			
		}
		io.close();
		Set<Integer> users = new HashSet<Integer>();
		for(int user:dataset.keySet()) {
			users.add(user);
		}
		for(int user: users){
//			if (dataset.get(user) < min_ratings) {
			if (dataset.get(user).size() < min_ratings) {
//				dataset.put(user, null);
				dataset.remove(user);
			}
		}
		

		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(fileNameOutput));

		ips = new FileInputStream(fileNameInput);
		ipsr = new InputStreamReader(ips);
		io = new BufferedReader(ipsr);
		boolean first_line = true;
		while ((ligne=io.readLine())!=null){
			ligne = ligne.replace(separator, "");
			String[] rating_results = ligne.split(split);
			int user = Integer.parseInt(rating_results[0]);
			if(dataset.get(user) != null) {
				if(first_line) {
					mbrWriter.write(ligne);
					first_line = false;
				}
				else{
					mbrWriter.write("\n"+ligne);
				}
			}
		}
		io.close();
		mbrWriter.close();

	}
	

	public static void refineDataSetMinRatingsInt(String fileNameInput, String separator, String split, String fileNameOutput, int min_ratings) throws IOException{
//		Map<Integer, Integer> dataset = new HashMap<Integer, Integer>();
		Map<Integer, Integer> dataset = new HashMap<Integer, Integer>();
		
//		this.initValue = 0;
//		System.out.println(initValue);
		
		InputStream ips = new FileInputStream(fileNameInput);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		while ((ligne=io.readLine())!=null){
			ligne = ligne.replace(separator, "");
			String[] rating_results = ligne.split(split);
			int user = Integer.parseInt(rating_results[0]);
//			System.out.println(rating);
			if (dataset.get(user) == null) {
//				dataset.put(user, 0);
				dataset.put(user, 0);
			}
			dataset.put(user, dataset.get(user)+1);
//			dataset.get(user).add(Integer.parseInt(rating_results[1]));
			
			//
//			if(norm.get(user) == null) {
//				norm.put(user, 0.);
//			}
//			norm.put(user, norm.get(user) + (rating * rating));
			
		}
		io.close();
		Set<Integer> users = new HashSet<Integer>();
		for(int user:dataset.keySet()) {
			users.add(user);
		}
		for(int user: users){
			if (dataset.get(user) < min_ratings) {
//			if (dataset.get(user).size() < min_ratings) {
//				dataset.put(user, null);
				dataset.remove(user);
			}
		}
		

		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(fileNameOutput));

		ips = new FileInputStream(fileNameInput);
		ipsr = new InputStreamReader(ips);
		io = new BufferedReader(ipsr);
		boolean first_line = true;
		while ((ligne=io.readLine())!=null){
			ligne = ligne.replace(separator, "");
			String[] rating_results = ligne.split(split);
			int user = Integer.parseInt(rating_results[0]);
			if(dataset.get(user) != null) {
				if(first_line) {
					mbrWriter.write(ligne);
					first_line = false;
				}
				else{
					mbrWriter.write("\n"+ligne);
				}
			}
		}
		io.close();
		mbrWriter.close();

	}
}
