package dataset;

import java.util.Map;
import java.util.Set;

public interface Dataset {

//	public void add(int user, int item, double rating);
//	public Object getProfile(int user);
	public double getRating(int user, int item);
	public boolean hasRated(int user, int item);
	public double sim(int user1, int user2);
//	public double sim(Object profile1, int user2);
//	public double sim(Object profile1, int user1, int user2);
	public Set<Integer> getUsers();
	public Set<Integer> getRatedItems(int user);
	public double getInitValue();
	
	public Map<String, String> ParametersToMap();
	public String ParametersToString();
	
	public Dataset subDataset(int[] users);
	
	public default String structure(){
		return "";
	}
	
//	public default int getID(int user) {
//		return user;
//	}
}
