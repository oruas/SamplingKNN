package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import util.BitVectorSize;

public final class DatasetBitVectorSize implements Dataset {

	private final int size_sketchs;
	
	private final Map<Integer,BitVectorSize> dataset;
	
	private final double initValue;
	

	public String structure() {
		return util.Names.BitArray;
	}
	
	public DatasetBitVectorSize(int size_sketchs,Map<Integer,BitVectorSize> dataset,double initValue, int[] users) {
		this.size_sketchs = size_sketchs;
		this.dataset = new HashMap<Integer, BitVectorSize>();
		this.initValue = initValue;
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetBitVectorSize(size_sketchs,dataset,initValue,users));
	}
	
	
	
	public DatasetBitVectorSize(String fileName, int size) throws IOException {
		this(fileName, size, 0);
	}
	
	public DatasetBitVectorSize(String fileName, int size, double initValue) throws IOException {
		
		this.initValue = initValue;
		
		dataset = new HashMap<Integer, BitVectorSize>();
		this.size_sketchs = size;
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		BitVectorSize sketch;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset.get(user) == null && rating > initValue + 3.0) {
				sketch = new BitVectorSize(size_sketchs);
				dataset.put(user, sketch);
			}
			if(rating > initValue + 3.0) {
				BitVectorSize bitset = dataset.get(user);
				bitset.put(item);
				
			}
		}
		io.close();
	}

	@Override
	public double getRating(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = hash / 64;
			long[] sketch = dataset.get(user).get_bitset();
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		if(b) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = hash / 64;
			long[] sketch = dataset.get(user).get_bitset();
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		return b;
	}

	@Override
	public double sim(int user1, int user2) {
		return (BitVectorSize.jaccard(dataset.get(user1).get_bitset(),dataset.get(user2).get_bitset()));
	}
//	public double sim(int user1, int user2) {
//		return (jaccard(dataset.get(user1),dataset.get(user2)));
//	}
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		Set<Integer> set = new HashSet<Integer>();
		if (dataset.containsKey(user)) {
			long[] bitset = dataset.get(user).get_bitset();
			for(int indexbitset = 0; indexbitset < bitset.length; indexbitset++) {
				for(int indexlong = 0; indexlong < 64; indexlong++) {
					if((bitset[indexbitset] & (((long) 1)<<indexlong)) != 0) {
						set.add((indexbitset * 64) + indexlong);
					}
				}
			}
		}
		return set;
	}
	
	@Override
	public double getInitValue() {
		return initValue;
	}
	
	
	
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetBitArray);
		dic.put(util.Names.nb_bits, Integer.toString(size_sketchs));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}
	
	
	
}
