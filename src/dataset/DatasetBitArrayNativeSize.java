package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class DatasetBitArrayNativeSize implements Dataset {

	private final int size_sketchs;
	
	private final Map<Integer,long[]> dataset;
	
	private final double initValue;
	

	public String structure() {
		return util.Names.BitArray;
	}
	
	public DatasetBitArrayNativeSize(int size_sketchs,Map<Integer,long[]> dataset,double initValue, int[] users) {
		this.size_sketchs = size_sketchs;
		this.dataset = new HashMap<Integer, long[]>(users.length);
		this.initValue = initValue;
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetBitArrayNativeSize(size_sketchs,dataset,initValue,users));
	}
	
	
	
	public DatasetBitArrayNativeSize(String fileName, int size) throws IOException {
		this(fileName, size, 0);
	}
	
	public DatasetBitArrayNativeSize(String fileName, int size, double initValue) throws IOException {
		
//		System.out.println("good dataset:" + fileName);
		
		this.initValue = initValue;
		
		dataset = new HashMap<Integer, long[]>();
		this.size_sketchs = size;
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		long[] sketch;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset.get(user) == null && rating > initValue + 3.0) {
				sketch = new long[((int) Math.ceil(((double) size_sketchs)/((double)64))) + 1];
				dataset.put(user, sketch);
			}
			if(rating > initValue + 3.0) {
				long[] bitset = dataset.get(user);
				int hash = util.HashFunctions.hash(item, size_sketchs);
//				int hash = item % size_sketchs;
				int index = (hash / 64) + 1;
				
//				if((bitset[index] & (((long) 1) << (hash % 64))) != 0) {
//					dataset.get(user)[0] = dataset.get(user)[0]+1;
//				}
				
				bitset[index] = bitset[index] | (((long) 1) << (hash % 64));
				
			}
		}
		io.close();
		for(int user_id: dataset.keySet()) {
			dataset.get(user_id)[0] = cardinality(dataset.get(user_id));
		}
	}

	@Override
	public double getRating(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = (hash / 64) + 1;
			long[] sketch = dataset.get(user);
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		if(b) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = (hash / 64)+1;
			long[] sketch = dataset.get(user);
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		return b;
	}

	@Override
	public double sim(int user1, int user2) {

		
		long[] bitset1 = dataset.get(user1);
		long[] bitset2 = dataset.get(user2);
		int intersize = getIntersectionSize(bitset1,bitset2);
		

//		if(user1 == 1 && user2 == 456) {
//			System.out.println(user1);
//			System.out.println(user2);
//			System.out.println(((double) intersize) / ((double) (bitset1[0] + bitset2[0] - intersize)));
//		}
		
		return (((double) intersize) / ((double) (bitset1[0] + bitset2[0] - intersize)));
//		return (jaccard(dataset.get(user1),dataset.get(user2)));
	}
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		Set<Integer> set = new HashSet<Integer>();
		if (dataset.containsKey(user)) {
			long[] bitset = dataset.get(user);
			for(int indexbitset = 1; indexbitset < bitset.length; indexbitset++) {
				for(int indexlong = 0; indexlong < 64; indexlong++) {
					if((bitset[indexbitset] & (((long) 1)<<indexlong)) != 0) {
						set.add((indexbitset * 64) + indexlong);
					}
				}
			}
		}
		return set;
	}
	
	@Override
	public double getInitValue() {
		return initValue;
	}
	
	
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetBitArrayNative);
		dic.put(util.Names.nb_bits, Integer.toString(size_sketchs));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}
	
//	private final static double jaccard(long[] bitset1, long[] bitset2){
//		int intersize = getIntersectionSize(bitset1,bitset2);
//		return (intersize / (bitset1[0] + bitset2[0] - intersize));
//	}
//	
	
	
	
	private final static int cardinality(long[] bitset) {
		int cardinality = 0;
		for(int i=1; i < bitset.length ; i++) {
			cardinality = cardinality + Long.bitCount(bitset[i]);
		}
		return cardinality;
	}
	
	private final static int getIntersectionSize(long[] bits1, long[] bits2) {
	    int nBits = 0;
	    for (int i=1; i<bits1.length; i++)
	        nBits += Long.bitCount(bits1[i] & bits2[i]);
	    return nBits;
	}
	
//	public final long get_card(int user) {
//		return (dataset.get(user)[0]);
//	}
	
	
	/**
	 * Remove if not used in DatasetBitArrayNativeSize_array
	 * @return
	 */
	
	public final Map<Integer,long[]> get_dataset() {
		return dataset;
	}
	
	public final int get_size_sketchs() {
		return size_sketchs;
	}
	
	
}
