package dataset;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import util.KNNGraph;

public final class DatasetABC_ implements Dataset {

	private final HashMap<Integer,BitSet> dataset;
	private final HashMap<Integer,Integer> profiles_size;
	private final int nb_bitset; /* number of bitset stored for each user, define the difference of size under which two users will be compared: 2^(nb_bitset-1)*/
	private final boolean estimNbBitset;
	
	private final int min_bitset_size;/* log of the minimum size for a bitset. e.g. 6 => 2^6 = 64*/
	private final int add_size; /*The size of the minimum bitset for an user is 2^(log(Profile)+add_size)  e.g. add_size = 3 mean that the profile will fill at most 1/(1+3) = 1/4 of the bitset*/

	private final double initValue;
	
	
	public String structure() {
		return util.Names.ABC_;
	}
	
	public DatasetABC_(HashMap<Integer,BitSet> dataset, HashMap<Integer,Integer> profiles_size, int nb_bitset, int min_bitset_size, int add_size, boolean estimNbBitset, double initValue, int[] users) {
		this.nb_bitset = nb_bitset;
		this.add_size = add_size;
		this.min_bitset_size = min_bitset_size;
		this.estimNbBitset = estimNbBitset;
		this.initValue = initValue;
		this.dataset = new HashMap<Integer,BitSet>();
		this.profiles_size = new HashMap<Integer,Integer>();
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
			this.profiles_size.put(user, profiles_size.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetABC_(dataset,profiles_size,nb_bitset,min_bitset_size,add_size,estimNbBitset,initValue,users));
	}

	public DatasetABC_(Dataset dataset_, Dataset subdataset_, KNNGraph knng, int min_bitset_size, int add_size) throws IOException {
		this(dataset_,subdataset_,knng,1,min_bitset_size,add_size);
	}

	
	public DatasetABC_(Dataset dataset_, Dataset subdataset_, KNNGraph knng, int size_sampling, int min_bitset_size, int add_size) throws IOException {
		this.estimNbBitset = true;
		this.min_bitset_size = min_bitset_size;
		this.add_size = add_size;
		this.dataset = new HashMap<Integer,BitSet>();
		this.profiles_size = new HashMap<Integer,Integer>();
		this.initValue = dataset_.getInitValue();
		double ratio = (((double) dataset_.getUsers().size()) / ((double) subdataset_.getUsers().size()));
		double minmax = util.Estimator.estimMinMinMax(subdataset_, knng, size_sampling) / ratio;
		this.nb_bitset = (int) (Math.ceil((Math.log(1/minmax)/Math.log(2))));
//		this.nb_bitset = Math.max(10,(int) (Math.ceil((Math.log(1/minmax)/Math.log(2)))));
		
		
		int profile_size;
		int log_2_size;
		int nb_of_bitset;
		int bitset_size;
		int hash;
		for(int user: dataset_.getUsers()) {
			profile_size = dataset_.getRatedItems(user).size();
			profiles_size.put(user, profile_size);
			log_2_size = ((int) (Math.log(profile_size)/Math.log(2)))+add_size;
			
			nb_of_bitset = nb_bitset;
			/*
			 * The minimum size of bitset is 2^min_bitset_size
			 * We do not need bitset of size below, and we need at least one bitset
			 * */
			if(log_2_size < min_bitset_size) {
				nb_of_bitset = log_2_size + nb_bitset - min_bitset_size;
				if(nb_of_bitset < 1) {
					nb_of_bitset = 1;
				}
				log_2_size = min_bitset_size;
			}
			
			bitset_size = power(2,(log_2_size + nb_of_bitset));
			dataset.put(user,(new BitSet(bitset_size)));				
			for(int item: dataset_.getRatedItems(user)) {
//				int hash = util.HashFunctions.get_hash(item, bitset_size);
				hash = item % bitset_size;
				dataset.get(user).set(hash);
			}	
		}
	}
	
	public DatasetABC_(String fileName, double initValue, Dataset dataset_, KNNGraph knng, int size_sampling, int min_bitset_size, int add_size) throws IOException {
		this.estimNbBitset = true;
		this.add_size = add_size;
		this.min_bitset_size = min_bitset_size;
		this.dataset = new HashMap<Integer,BitSet>();
		this.profiles_size = new HashMap<Integer,Integer>();
		this.initValue = initValue;

		double minmax = util.Estimator.estimMinMinMax(dataset_, knng, size_sampling);
		this.nb_bitset = (int) (Math.ceil((Math.log(1/minmax)/Math.log(2))));
		
		HashMap<Integer,HashMap<Integer,Double>> temporary_dataset= new HashMap<Integer,HashMap<Integer,Double>>();
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		
		int user;
		int item;
		double rating;
		String[] rating_results;
		while ((ligne=io.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if(rating > 3 + this.initValue) {
				if (temporary_dataset.get(user) == null) {
					temporary_dataset.put(user, new HashMap<Integer,Double>());
				}
				temporary_dataset.get(user).put(item,rating);
			}
		}
		io.close();
		rating_results = null;
		

		int profile_size;
		int log_2_size;
		int nb_of_bitset;
		int bitset_size;
		int hash;
		for(int user_id: temporary_dataset.keySet()) {
			profile_size = temporary_dataset.get(user_id).keySet().size();
			profiles_size.put(user_id, profile_size);
			log_2_size = (int) (Math.log(profile_size)/Math.log(2))+add_size;
			
			nb_of_bitset = nb_bitset;
			/*
			 * The minimum size of bitset is 2^min_bitset_size=64
			 * We do not need bitset of size below, and we need at least one bitset
			 * */
			if(log_2_size < min_bitset_size) {
				nb_of_bitset = log_2_size + nb_bitset - min_bitset_size;
				if(nb_of_bitset < 1) {
					nb_of_bitset = 1;
				}
				log_2_size = min_bitset_size;
			}
			
			bitset_size = power(2,(log_2_size + nb_of_bitset));
			dataset.put(user_id,(new BitSet(bitset_size)));
			for(int item_id: temporary_dataset.get(user_id).keySet()) {
//				int hash = util.HashFunctions.get_hash(item, bitset_size);
				hash = item_id % bitset_size;
				dataset.get(user_id).set(hash);
			}
		}
		
		temporary_dataset = null;
	}
	
	
//	public DatasetABC_(String fileName, double initValue) throws IOException {
//		this(fileName,10,initValue);
//	}
//	
//	public DatasetABC_(String fileName, int nb_bitset, double initValue) throws IOException {
//		this(fileName,nb_bitset,6,3,initValue);
//	}
//	
	public DatasetABC_(String fileName, int nb_bitset, int min_bitset_size, int size_add, double initValue) throws IOException {
		this.dataset = new HashMap<Integer,BitSet>();
		this.profiles_size = new HashMap<Integer,Integer>();

		this.estimNbBitset = false;
		this.initValue = initValue;
		this.nb_bitset = nb_bitset;
		this.min_bitset_size = min_bitset_size;
		this.add_size = size_add;
		
		HashMap<Integer,HashMap<Integer,Double>> temporary_dataset= new HashMap<Integer,HashMap<Integer,Double>>();
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;

		int user;
		int item;
		double rating;
		String[] rating_results;
		while ((ligne=io.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if(rating > 3 + this.initValue) {
				if (temporary_dataset.get(user) == null) {
					temporary_dataset.put(user, new HashMap<Integer,Double>());
				}
				temporary_dataset.get(user).put(item,rating);
			}
		}
		io.close();
		rating_results = null;
		

		int profile_size;
		int log_2_size;
		int nb_of_bitset;
		int bitset_size;
		int hash;
		for(int user_id: temporary_dataset.keySet()) {
			profile_size = temporary_dataset.get(user_id).keySet().size();
			profiles_size.put(user_id, profile_size);
			log_2_size = (int) (Math.ceil(Math.log(profile_size)/Math.log(2)))+add_size;
			
			nb_of_bitset = nb_bitset;
			/*
			 * The minimum size of bitset is 2^min_bitset_size=64
			 * We do not need bitset of size below, and we need at least one bitset
			 * */
			if(log_2_size < min_bitset_size) {
				nb_of_bitset = log_2_size + nb_bitset - min_bitset_size;
				if(nb_of_bitset < 1) {
					nb_of_bitset = 1;
				}
				log_2_size = min_bitset_size;
			}
			
			bitset_size = power(2,(log_2_size + nb_of_bitset));
			dataset.put(user_id,(new BitSet(bitset_size)));
					
			for(int item_id: temporary_dataset.get(user_id).keySet()) {
//				int hash = util.HashFunctions.get_hash(item, bitset_size);
				hash = item_id % bitset_size;
				dataset.get(user_id).set(hash);
			}
		}
		
		temporary_dataset = null;
	}
	
	@Override
	public double getRating(int user, int item) {
		BitSet bs = dataset.get(user);
//		boolean b = bs.get(util.HashFunctions.hash(item, bs.size()));
		if (bs.get(util.HashFunctions.hash(item, bs.size()))) return 1;
		else return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		BitSet bs = dataset.get(user);
//		boolean b = bs.get(util.HashFunctions.hash(item, bs.size()));
		return bs.get(util.HashFunctions.hash(item, bs.size()));
	}

	@Override
	public double sim(int user1, int user2) {
		
		int size1 = profiles_size.get(user1);
		int size2 = profiles_size.get(user2);
		
		int min;
		int max;
		boolean b = size1<size2;
		if(b){
			min = size1;
			max = size2;
		}
		else{
			min = size2;
			max = size1;
		}
		if(min * (2^nb_bitset) < max) {
//			return 0;
			return (((double) min)/((double) max));
		}
		
		BitSet bs1 = dataset.get(user1);
		BitSet bs2 = dataset.get(user2);
		int sizebs1 = bs1.size();
		int sizebs2 = bs2.size();
//		int sizebs1 = bs1.length();
//		int sizebs2 = bs2.length();
		double score = 0;
		BitSet inter;
		int size;
		if(b) {
			inter = (BitSet) bs1.clone();
			size = sizebs1;
			if(sizebs2==size){
				inter.and(bs2);
			}
			else{
				inter.and(compress(bs2,size));
			}
		}
		else {
			inter = (BitSet) bs2.clone();
			size = sizebs2;
			if(sizebs1==size){
				inter.and(bs1);
			}
			else{
				inter.and(compress(bs1,size));
			}
		}
//		System.out.println(size);
		int hamming = inter.cardinality();
//		System.out.println(size + " " + ((double) hamming) / ((double) (size1 + size2 - hamming)));
		score = ((double) hamming) / ((double) (size1 + size2 - hamming));
		return score;
	}
	
	
	public int common_size(int user1, int user2) {
		int size1 = profiles_size.get(user1);
		int size2 = profiles_size.get(user2);
		int min;
		int max;
		boolean b = size1<size2;
		if(b){
			min = size1;
			max = size2;
		}
		else{
			min = size2;
			max = size1;
		}
		if(min * (2^nb_bitset) < max) {
			return 0;
		}
		else {
			if(b) {
				return dataset.get(user1).size();
			}
			else {
				return dataset.get(user2).size();
			}
		}
	}

	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		BitSet bitset = dataset.get(user);
		Set<Integer> set = new HashSet<Integer>();
		for(int i=0; i<bitset.size();i++){
			if (bitset.get(i)){
				set.add(i);
			}
		}
		return set;
	}

	@Override
	public double getInitValue() {
		return initValue;
	}
	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetABC_);
		dic.put("nb_bitset", Integer.toString(nb_bitset));
//		dic.put(util.Names.nb_bitset, Integer.toString(nb_bitset));
		dic.put("add_size", Integer.toString(add_size));
//		dic.put(util.Names.add_size, Integer.toString(add_size));
		dic.put("min_bitset_size", Integer.toString(min_bitset_size));
//		dic.put(util.Names.min_bitset_size, Integer.toString(min_bitset_size));
		dic.put("estimNbBitset", Boolean.toString(estimNbBitset));
//		dic.put(util.Names.optionEstimNbBitsetShort, Boolean.toString(estimNbBitset));
		dic.put(util.Names.initValue, Double.toString(initValue));
		return dic;
	}
	
	private static int power(int base, int power){
		return (int) Math.pow(base, power);
	}
	
	private static BitSet compress(BitSet bs, int size) {
//		BitSet new_bs = new BitSet(size);
		long[] bs_ = bs.toLongArray();
		if(size/64 == 0) {
			System.out.println("error");
			System.out.println(size);
			System.out.println(bs);
		}
		long[] new_bs = new long[size/64]; //new_bs.toLongArray();
		for(int i = 0; i < bs_.length; i++) {
			int index_ = i%new_bs.length;
			new_bs[index_] = new_bs[index_]|bs_[i];
		}
		return BitSet.valueOf(new_bs);
//		return new_bs;
	}

}
