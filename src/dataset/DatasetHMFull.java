package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class DatasetHMFull implements Dataset{
	private final Map<Integer, Map<Integer,Double>> dataset;
	private final Map<Integer, Integer> norm;
	
	private final double initValue;

	/*
	 * We store all the values, INCLUDING the negative ones
	 */
	

	public String structure() {
		return util.Names.HashMapFull;
	}


	public DatasetHMFull(Map<Integer, Integer> norm, Map<Integer, Map<Integer,Double>> dataset, double initValue, int[] users) {
		this.norm = new HashMap<Integer, Integer>();
		this.dataset = new HashMap<Integer, Map<Integer,Double>>();
		this.initValue = initValue;
		for(int user: users) {
			this.norm.put(user, norm.get(user));
			this.dataset.put(user, dataset.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetHMFull(norm,dataset,initValue,users));
	}
	
	

	public DatasetHMFull(String fileName, double initValue) throws IOException { //Add double initValue = 0?
		dataset = new HashMap<Integer, Map<Integer,Double>>();
		norm = new HashMap<Integer, Integer>();
		
		this.initValue = initValue;
		
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		while ((ligne=io1.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (dataset.get(user) == null) {
				dataset.put(user, new HashMap<Integer,Double>());
				norm.put(user, 0);
			}
			dataset.get(user).put(item,rating);
			if(rating > initValue + 3.0) {
				norm.put(user, norm.get(user)+1);
			}
			
		}
		io1.close();

	}

	public DatasetHMFull(String fileName) throws IOException { //Add double initValue = 0?
		this(fileName,0.0);
	}


	@Override
	public double getRating(int user, int item) {
		if(dataset.get(user) != null) {
			return dataset.get(user).getOrDefault(item, initValue);
		}
		else {
			return initValue;
		}
//		double rating = initValue;
//		if(dataset.get(user) != null) {
//			if(dataset.get(user).get(item) != null) {
//				rating = dataset.get(user).get(item);
//			}
//		}
//		return rating;
	}

	@Override
	public boolean hasRated(int user, int item) {
		return (dataset.get(user) != null && dataset.get(user).get(item) != null);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	JACCARD
	
	@Override
	public double sim(int user1, int user2) {
		Map<Integer, Double> profile1 = dataset.get(user1);
		Map<Integer, Double> profile2 = dataset.get(user2);
		double sim = 0;
		Set<Integer> set1 = profile1.keySet();
		Set<Integer> set2 = profile2.keySet();
		double r1;
		for(int item : set1) {
			r1 = profile1.get(item);
			if(r1 > (initValue+3)) {
				if (set2.contains(item) && profile2.getOrDefault(item, 0.)> (initValue+3)) {
					sim++;
				}
			}
		}
//		double dist = set1.size() + set2.size() - sim;
		double dist = norm.get(user1) + norm.get(user2) - sim;
		if (dist != 0) {
			sim = sim / dist;
		}
		return sim;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	COSINE SIMILARITY
	
//	@Override
//	public double sim(int user1, int user2) {
//
//		Map<Integer, Double> profile1 = dataset.get(user1);
//		Map<Integer, Double> profile2 = dataset.get(user2);
//		double sim = 0;
////		double dist1 = 0;
////		double dist2 = 0;
//		
//		
//		double dist1 = norm.get(user1);
//		double dist2 = norm.get(user2);
//		
//		
//		double common = 0;
//
//		for(int item : profile1.keySet()) {
//			double r1 = profile1.get(item);
////			dist1 = dist1 + (r1 * r1);
//			if(r1 != initValue) { //TO change into r1 != initValue
//				double r2 = 0;
//				if(profile2.get(item)!=null && profile2.get(item)!=initValue) {
//					r2 = profile2.get(item);
//				}
//				common = common + r1 * r2;
////				common = common + r1 * profile2.getOrDefault(item, 0.0);
//			}
//		}
////		for(int item : profile2.keySet()) {
////			double r2 = profile2.get(item);
////			dist2 = dist2 + (r2 * r2);
////		}
//		if (dist1 != 0 && dist2 != 0) {
////			sim = common / Math.sqrt(dist1 * dist2);
//			
//			//
//			sim = common / (dist1 * dist2);
//		}
////		System.out.println(sim);
//		return sim;
//	}

//	@Override
//	public double sim(Object profile, int user1, int user2) {
//
//		@SuppressWarnings("unchecked")
//		Map<Integer, Double> profile1 = (Map<Integer, Double>) profile;
//		Map<Integer, Double> profile2 = dataset.get(user2);
//		double sim = 0;
////		double dist1 = 0;
////		double dist2 = 0;
//		
//		//
//		double dist1 = norm.get(user1);
//		double dist2 = norm.get(user2);
//		
//		double common = 0;
//
//		for(int item : profile1.keySet()) {
//			double r1 = profile1.get(item);
////			dist1 = dist1 + (r1 * r1);
//			if(r1 != initValue) { //TO change into r1 != initValue
//				double r2 = 0;
//				if(profile2.get(item)!=null) {
//					r2 = profile2.get(item);
//				}
//				common = common + r1 * r2;
////				common = common + r1 * profile2.getOrDefault(item, 0.0);
//			}
//		}
////		for(int item : profile2.keySet()) {
////			double r2 = profile2.get(item);
////			dist2 = dist2 + (r2 * r2);
////		}
//		if (dist1 != 0 && dist2 != 0) {
////			sim = common / Math.sqrt(dist1 * dist2);
//			sim = common / (dist1 * dist2);
//		}
//		return sim;
//	}
//
//	@Override
//	public double sim(Object profile, int user2) {
//
//		@SuppressWarnings("unchecked")
//		Map<Integer, Double> profile1 = (Map<Integer, Double>) profile;
//		Map<Integer, Double> profile2 = dataset.get(user2);
//		double sim = 0;
//		double dist1 = 0;
////		double dist2 = 0;
//		
//		//
////		double dist1 = norm.get(user1);
//		double dist2 = norm.get(user2);
//		
//		double common = 0;
//
//		for(int item : profile1.keySet()) {
//			double r1 = profile1.get(item);
//			dist1 = dist1 + (r1 * r1);
//			if(r1 != initValue) { //TO change into r1 != initValue
//				double r2 = 0;
//				if(profile2.get(item)!=null) {
//					r2 = profile2.get(item);
//				}
//				common = common + r1 * r2;
////				common = common + r1 * profile2.getOrDefault(item, 0.0);
//			}
//		}
////		for(int item : profile2.keySet()) {
////			double r2 = profile2.get(item);
////			dist2 = dist2 + (r2 * r2);
////		}
//		if (dist1 != 0 && dist2 != 0) {
////			sim = common / Math.sqrt(dist1 * dist2);
//			sim = common / (Math.sqrt(dist1) * dist2);
//		}
//		return sim;
//	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		if(dataset.get(user) == null) {
			return (new HashSet<Integer>());
		}
		else {
			return dataset.get(user).keySet();
		}
	}

	
	@Override
	public double getInitValue() {
		return initValue;
	}
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetHM);
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}


}
