package dataset.extend;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import dataset.Dataset;
import util.IntDoublePair;
import util.KNN;

public final class DatasetMostLeastPopCorrected implements Dataset{
//	private final Map<Integer, Map<Integer,Double>> dataset;
	private final int B;
	private final Map<Integer, Integer> norm;
	private final Map<Integer, Double> ratio;

	private final Map<Integer, Set<Integer>> dataset_;
	
	private final double initValue;
	/*
	 * We do NOT store the negative ratings
	 * For each user, we keep its B most popular items and its B least popular items.
	 */
	

	public String structure() {
		return util.Names.MLPC;
	}
	

	public DatasetMostLeastPopCorrected(Map<Integer, Integer> norm, Map<Integer, Double> ratio, Map<Integer, Set<Integer>> dataset_, int B, double initValue, int[] users/*, Map<Integer,Integer> mapID*/) {
		this.norm = new HashMap<Integer, Integer>();
		this.ratio = ratio;
		this.dataset_ = new HashMap<Integer, Set<Integer>>();
		this.initValue = initValue;
		this.B = B;
		for(int user: users) {
			this.norm.put(user, norm.get(user));
			this.dataset_.put(user, dataset_.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetMostLeastPopCorrected(norm,ratio,dataset_,B,initValue,users/*,mapID*/));
	}
	

	public DatasetMostLeastPopCorrected(String fileName, int B, double initValue) throws IOException { //Add double initValue = 0?
//		dataset = new HashMap<Integer, Map<Integer,Double>>();
		dataset_ = new HashMap<Integer, Set<Integer>>();
		norm = new HashMap<Integer, Integer>();
		ratio = new HashMap<Integer, Double>();
		this.B = B;
		
		this.initValue = initValue;
		
		HashMap<Integer,Integer> popularity = new HashMap<Integer,Integer>();
		
		
		
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
//		int userID;
		int user;
		int item;
		double rating;
		HashMap<Integer, Set<Integer>> dataset_aux = new HashMap<Integer, Set<Integer>>();
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (rating > initValue + 3.0) {

				user = Integer.parseInt(rating_results[0]);
				
				item = Integer.parseInt(rating_results[1]);
				if (dataset_aux.get(user) == null) {
					dataset_aux.put(user, new HashSet<Integer>());
					norm.put(user, 0);
				}
				dataset_aux.get(user).add(item);
				norm.put(user, norm.get(user)+1);
				
				if(!popularity.containsKey(item)) {
					popularity.put(item, 0);
				}
				popularity.put(item, popularity.get(item)+1);
			}
			
		}
		io1.close();
		
		
		KNN mostpop;
		KNN leastpop;
		Set<Integer> profile;
		
		for(int userID: dataset_aux.keySet()) {
			profile = dataset_aux.get(userID);
			if (profile.size() <= 2 * B) {
				dataset_.put(userID, profile);
				ratio.put(userID, ((double) profile.size())/((double) norm.get(userID)));
				continue;
			}
			
			mostpop = new KNN(B);
			leastpop = new KNN(B);
			for(int itemID: dataset_aux.get(userID)) {
				mostpop.add(new IntDoublePair(itemID,popularity.get(itemID)));
				leastpop.add(new IntDoublePair(itemID,Double.MAX_VALUE-popularity.get(itemID)));
			}
			profile = new HashSet<Integer>();
			for(int itemID: mostpop.itemArray()) {
				profile.add(itemID);
			}
			for(int itemID: leastpop.itemArray()) {
				profile.add(itemID);
			}
			profile.remove(-1);
			dataset_.put(userID, profile);
			ratio.put(userID, ((double) profile.size())/((double) norm.get(userID)));
//			ratio.put(userID, ((double) dataset_.get(userID).size())/((double) norm.get(userID)));
		}
		popularity=null;
//		System.out.println(dataset_aux.keySet().size());
//		System.out.println(dataset_.keySet().size());
	}

	public DatasetMostLeastPopCorrected(String fileName) throws IOException { //Add double initValue = 0?
		this(fileName,10,0.0);
	}


	@Override
	public double getRating(int user, int item) {
		if(dataset_.get(user).contains(item)) {
			return 5;
		}
		else {
			return initValue;
		}
	}

	@Override
	public boolean hasRated(int user, int item) {
//		return (dataset.get(user) != null && dataset.get(user).get(item) != null);
		return (dataset_.get(user) != null && dataset_.get(user).contains(item));
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
////	JACCARD
//
//	@Override
//	public double sim(int user1, int user2) {
//		double sim = 0;
////		Set<Integer> set1 = dataset.get(user1).keySet();
////		Set<Integer> set2 = dataset.get(user2).keySet();
//		Set<Integer> set1 = dataset_.get(user1);
//		Set<Integer> set2 = dataset_.get(user2);
////		if (set2 != null) {
//			for(int item:set1) {
//				if (set2.contains(item)) {
//					sim++;
//				}
//			}
////		}
//		double dist = set1.size() + set2.size() - sim;
////		if (sim != 0) {
////			double dist = norm.get(user1) + norm.get(user2) - sim;
//			if (dist != 0) {
//				sim = sim / dist;
//			}
////		}
//		return sim;
//	}
	
//	JACCARD: scaled because of the sampling
	
	@Override
	public double sim(int user1, int user2) {
		double sim = 0;
		Set<Integer> set1 = dataset_.get(user1);
		Set<Integer> set2 = dataset_.get(user2);
		for(int item:set1) {
			if (set2.contains(item)) {
				sim++;
			}
		}	

		sim = sim * (1/(ratio.get(user1) * ratio.get(user2)));
		double dist = norm.get(user1) + norm.get(user2) - sim;
		
//		double dist = set1.size() + set2.size() - sim;
		
		if (dist != 0) {
			sim = sim / dist;
		}
		return sim;
	}
	
	
	
	
	
	
	
	
	@Override
	public Set<Integer> getUsers() {
//		return dataset.keySet();
		return dataset_.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
//		if(dataset.get(user) == null) {
		if(dataset_.get(user) == null) {
			return (new HashSet<Integer>());
		}
		else {
//			return dataset.get(user).keySet();
			return dataset_.get(user);
		}
	}

	
	@Override
	public double getInitValue() {
		return initValue;
	}
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.MLPC);
		dic.put(util.Names.initValue, Double.toString(initValue));
		dic.put(util.Names.size, Double.toString(this.B));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}


}
