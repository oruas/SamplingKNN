package dataset.extend;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import dataset.Dataset;

public final class DatasetSamplingConstantProbCorrected implements Dataset{
//	private final Map<Integer, Map<Integer,Double>> dataset;
	private final double prob;
	private final Map<Integer, Integer> norm; //Real norm
	private final Map<Integer, Double> ratio;

	private final Map<Integer, Set<Integer>> dataset_;
	
	private final double initValue;
	
	/*
	 * We do NOT store the negative ratings
	 * We keep every rating with probability prob.
	 */
	

	public String structure() {
		return util.Names.SCPC;
	}
	

	public DatasetSamplingConstantProbCorrected(Map<Integer, Integer> norm, Map<Integer, Double> ratio, Map<Integer, Set<Integer>> dataset_, double prob, double initValue, int[] users/*, Map<Integer,Integer> mapID*/) {
		this.norm = new HashMap<Integer, Integer>();
		this.ratio = new HashMap<Integer, Double>();
		this.dataset_ = new HashMap<Integer, Set<Integer>>();
		this.initValue = initValue;
		this.prob = prob;
		for(int user: users) {
			this.norm.put(user, norm.get(user));
			this.dataset_.put(user, dataset_.get(user));
			this.ratio.put(user, ratio.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetSamplingConstantProbCorrected(norm,ratio,dataset_,prob,initValue,users/*,mapID*/));
	}
	

	public DatasetSamplingConstantProbCorrected(String fileName, double prob, double initValue) throws IOException { //Add double initValue = 0?
//		dataset = new HashMap<Integer, Map<Integer,Double>>();
		dataset_ = new HashMap<Integer, Set<Integer>>();
		norm = new HashMap<Integer, Integer>();
		ratio = new HashMap<Integer,Double>();
		this.prob = prob;
		
		this.initValue = initValue;
		
		
		
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
//		int userID;
		int user;
		int item;
		double rating;
		Random rand = new Random();
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (rating > initValue + 3.0) {

				user = Integer.parseInt(rating_results[0]);
				
				if(!norm.containsKey(user)) {
//					ratio.put(user, 0.);
					norm.put(user, 0);
				}
//				ratio.put(user, ratio.get(user)+1);
				norm.put(user, norm.get(user)+1);
				
				//We skip the rating with a probability equals to 1-prob
				if(rand.nextDouble() > prob) {
					continue;
				}
				
				
				
				item = Integer.parseInt(rating_results[1]);
				if (dataset_.get(user) == null) {
					dataset_.put(user, new HashSet<Integer>());
//					norm.put(user, 0);
				}
				dataset_.get(user).add(item);
//				norm.put(user, norm.get(user)+1);
				
			}
			
		}
		io1.close();
		for(int user_id: dataset_.keySet()) {
			ratio.put(user_id, ((double) dataset_.get(user_id).size())/((double) norm.get(user_id)));
		}
	}

	public DatasetSamplingConstantProbCorrected(String fileName) throws IOException { //Add double initValue = 0?
		this(fileName,0.5,0.0);
	}


	@Override
	public double getRating(int user, int item) {
		if(dataset_.get(user).contains(item)) {
			return 5;
		}
		else {
			return initValue;
		}
	}

	@Override
	public boolean hasRated(int user, int item) {
//		return (dataset.get(user) != null && dataset.get(user).get(item) != null);
		return (dataset_.get(user) != null && dataset_.get(user).contains(item));
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	JACCARD: scaled because of the sampling
	
	@Override
	public double sim(int user1, int user2) {
		double sim = 0;
		Set<Integer> set1 = dataset_.get(user1);
		Set<Integer> set2 = dataset_.get(user2);
		for(int item:set1) {
			if (set2.contains(item)) {
				sim++;
			}
		}	

//		System.out.println("correcting value: " + (1/(ratio.get(user1) * ratio.get(user2))));
		sim = sim * (1/(ratio.get(user1) * ratio.get(user2)));
		double dist = norm.get(user1) + norm.get(user2) - sim;
		if (dist != 0) {
			sim = sim / dist;
		}
		return sim;
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public Set<Integer> getUsers() {
//		return dataset.keySet();
		return dataset_.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
//		if(dataset.get(user) == null) {
		if(dataset_.get(user) == null) {
			return (new HashSet<Integer>());
		}
		else {
//			return dataset.get(user).keySet();
			return dataset_.get(user);
		}
	}

	
	@Override
	public double getInitValue() {
		return initValue;
	}
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.SCPC);
		dic.put(util.Names.initValue, Double.toString(initValue));
		dic.put(util.Names.prob, Double.toString(this.prob));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}


}
