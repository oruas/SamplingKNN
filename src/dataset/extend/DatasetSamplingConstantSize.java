package dataset.extend;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import dataset.Dataset;
import util.IntDoublePair;
import util.KNN;

public final class DatasetSamplingConstantSize implements Dataset{
	private final Map<Integer, Integer> norm;
	
//	private final Map<Integer, Double> ratio;

	private final Map<Integer, Set<Integer>> dataset_;
	
	private final int maxsize;
	
	private final double initValue;

	/*
	 * We do NOT store the negative ratings
	 * If the profile of an user is bigger than maxsize, we randomly select maxsize items to be its new profile.
	 */
	

	public String structure() {
		return util.Names.SCS;
	}
	

	public DatasetSamplingConstantSize(Map<Integer, Integer> norm, Map<Integer, Set<Integer>> dataset_, int maxsize, double initValue, int[] users/*, Map<Integer,Integer> mapID*/) {
		this.norm = new HashMap<Integer, Integer>();
//		this.ratio = ratio;
		this.dataset_ = new HashMap<Integer, Set<Integer>>();
		this.initValue = initValue;
		this.maxsize = maxsize;
		for(int user: users) {
			this.norm.put(user, norm.get(user));
			this.dataset_.put(user, dataset_.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetSamplingConstantSize(norm,dataset_,maxsize,initValue,users));
	}
	

	public DatasetSamplingConstantSize(String fileName, int maxsize, double initValue) throws IOException {
		dataset_ = new HashMap<Integer, Set<Integer>>();
		norm = new HashMap<Integer, Integer>();
//		ratio = new HashMap<Integer, Double>();
		this.maxsize = maxsize;
		
		this.initValue = initValue;

		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
//		int userID;
		int user;
		int item;
		double rating;
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (rating > initValue + 3.0) {

				user = Integer.parseInt(rating_results[0]);
				
				item = Integer.parseInt(rating_results[1]);
				if (dataset_.get(user) == null) {
					dataset_.put(user, new HashSet<Integer>());
//					norm.put(user, 0);
				}
				dataset_.get(user).add(item);
//				norm.put(user, norm.get(user)+1);
//				}
			}
			
		}
		io1.close();
		
		
		Set<Integer> profile;
		KNN new_profile;
		Random random = new Random();
		for(int userID: dataset_.keySet()) {
//			ratio.put(userID, ((double) maxsize)/((double) norm.get(userID)));
			profile = dataset_.get(userID);
			if(profile.size() > maxsize) {
				new_profile = new KNN(maxsize);
				for(int itemID: profile) {
					new_profile.add(new IntDoublePair(itemID,random.nextDouble()));
				}
				profile = new HashSet<Integer>();
				for(int i: new_profile.itemArray()) {
					profile.add(i);
				}
				dataset_.put(userID, profile);
			}
			norm.put(userID, profile.size());
		}

	}

	public DatasetSamplingConstantSize(String fileName) throws IOException { //Add double initValue = 0?
		this(fileName,20,0.0);
	}


	@Override
	public double getRating(int user, int item) {
		if(dataset_.get(user).contains(item)) {
			return 5;
		}
		else {
			return initValue;
		}
	}

	@Override
	public boolean hasRated(int user, int item) {
		return (dataset_.get(user) != null && dataset_.get(user).contains(item));
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	JACCARD: scaled because of the sampling
	
	@Override
	public double sim(int user1, int user2) {
		double sim = 0;
		Set<Integer> set1 = dataset_.get(user1);
		Set<Integer> set2 = dataset_.get(user2);
		for(int item:set1) {
			if (set2.contains(item)) {
				sim++;
			}
		}	

//		sim = sim * (1/(ratio.get(user1) * ratio.get(user2)));
		double dist = norm.get(user1) + norm.get(user2) - sim;
		if (dist != 0) {
			sim = sim / dist;
		}
		return sim;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset_.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		if(dataset_.get(user) == null) {
			return (new HashSet<Integer>());
		}
		else {
			return dataset_.get(user);
		}
	}

	
	@Override
	public double getInitValue() {
		return initValue;
	}
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.SCS);
		dic.put(util.Names.initValue, Double.toString(initValue));
		dic.put(util.Names.size, Integer.toString(maxsize));
		return dic;
	}


}
