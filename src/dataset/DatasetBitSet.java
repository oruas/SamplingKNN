package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class DatasetBitSet implements Dataset {

	private final int size_sketchs;
	
//	private final Map<Integer,long[]> dataset;
	
	private final Map<Integer, Integer> norm;

	private final Map<Integer, Set<Integer>> dataset_;
	
	private final double initValue;
	

	public String structure() {
		return util.Names.BitSet;
	}
	

	public DatasetBitSet(Map<Integer, Integer> norm, Map<Integer, Set<Integer>> dataset_, int size, double initValue, int[] users/*, Map<Integer,Integer> mapID*/) {
		this.norm = new HashMap<Integer, Integer>();
		this.dataset_ = new HashMap<Integer, Set<Integer>>();
		this.initValue = initValue;
		for(int user: users) {
			this.norm.put(user, norm.get(user));
			this.dataset_.put(user, dataset_.get(user));
		}
		this.size_sketchs = size;
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetBitSet(norm,dataset_,size_sketchs,initValue,users/*,mapID*/));
	}
	
	public DatasetBitSet(String fileName, int size, double initValue) throws IOException {
		
		this.initValue = initValue;
		dataset_ = new HashMap<Integer, Set<Integer>>();
		norm = new HashMap<Integer, Integer>();
		
		
		this.size_sketchs = size;
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset_.get(user) == null && rating > initValue + 3.0) {
				dataset_.put(user, new HashSet<Integer>());
			}
			if(rating > initValue + 3.0) {
				dataset_.get(user).add(util.HashFunctions.hash(item, size_sketchs));
				
			}
		}
		io.close();
		for(int user_id: dataset_.keySet()) {
			norm.put(user_id, dataset_.get(user_id).size());
		}
	}

	@Override
	public double getRating(int user, int item) {
		if(dataset_.get(user).contains(item)) {
			return 5;
		}
		else {
			return initValue;
		}
	}

	@Override
	public boolean hasRated(int user, int item) {
		return (dataset_.get(user) != null && dataset_.get(user).contains(item));
	}

	@Override
	public double sim(int user1, int user2) {
		double sim = 0;
		Set<Integer> set1 = dataset_.get(user1);
		Set<Integer> set2 = dataset_.get(user2);
		for(int item:set1) {
			if (set2.contains(item)) {
				sim++;
			}
		}
		double dist = set1.size() + set2.size() - sim;
		if (dist != 0) {
			sim = sim / dist;
		}
		return sim;
	}
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset_.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		if(dataset_.get(user) == null) {
			return (new HashSet<Integer>());
		}
		else {
			return dataset_.get(user);
		}
	}
	
	@Override
	public double getInitValue() {
		return initValue;
	}
	
	
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.BitSet);
		dic.put(util.Names.nb_bits, Integer.toString(size_sketchs));
		dic.put(util.Names.initValue, Double.toString(initValue));
		return dic;
	}
	
	
	
}
