package dataset;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import util.BitVector;
//import util.Sketch;

public final class DatasetAC /*implements Dataset*/ {

	final double initValue;
	
	int nb_bitset = 6;
	
	final double rho;
	final int limitHM;
	final int limitBV;
	
	private final Map<Integer,Integer> hm_size;
	private final Map<Integer, int[]> hm_items;
	private final Map<Integer, BitVector[]> hm_compacted_profiles;
	private final Map<Integer, int[]> hm_compacted_profiles_size;
//	private final Map<Integer, Sketch[]> hm_highly_compacted_profiles;
	
	private Map<Integer,Set<Integer>> dataset_full;
	private Map<Integer,Set<Integer>> dataset_inverted;
	
	
	
	public DatasetAC(double rho, int limitHM, int limitBV, double initValue, String fileName) throws IOException {
		this.initValue = initValue;
		this.rho = rho;
		this.limitHM = limitHM;
		this.limitBV = limitBV;
		hm_size = new HashMap<Integer,Integer>();
		hm_items = new HashMap<Integer, int[]>();
		hm_compacted_profiles = new HashMap<Integer, BitVector[]>();
		hm_compacted_profiles_size = new HashMap<Integer, int[]>();
//		hm_highly_compacted_profiles = new HashMap<Integer, Sketch[]>();
		
		
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		
		int user;
		int item;
		double rating;
		String[] rating_results;
		while ((ligne=io.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if(rating > 3 + this.initValue) {
				if (dataset_full.get(user) == null) {
					dataset_full.put(user, new HashSet<Integer>());
				}
				dataset_full.get(user).add(item);
				
				if (dataset_inverted.get(item) == null) {
					dataset_inverted.put(item, new HashSet<Integer>());
				}
				dataset_full.get(item).add(user);
			}
		}
		io.close();
		doDataset();
		
	}
	
	
	private void doDataset() {
		Set<Integer> items;
		int profile_size;
		int log_2_size;
		int nb_of_bitset;
		for(int user:dataset_full.keySet()) {
			items = dataset_full.get(user);
			profile_size = items.size();
			hm_size.put(user, profile_size);
			if(items.size() <= limitHM) {
				hm_items.put(user, util.SetToArray.setToArray(items));
			}
			if(items.size() >= limitBV) {
				
			}
			
			log_2_size = (int) (Math.log(profile_size)/Math.log(2));
			
			nb_of_bitset = nb_bitset;
			/*
			 * The minimum size of bitset is 2^6=64
			 * We do not need bitset of size below, and we need at least one bitset
			 * */
			if(log_2_size < 6) {
				nb_of_bitset = log_2_size + nb_bitset - 6;
				if(nb_of_bitset < 1) {
					nb_of_bitset = 1;
				}
				log_2_size = 6;
			}
			
			hm_compacted_profiles.put(user, new BitVector[nb_of_bitset]);
			hm_compacted_profiles_size.put(user, new int[nb_of_bitset]);
			
			for(int i = 0; i < nb_of_bitset; i++) {
				int bitset_size = 2^(log_2_size + i);
				hm_compacted_profiles.get(user)[i] = new BitVector(bitset_size);
				hm_compacted_profiles_size.get(user)[i] = bitset_size;
				
				for(int item: items) {
					hm_compacted_profiles.get(user)[i].put(item);
				}
			}
		}
	}
	
	public void add(int user, int item) {
		int size = hm_size.get(user);
		if(size < limitHM+1) {
			hm_items.get(user)[size] = item;
			hm_size.put(user,size+1);
		}
		BitVector[] bv = this.hm_compacted_profiles.get(user);
		for(int i = 0; i < bv.length; i++) {
			bv[i].put(item);
		}
	}
	
	

}
