package measures;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import dataset.Dataset;

public final class Heatmap {

	public final static void heatmap(Dataset dataset1, Dataset dataset2, String add, String fileName, int[] user1_table, int[] user2_table) throws IOException {
		heatmap(dataset1, dataset2, 1000, 1000, add, fileName, user1_table, user2_table);
	}

	public final static void heatmap(Dataset dataset1, Dataset dataset2, int xsize, int ysize, String add, String fileName, int[] user1_table, int[] user2_table) throws IOException {

		long[][] heatmap = new long[ysize][xsize];
		for(int i = 0; i < xsize; i++) {
			for(int j = 0; j < ysize; j++) {
				heatmap[i][j] = 0;
			}
		}



		for(int index = 0; index < user1_table.length; index++) {
			int user = user1_table[index];
			int user2 = user2_table[index];
			double score1 = dataset1.sim(user, user2);
			double score2 = dataset2.sim(user, user2);
			
			int x = (int) (score1 * (xsize-1));
			int y = (int) (score2 * (ysize-1));

			heatmap[x][y] = heatmap[x][y] + 1;
		}

		File file = new File(add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(add+fileName));
		boolean first_line = true;
		for(int i = 0; i < xsize; i++) {
			if (!first_line) {
				mbrWriterOutput.newLine();
			}
			else {
				first_line = false;
			}

			mbrWriterOutput.write("(");
			boolean first_column = true;
			for (int j = 0; j < ysize; j++) {
				if (!first_column) {
					mbrWriterOutput.write(",");
				}
				else {
					first_column = false;
				}
				mbrWriterOutput.write(Long.toString(heatmap[i][j]));
			}
			mbrWriterOutput.write(")");
		}


		mbrWriterOutput.close();
	}
	
	
	
	
	
	

	public final static void heatmap(Dataset dataset1, Dataset dataset2, String add, String fileName) throws IOException {
		heatmap(dataset1, dataset2, 1000, 1000, add, fileName);
	}

	public final static void heatmap(Dataset dataset1, Dataset dataset2, int xsize, int ysize, String add, String fileName) throws IOException {

		long[][] heatmap = new long[ysize][xsize];
		for(int i = 0; i < xsize; i++) {
			for(int j = 0; j < ysize; j++) {
				heatmap[i][j] = 0;
			}
		}



		for(int user : dataset1.getUsers()) {
			for(int user2: dataset1.getUsers()) {
//				if(user != user2) {
				if(user < user2) {
					double score1 = dataset1.sim(user, user2);
					double score2 = dataset2.sim(user, user2);

					int x = (int) (score1 * (xsize-1));
					int y = (int) (score2 * (ysize-1));

					heatmap[x][y] = heatmap[x][y] + 1;
				}
			}
		}

		File file = new File(add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(add+fileName));
		boolean first_line = true;
		for(int i = 0; i < xsize; i++) {
			if (!first_line) {
				mbrWriterOutput.newLine();
			}
			else {
				first_line = false;
			}

			mbrWriterOutput.write("(");
			boolean first_column = true;
			for (int j = 0; j < ysize; j++) {
				if (!first_column) {
					mbrWriterOutput.write(",");
				}
				else {
					first_column = false;
				}
				mbrWriterOutput.write(Long.toString(heatmap[i][j]));
			}
			mbrWriterOutput.write(")");
		}


		mbrWriterOutput.close();
	}
}
