package measures;

import dataset.Dataset;
import util.CounterHeatMaps;

public class NumberCompZeroRunnableCounters implements Runnable{

	Dataset dataset;
	Dataset dataset2;
	int[] users;
	CounterHeatMaps chm;
	
	public NumberCompZeroRunnableCounters(Dataset dataset, Dataset dataset2, int[] users, CounterHeatMaps chm) {
		this.dataset = dataset;
		this.dataset2 = dataset2;
		this.users = users;
		this.chm = chm;
	}
	
	@Override
	public void run() {
		for(int user1: users) {
			for(int user2: dataset.getUsers()) {
				if (user1 < user2) {
					double sim1 = dataset.sim(user1, user2);
					double sim2 = dataset2.sim(user1, user2);
					chm.add(sim1, sim2);
				}
			}
		}
	}

}
