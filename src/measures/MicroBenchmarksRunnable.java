package measures;

import dataset.Dataset;

public class MicroBenchmarksRunnable implements Runnable{

	Dataset dataset;
	int[] users1;
	int[] users2;
	int nb_test;
	
	public MicroBenchmarksRunnable(Dataset dataset, int[] users1, int[] users2) {
		this.dataset = dataset;
		this.users1 = users1;
		this.users2 = users2;
		this.nb_test = users1.length;
	}
	
	@Override
	public void run() {
//		for(int test = 0 ; test < nb_test; test++) {
//			dataset.sim(users1[test], users2[test]);
//		}
		for(int user1: users1) {
			for(int user2: users2) {
				dataset.sim(user1, user2);
			}
		}
	}

}
