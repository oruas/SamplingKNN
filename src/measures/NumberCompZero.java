package measures;

import dataset.Dataset;
import util.Counter;
import util.CounterHeatMaps;

public class NumberCompZero {
	
	public static long nb_comp_zero(Dataset dataset, double min_score, int nb_thread) {
		Thread[] threads = new Thread[nb_thread];
		Counter[] counters = new Counter[nb_thread];
		Counter[] counters_comp = new Counter[nb_thread];
		int[] users = util.SetToArray.setToArray(dataset.getUsers());
		
		for (int i=0; i<nb_thread; i++) {
			int[] users_aux = util.ThreadRepartition.pivot_repartition(users, nb_thread, i);
			counters[i] = new Counter();
			counters_comp[i] = new Counter();
			threads[i] = new Thread(new NumberCompZeroRunnable(dataset,users_aux,counters[i],counters_comp[i],min_score));
		}
		
		for (int i=0; i<nb_thread; i++) {
			threads[i].start();
		}
		for (int i=0; i<nb_thread; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		long count = 0;
		long count_comp = 0;
		for (int i=0; i<nb_thread; i++) {
			count = count + counters[i].getValue();
			count_comp = count_comp + counters_comp[i].getValue();
		}
		
//		System.out.println("counter_comp " + count_comp);
		return count;
	}

	
	public static void nb_comp_zero(Dataset dataset, Dataset dataset2, CounterHeatMaps chm, int nb_thread) {
		Thread[] threads = new Thread[nb_thread];
		int[] users = util.SetToArray.setToArray(dataset.getUsers());
		
		for (int i=0; i<nb_thread; i++) {
			int[] users_aux = util.ThreadRepartition.pivot_repartition(users, nb_thread, i);
			threads[i] = new Thread(new NumberCompZeroRunnableCounters(dataset,dataset2, users_aux,chm));
		}
		
		for (int i=0; i<nb_thread; i++) {
			threads[i].start();
		}
		for (int i=0; i<nb_thread; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
//		return count;
	}
	
}
