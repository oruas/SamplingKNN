package main;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import dataset.Dataset;
import dataset.DatasetHM;
import dataset.DatasetHMFull;
import util.IntDoublePair;
import util.KNNGraph;

public class TestFakeUsers {

	public static void main(String[] args) throws IOException {
		String dataset_name = "movielens1M";
		int dataset_part = 0;
		String dataset_add = "./Files/Datasets/"+dataset_name+"/TestSet"+dataset_part+".data";
		
		
		int k=30;
		int nb_reco=30;
		
		String reco_add = "./Files/results/TestFakeUsers/" + dataset_name+ "/BruteForce/k" + k + "/" + KNNmain.getTime()+"/";
		
		Dataset dataset = new DatasetHM(dataset_add,0);
		Dataset datasetFull = new DatasetHMFull(dataset_add,0);
		

//		int number_default_users=200000;
		int number_default_users = 3000;
		int number_hash=4;
		
		HashMap<Integer,HashSet<Integer>> fakeDataset = new HashMap<Integer,HashSet<Integer>>();
		HashMap<Integer,HashMap<Integer,Double>> fakeDatasetRatings = new HashMap<Integer,HashMap<Integer,Double>>();
		
		
		for(int new_user = 0; new_user < number_default_users; new_user++) {
			fakeDataset.put(new_user, new HashSet<Integer>());
			fakeDatasetRatings.put(new_user, new HashMap<Integer,Double>());
		}
		
		for(int user: dataset.getUsers()) {
			for(int hash_number=0; hash_number<number_hash; hash_number++) {
				int hash_id = util.HashFunctions.get_hash(user, hash_number, number_default_users);

				//remove for real test
				//					hash_id = user;
				for(int item: dataset.getRatedItems(user)) {
					
					fakeDataset.get(hash_id).add(item);
				}
			}
		}
		
		for(int user: dataset.getUsers()) {
			for(int hash_number=0; hash_number<number_hash; hash_number++) {
				int hash_id = util.HashFunctions.get_hash(user, hash_number, number_default_users);

				//remove for real test
				//					hash_id = user;

				for(int item: datasetFull.getRatedItems(user)) {
					double rating = datasetFull.getRating(user, item);
//				for(int item: datasetFull.getRatedItems(dataset.getID(user))) {
//					double rating = datasetFull.getRating(dataset.getID(user), item);
					fakeDatasetRatings.get(hash_id).put(item, rating);
				}
			}
		}
		
//		int user_test = 0;
//		System.out.println(dataset.getRatedItems(user_test));
//		System.out.println(datasetFull.getRatedItems(dataset.getID(user_test)));
//		System.out.println(fakeDataset.get(user_test));
//		System.out.println(fakeDatasetRatings.get(user_test).keySet());
		
		
		KNNGraph knng = new KNNGraph(k,dataset.getUsers().size(),0);
		
		for (int user: dataset.getUsers()) {
			knng.init(user, k, 0);
		}
		

		for (int user: dataset.getUsers()) {
			for(int default_user: fakeDataset.keySet()) {
				//remove the if for real test (not what is inside)
//				if (user != default_user) {
				double sim = 0;
				Set<Integer> set1 = dataset.getRatedItems(user);
				Set<Integer> set2 = fakeDataset.get(default_user);
				for(int item:set1) {
					if (set2.contains(item)) {
						sim++;
					}
				}
				double dist = set1.size() + set2.size() - sim;
				if (dist != 0) {
					sim = sim / dist;
				}
				knng.put(user, default_user, sim);
//				}
			}
		}
		

		KNNGraph reco = new KNNGraph(nb_reco,dataset.getUsers().size(),0);
//		HashMap<Integer,Double> nullHashMap = new HashMap<Integer,Double>();
		
		for (int user: dataset.getUsers()) {

			reco.init(user, nb_reco, 0);
			HashSet<Integer> candidates_items = new HashSet<Integer>();
			for(int neighbor: knng.get_neighbors_ids(user)) {
				candidates_items.addAll(fakeDataset.get(neighbor));
			}
			for(int item: candidates_items) {
//				if(!datasetFull.getRatedItems(user).contains(item)){
					double score = 0;
					double sum_sim = 0;
					for(IntDoublePair idp: knng.get_neighbors(user)) {
//						score = score + (idp.score * fakeDatasetRatings.getOrDefault(idp.integer,nullHashMap).getOrDefault(item, 0.));
//						score = score + (idp.score * datasetFull.getRating(dataset.getID(idp.integer), item));
						score = score + (idp.score * datasetFull.getRating(idp.integer, item));
						sum_sim = sum_sim + idp.score;
					}
					if (sum_sim != 0) {
						score = score / sum_sim;
					}
					reco.put(user, item, score);
//				}
			}
		}
		

		io.IO.KNNGraphtoJSON(knng,  reco_add, util.Names.knngFile);
		io.IO.RectoJSON(reco, reco_add, util.Names.recoFile);
//		io.IO.KNNGraphtoJSON(knng, dataset, reco_add, util.Names.knngFile);
//		io.IO.RectoJSON(reco, dataset, reco_add, util.Names.recoFile);
		
	}

}
