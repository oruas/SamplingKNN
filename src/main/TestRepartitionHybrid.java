package main;

import java.io.IOException;
//import java.util.AbstractMap;
import java.util.HashSet;
import java.util.LinkedList;
//import java.util.Map.Entry;
import java.util.Set;

import dataset.Dataset;
//import dataset.DatasetBitArrayNativeSize;
import dataset.DatasetHM;
import util.HeapIntInt;
import util.HeapSetInteger;
import util.IntIntSetInt;

public class TestRepartitionHybrid {

	
	public static void main(String[] args) throws IOException {

		int nb_bits = 4096*2*2;
//		int nb_bits = 4096;
		int nb_hash = 8;
		int nb_proc = Runtime.getRuntime().availableProcessors()+1;
		int k = 30;
		
		long start;
		long stop;
		long[][] distrib = new long[nb_hash][nb_bits];
		
		String input = "./Files/Datasets/movielens10M/TestSet0.data";
		
		
		Dataset dataset_HM = new DatasetHM(input,0);
//		Dataset dataset = new DatasetBitArrayNativeSize(input, 1024, 0);

		start = System.currentTimeMillis();
		
		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new HashSet[nb_hash][nb_bits];
		
		for(int i = 0; i < nb_hash; i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets[i][j] = new HashSet<Integer>();
			}
		}
		stop = System.currentTimeMillis();

		System.out.println("Creation of buckets: " + (stop - start) + "ms.");

		
		
		start = System.currentTimeMillis();
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		
//		long bucket_start = System.currentTimeMillis();
		Set<Integer> user_aux = dataset_HM.getUsers();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			users[index] = user;
			index++;
		}
		
//			}
			
		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < nb_hash; permutation++) {
				bucket_id = nb_bits;
				for(int item : dataset_HM.getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets[permutation][bucket_id].add(user);
			}
		}
		

		stop = System.currentTimeMillis();
		System.out.println("Filling the buckets: " + (stop - start) + "ms.");
		
//		scanrate.put(key, value)
		for(int i = 0; i < nb_hash; i++) {
			for(int j=0; j < nb_bits; j++) {
				distrib[i][j] = buckets[i][j].size();
			}
		}
		
		//REPARTITION
		start = System.currentTimeMillis();
		@SuppressWarnings("unchecked")
//		LinkedList<Entry<Integer, Integer>>[] distribution = (LinkedList<Entry<Integer,Integer>>[]) new LinkedList[nb_proc];
		LinkedList<Set<Integer>>[] distribution = (LinkedList<Set<Integer>>[]) new LinkedList[nb_proc];

		

//		int nb_knng_computed;
//		int aux;
//		int index_i;
//		int index_j;

		//OLD FILLING
//		for(int loop=0; loop<nb_proc; loop++) {
//			nb_knng_computed = 0;
//			distribution[loop] = new LinkedList<Set<Integer>>();
//
//			for(int i=0; i<loop; i++) {
//				aux =(nb_bits * nb_hash) / nb_proc;
//				if ((nb_proc % nb_proc) > (nb_proc  - (i+1))) {
//					aux++;
//				}
//				nb_knng_computed = nb_knng_computed + aux;
//			}
//			
//			int nb_knng_to_compute  = (nb_bits * nb_hash) / nb_proc;
//			if((nb_proc % nb_proc) > (nb_proc  - (loop+1))) {
//				nb_knng_to_compute++;
//			}
//			for (int iter=0; iter < nb_knng_to_compute; iter++) {
//				
//				//Setting the indexes of the bucket
//				index_i = nb_knng_computed / nb_bits;
//				index_j = nb_knng_computed % nb_bits;
//				nb_knng_computed++;
////				distribution[loop].add(new AbstractMap.SimpleEntry<Integer,Integer>(index_i, index_j));
//				distribution[loop].add(buckets[index_i][index_j]);
//			}
//		}
		
		HeapSetInteger heap_set = new HeapSetInteger(nb_hash * nb_proc);
		for(int i=0; i<nb_hash;i++) {
			for(int j=0; j<nb_bits; j++) {
				heap_set.add(new IntIntSetInt(i,j,buckets[i][j]));
			}
		}
		HeapIntInt heap_add = new HeapIntInt(nb_proc);
//		int set_added=0;
		int proc_id;
		
		for(int i=0; i<nb_proc;i++) {
			distribution[i] = new LinkedList<Set<Integer>>();
		}
		
		
		for(IntIntSetInt iisi: heap_set.toArray()) {
			buckets[iisi.index_i][iisi.index_j] = null;
			proc_id = heap_add.get_id();
			distribution[proc_id].add(iisi.set);
			heap_add.add(iisi.score); //TODO score should be the square...
		}
		Set<Integer> set_aux;
		for(int i=0; i<nb_hash;i++) {
			for(int j=0; j<nb_bits; j++) {
				set_aux = buckets[i][j];
				if(set_aux != null && set_aux.size()>1) {
					proc_id = heap_add.get_id();
					distribution[proc_id].add(set_aux);
					heap_add.add(set_aux.size());
				}
			}
		}
		
//
//		for(int loop=0; loop<nb_proc; loop++) {
//			nb_knng_computed = 0;
//			distribution[loop] = new LinkedList<Set<Integer>>();
//
//			for(int i=0; i<loop; i++) {
//				aux =(nb_bits * nb_hash) / nb_proc;
//				if ((nb_proc % nb_proc) > (nb_proc  - (i+1))) {
//					aux++;
//				}
//				nb_knng_computed = nb_knng_computed + aux;
//			}
//			
//			int nb_knng_to_compute  = (nb_bits * nb_hash) / nb_proc;
//			if((nb_proc % nb_proc) > (nb_proc  - (loop+1))) {
//				nb_knng_to_compute++;
//			}
//			for (int iter=0; iter < nb_knng_to_compute; iter++) {
//				
//				//Setting the indexes of the bucket
//				index_i = nb_knng_computed / nb_bits;
//				index_j = nb_knng_computed % nb_bits;
//				nb_knng_computed++;
////				distribution[loop].add(new AbstractMap.SimpleEntry<Integer,Integer>(index_i, index_j));
//				distribution[loop].add(buckets[index_i][index_j]);
//			}
//		}
		
		
		stop = System.currentTimeMillis();
		System.out.println("Doing the repartition: " + (stop - start) + "ms.");
		
		
		long max;
		long sum;
		long sum_square;
		long nb_users_in_bucket;
		for(int loop=0; loop<nb_proc; loop++) {
			max=0;
			sum=0;
			sum_square=0;

//			for(Entry<Integer,Integer> entry:distribution[loop]) {
			for(Set<Integer> bucket:distribution[loop]) {
//				nb_users_in_bucket = distrib[entry.getKey()][entry.getValue()];
				nb_users_in_bucket = bucket.size();
				sum = sum + nb_users_in_bucket;
				if(10*k*k<nb_users_in_bucket) {
					sum_square = sum_square + ((long) 5*k*k*((long)nb_users_in_bucket));
				}
				else {
					sum_square = sum_square + ((long) nb_users_in_bucket-1)*((long) nb_users_in_bucket/2);
				}
				if(max<nb_users_in_bucket) {
					max = nb_users_in_bucket;
				}
			}
			System.out.println("proc: " + loop + " max: " + max);
			System.out.println("proc: " + loop + " sum: " + sum);
			System.out.println("proc: " + loop + " sum_square: " + sum_square);
		}
		

		
	}
	
	
}
