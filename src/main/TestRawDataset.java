package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Set;

import algo.Algo;
import algo.BruteForce.BruteForce;
import dataset.Dataset;
//import dataset.DatasetBitArray;
//import dataset.DatasetABC;
//import dataset.DatasetABC_;
import dataset.DatasetHM;
import dataset.DatasetHMFull;
import parameters.ParametersBruteForce;
import parameters.ParametersStats;
import util.KNNGraph;

public final class TestRawDataset {

	public static void main(String[] args) throws IOException {
//		essai();
//		String input = "./Files/Datasets/LiveJournal/raw/soc-LiveJournal1.txt";
//		String output = "./Files/Datasets/LiveJournal/u.data";
//		String input = "./Files/Datasets/Friendster/raw/com-friendster.ungraph.txt";
//		String output = "./Files/Datasets/Friendster/u.data";
//		String input = "./Files/Datasets/Twitter/raw/twitter_rv.net";
//		String output = "./Files/Datasets/Twitter/u.data";
//		String output = "./Files/Datasets/Twitter/TestSet0.data";
//		String output = "./Files/Datasets/movielens1M/TestSet0.data";
//		String output = "./Files/Datasets/AmazonBooks/u.data";
//		String output = "./Files/Datasets/AmazonMovies/u.data";
//		String output = "./Files/Datasets/DBLP_full/u.data";
//		String output = "./Files/Datasets/movielens1M/u.data";
//		String output = "./Files/Datasets/movielens10M/u.data";
//		String output = "./Files/Datasets/movielens20M/u.data";
		String output = "./Files/Datasets/Gowalla/u.data";
		
//		dataset.gen.CrossValidationTestSetGen.testset8020gen(output, "./Files/Datasets/Twitter/TestSet");
		
		Dataset dataset = new DatasetHMFull(output,0);
//		Dataset dataset = new DatasetBitArray(output, 256, 0);
//		int min_bitset_size = 6;
//		int add_size = 3;
//		int nb_bitset = 4;
//		Dataset dataset = new DatasetABC(output,nb_bitset,min_bitset_size,add_size, 0);
//		Dataset dataset_ = new DatasetABC_(output,nb_bitset,min_bitset_size,add_size,0);
		
//		System.out.println(dataset.sim(1, 5343));
		
//		long start1 = System.currentTimeMillis();
//		for(int user1: dataset.getUsers()) {
//			for(int user2: dataset.getUsers()) {
//				if(user1 < user2) {
//					dataset.sim(user1, user2);
//				}
//			}
//		}
//		long stop1 = System.currentTimeMillis();
//		System.out.println(stop1-start1);
		
//		long start2 = System.currentTimeMillis()
//		outerloop:
//		for(int user1: dataset_.getUsers()) {
//			for(int user2: dataset_.getUsers()) {
//				if(user1 < user2) {
//					double score1 = dataset.sim(user1, user2);
//					double score2 = dataset_.sim(user1, user2);
//					if(score1 != score2) {
//						System.out.println(score1 + " " + score2);
//						break outerloop;
//					}
//				}
//			}
//		}
		System.out.println("done");
//		long stop2 = System.currentTimeMillis();
//		System.out.println(stop2-start2);

		int nb_users_0 = 0;
		int nb_users_1 = 0;
		int nb_users_5 = 0;
		int nb_users_10 = 0;
		int nb_users_20 = 0;
		double mean_ratings = 0;
		
		for(int user : dataset.getUsers()) {
			int nb_ratings = dataset.getRatedItems(user).size();
			mean_ratings = mean_ratings + nb_ratings;
			if(nb_ratings >= 1) {
				nb_users_0++;
			}
			if(nb_ratings >= 2) {
				nb_users_1++;
			}
			if(nb_ratings >= 5) {
				nb_users_5++;
			}
			if(nb_ratings >= 10) {
				nb_users_10++;
			}
			if(nb_ratings >= 20) {
				nb_users_20++;
			}
		}
		
		System.out.println(dataset.getUsers().size());
		System.out.println(nb_users_0);
		System.out.println(((double) nb_users_0)/((double)dataset.getUsers().size()));
		System.out.println(nb_users_1);
		System.out.println(((double) nb_users_1)/((double)dataset.getUsers().size()));
		System.out.println(nb_users_5);
		System.out.println(((double) nb_users_5)/((double)dataset.getUsers().size()));
		System.out.println(nb_users_10);
		System.out.println(((double) nb_users_10)/((double)dataset.getUsers().size()));
		System.out.println(nb_users_20);
		System.out.println(((double) nb_users_20)/((double)dataset.getUsers().size()));
		System.out.println(mean_ratings/((double)dataset.getUsers().size()));
		
//		boolean undirected = false;
//		dataset.gen.AlterRawDataset.removeCommentsChangeSplitAddGoodRating(input, output, "#", "	", undirected);
		
//		InputStream ips = new FileInputStream(input);
//		InputStreamReader ipsr = new InputStreamReader(ips);
//		BufferedReader io = new BufferedReader(ipsr);
//		String ligne;
//		int nb_fail = 0;
//		long nb_list = 0;
//		while ((ligne=io.readLine())!=null){
////			System.out.println(ligne);
//			String split = "	";
//			String[] lignes = ligne.split(split);
//			if(lignes.length != 2) {
//				System.out.println(ligne);
//				nb_fail++;
//			}
//			nb_list++;
//		}		
//		io.close();
//		System.out.println(nb_list);
//		System.out.println(nb_fail);
		
		
		
//		Dataset dataset = new DatasetHM(output);
//		System.out.println(dataset.getUsers().size());
//		int nb_edges = 0;
//		Set<Integer> user_set = new HashSet<Integer>();
//		Set<Integer> larger_user_set = new HashSet<Integer>();
//		for(int user: dataset.getUsers()) {
//			nb_edges = nb_edges + dataset.getRatedItems(user).size();
//			user_set.add(user);
//			if(user >= 4847571) {
////				if(user > dataset.getUsers().size()) {
//				larger_user_set.add(user);
//			}
//		}
//		System.out.println(nb_edges);
//		Set<Integer> missing_user_set = new HashSet<Integer>();
//		for (int i = 0; i<4847571;i++) {
////			for (int i = 0; i<dataset.getUsers().size();i++) {
//			if(!user_set.contains(i)) {
////				System.out.println(i);
//				missing_user_set.add(i);
//			}
//		}
//		System.out.println(missing_user_set.size());
//		System.out.println(larger_user_set.size());
////		System.out.println(nb_edges/2);
	}

	
	
	public static void essai2() throws IOException {
		Dataset dataset = new DatasetHM("./Files/Datasets/movielens1M/TestSet0.data",0);
		Algo bf = new BruteForce(new ParametersBruteForce(3,"movielens1M",dataset,5),new ParametersStats(false, false));
		KNNGraph knng = bf.doKNN();
		double minmax = util.Estimator.estimMinMinMax(dataset, knng, 1);
		System.out.println(minmax);
		int nb_bitset = (int) (Math.ceil((Math.log(1/minmax)/Math.log(2))));
		System.out.println(nb_bitset);
	}
	
	
	public static void essai() throws IOException {

		String fileName = "./Files/Datasets/Friendster/u_full.data";
		String output = "./Files/Datasets/Friendster/u20.data";
//		String fileName = "./Files/Datasets/LiveJournal/raw/soc-LiveJournal1.txt";


//		HashMap<Integer,Set<Integer>> dataset = new HashMap<Integer, Set<Integer>>();
		HashMap<Integer,Integer> dataset = new HashMap<Integer, Integer>();

		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
//		int count = 0;
		while ((ligne=io1.readLine())!=null /*&& count < 20*/){
			if (!ligne.startsWith("#")){
				//			System.out.println(ligne);
				//			count++;
				String[] lignes = ligne.split(" ");
				int user1 = Integer.parseInt(lignes[0]);
//				int user2 = Integer.parseInt(lignes[1]);
				if (dataset.get(user1) == null) {
//					dataset.put(user1, new HashSet<Integer>());
					dataset.put(user1, 0);
				}
//				dataset.get(user1).add(user2);
				dataset.put(user1, dataset.get(user1)+1);
//				if (dataset.get(user2) == null) {
//					dataset.put(user2, new HashSet<Integer>());
//				}
//				dataset.get(user2).add(user1);
//				System.out.println(ligne);
//				if ((user1 == 101 /*&& user2 == 102*/) || (/*user2 == 101 &&*/ user1 == 102)) {
//					System.out.println(ligne);
//				}
//				break;
 			}
//			else{
//				System.out.println(ligne);
//			}
		}
		System.out.println("number of users: " + dataset.size());
		io1.close();
		
		int max = 0;
		for(int user : dataset.keySet()) {
			if (dataset.get(user)>max) {
				max = dataset.get(user);
			}
		}
		System.out.println(max);
		
		ips = new FileInputStream(fileName);
		ipsr = new InputStreamReader(ips);
		io1 = new BufferedReader(ipsr);
//		count = 0;
//		Set<Integer> users = dataset.keySet();
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(output));
		while ((ligne=io1.readLine())!=null /*&& count < 20*/){
			if (!ligne.startsWith("#")){
				//			System.out.println(ligne);
				//			count++;
				String[] lignes = ligne.split(" ");
				int user1 = Integer.parseInt(lignes[0]);
				if (dataset.get(user1)>=20) {
					mbrWriter.write(ligne);
				}
 			}
//			else{
//				System.out.println(ligne);
//			}
		}
		mbrWriter.close();
		io1.close();
	}
}
