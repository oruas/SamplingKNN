package main;

//import util.KNNGraph;

//import java.util.BitSet;

import util.IntDoublePair;
import util.KNN;

public class TestKNNAdd {

	public static void main(String[] args) {
		
//		KNNGraph knng = new KNNGraph(5,5,0);
//		
//		knng.put(0, 1, 0.1);
//		knng.put(0, 2, 0.2);
//		knng.put(0, 3, 0.3);
//		knng.put(0, 4, 0.4);
//		knng.put(0, 5, 0.5);
//		knng.put(0, 6, 0.5);
//		knng.put(0, 6, 0.5);
//
//		knng.put(1, 1, 0.1);
//		knng.put(1, 2, 0.2);
//		knng.put(1, 3, 0.3);
//		knng.put(1, 4, 0.4);
//		knng.put(1, 5, 0.5);
//		knng.put(1, 6, 0.5);
//		knng.put(1, 6, 0.5);
//		
//		
//		
//		for (int user: knng.get_users()) {
//			System.out.print(user + ":");
//			for(IntDoublePair idp: knng.get_sorted_neighbors(user)) {
//				System.out.print(" " + idp);
//			}
//			System.out.println();
//		}
		
		
		KNN knn = new KNN(5,-1);
		

		knn.add(new IntDoublePair(1,-0.1));
		knn.add(new IntDoublePair(2,-0.2));
		knn.add(new IntDoublePair(3,0.1));
		knn.add(new IntDoublePair(4,0.2));
		knn.add(new IntDoublePair(5,0));
		knn.add(new IntDoublePair(6,-0.1));
		

//		knn.add(new IntDoublePair(1,1-0));
//		knn.add(new IntDoublePair(2,1-0.1));
//		knn.add(new IntDoublePair(3,1-0.2));
//		knn.add(new IntDoublePair(4,1-0.3));
//		knn.add(new IntDoublePair(5,1-0.4));
//		knn.add(new IntDoublePair(6,1-0.5));
//		knn.add(new IntDoublePair(7,1-0.6));
//		knn.add(new IntDoublePair(8,1-0.8));
//
//		knn.add(new IntDoublePair(1,0));
//		knn.add(new IntDoublePair(2,0));
//		knn.add(new IntDoublePair(3,0));
//		knn.add(new IntDoublePair(4,0));
//		knn.add(new IntDoublePair(5,0));
//		knn.add(new IntDoublePair(6,0));
//		knn.add(new IntDoublePair(7,0));
//		knn.add(new IntDoublePair(8,0));
		
		
		System.out.println(knn);
		System.out.println(knn.min_score());
		
//		int size = power(2,30);
//		System.out.println(size);
//		BitSet bs = new BitSet(size);
//		bs.set(15);
//		bs.set(75);
//		System.out.println(bs.size());
//		System.out.println(bs);
		
		
	}

	

//	private static int power(int base, int power){
//		return (int) Math.pow(base, power);
//	}

}
