package main;

import java.io.IOException;
import dataset.Dataset;
//import dataset.DatasetABC;
import dataset.DatasetBitArray;
//import dataset.DatasetABC_;
import dataset.DatasetHM;
//import dataset.DatasetHMFull;
//import util.IntDoublePair;
import util.KNNGraph;

public final class TestHeapEfficiency {

	public static void main(String[] args) throws IOException {
		System.out.println("Used memory megabytes: " + mem());
//		String output = "./Files/Datasets/Twitter/TestSet0.data";
		String dataset_add = "./Files/Datasets/movielens10M/TestSet0.data";
		
//		Dataset dataset = new DatasetHM(output,0);
//		int min_bitset_size = 6;
//		int add_size = 3;
//		int nb_bitset = 4;
		System.out.println("Used memory megabytes: " + mem());
//		Dataset dataset = new DatasetABC(dataset_add,nb_bitset,min_bitset_size,add_size, 0);
		Dataset dataset1 = new DatasetBitArray(dataset_add,4096, 0);
		System.out.println("Used memory megabytes: " + mem());
//		Dataset dataset_ = new DatasetABC_(dataset_add,nb_bitset,min_bitset_size,add_size,0);
		Dataset dataset1_ = new DatasetHM(dataset_add,0);
		System.out.println("Used memory megabytes: " + mem());
//		Dataset dataset_ = new DatasetABC_(dataset_add,nb_bitset,min_bitset_size,add_size,0);
//		Dataset dataset1_f = new DatasetHMFull(dataset_add,0);
		System.out.println("Used memory megabytes: " + mem());
		
		Dataset[] datasets = {dataset1,dataset1_};
		
		for(Dataset dataset: datasets) { 

			long start1 = System.currentTimeMillis();
			for(int user1: dataset.getUsers()) {
				for(int user2: dataset.getUsers()) {
					if(user1 < user2) {
						dataset.sim(user1, user2);
					}
				}
			}
			long stop1 = System.currentTimeMillis();
			System.out.println("1st dataset, no heap: " + (stop1-start1));

			int nb_users = dataset.getUsers().size();

			int[] k_values = {1,30,90,200,500,1000,2000};
			//		int[] k_values = {6000};
			for(int k: k_values) {
				//			KNNGraph knng = new KNNGraph();
				KNNGraph knng = new KNNGraph(k,dataset.getUsers().size(),dataset.getInitValue());
				for(int user: dataset.getUsers()) {
					if (user < nb_users) {
						knng.init(user, k, -1);
					}
				}

				start1 = System.currentTimeMillis();
				for(int user1: dataset.getUsers()) {
					if (user1 < nb_users) {
						for(int user2: dataset.getUsers()) {
							if (user2 < nb_users) {
								if(user1 < user2) {
									double score = dataset.sim(user1, user2);
									//						knng.get_KNN(user1).add(new IntDoublePair(user2,score));
									knng.put(user1, user2, score);
									//						knng.get_KNN(user2).add(new IntDoublePair(user1,score));
									knng.put(user2, user1, score);
								}
							}
						}
					}
				}
				stop1 = System.currentTimeMillis();
				System.out.println("k: " + k + " time:" + (stop1-start1));
			}

		}
		
//		
//		start1 = System.currentTimeMillis();
//		for(int user1: dataset_.getUsers()) {
//			for(int user2: dataset_.getUsers()) {
//				if(user1 < user2) {
//					dataset_.sim(user1, user2);
//				}
//			}
//		}
//		stop1 = System.currentTimeMillis();
//		System.out.println("2nd dataset, no heap: " + (stop1-start1));
//		
////		int[] k_values = {1,30,90,200,500,1000,2000};
//		for(int k: k_values) {
////			KNNGraph knng = new KNNGraph();
//			KNNGraph knng = new KNNGraph(k,dataset.getUsers().size(),dataset.getInitValue());
//			for(int user: dataset_.getUsers()) {
//				knng.init(user, k, -1);
//			}
//			start1 = System.currentTimeMillis();
//			for(int user1: dataset_.getUsers()) {
//				for(int user2: dataset_.getUsers()) {
//					if(user1 < user2) {
//						double score = dataset_.sim(user1, user2);
////						knng.get_KNN(user1).add(new IntDoublePair(user2,score));
//						knng.put(user1, user2, score);
////						knng.get_KNN(user2).add(new IntDoublePair(user1,score));
//						knng.put(user2, user1, score);
//					}
//				}
//			}
//			stop1 = System.currentTimeMillis();
//			System.out.println("k: " + k + " time:" + (stop1-start1));
//		}
//		
//		System.out.println(dataset_f.getRatedItems(0));
//		

	}
	
	
	
	public static long mem() {
		Runtime runtime = Runtime.getRuntime();
		return ((runtime.totalMemory() - runtime.freeMemory())/(1024L * 1024L));
	}
}
