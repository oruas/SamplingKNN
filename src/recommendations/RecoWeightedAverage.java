package recommendations;

import java.io.IOException;
import java.util.HashSet;

import dataset.Dataset;
import parameters.ParametersStats;
import util.IntDoublePair;
import util.KNNGraph;

public final class RecoWeightedAverage /*implements Recommandation */{

	public static KNNGraph doRecommandation(Dataset dataset, KNNGraph knng, int nb_reco, int nb_proc) {
//		KNNGraph reco = new KNNGraph();
		KNNGraph reco = new KNNGraph(nb_reco,dataset.getUsers().size(),dataset.getInitValue());

		Thread[] threads = new Thread[nb_proc];
		

		int nb_user = knng.get_users().size();
		Object[] users_aux = knng.get_users().toArray();

		//DEF OF USERS, used to do random sampling.
		int[] users = new int[nb_user];
		for(int index = 0; index < nb_user; index++) {
			users[index] = (int) users_aux[index];
		}
		
		for(int i = 0; i < nb_proc; i++) {
			Thread t = new Thread(new RecoWeightedAverageRunnable(dataset, knng, reco, nb_reco, nb_proc, users, i));
			t.start();
			threads[i] = t;
		}
		for(int i = 0; i < nb_proc; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return reco;
	}

	public static KNNGraph launch(Dataset dataset, KNNGraph knng, int nb_reco, int nb_proc, String add) throws IOException {

		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		KNNGraph reco = doRecommandation(dataset,knng,nb_reco, nb_proc);

		if(ParametersStats.measure_time) {
			long duration = System.currentTimeMillis()-start;
			io.IO.toFile("{\"time\": "+duration+",\"nb_rec\": "+nb_reco+"}", add, util.Names.recoTimeFile);
		}
//		io.IO.RectoJSON(reco, dataset, add, util.Names.recoFile);
		io.IO.RectoJSON(reco, add, util.Names.recoFile);
		return reco;
	}
	
	
	public static KNNGraph doRecommandationSeq(Dataset dataset, KNNGraph knng, int nb_reco) {
//		KNNGraph reco = new KNNGraph();
		KNNGraph reco = new KNNGraph(nb_reco,dataset.getUsers().size(),dataset.getInitValue());
		for(int user:knng.get_users()) {
//		for(int user:dataset.getUsers()) {
			reco.init(user, nb_reco, 0);
			HashSet<Integer> candidates_items = new HashSet<Integer>();
			for(int neighbor: knng.get_neighbors_ids(user)) {
				candidates_items.addAll(dataset.getRatedItems(neighbor));
			}
			for(int item: candidates_items) {
				double score = 0;
				double sum_sim = 0;
				for(IntDoublePair idp: knng.get_neighbors(user)) {
					score = score + (idp.score * dataset.getRating(idp.integer, item));
					sum_sim = sum_sim + idp.score;
				}
				if (sum_sim != 0) {
					score = score / sum_sim;
				}
				reco.put(user, item, score);
			}
		}
		return reco;
	}


	public static KNNGraph launchSeq(Dataset dataset, KNNGraph knng, int nb_reco, String add) throws IOException {

		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		KNNGraph reco = doRecommandationSeq(dataset,knng,nb_reco);

		if(ParametersStats.measure_time) {
			long duration = System.currentTimeMillis()-start;
			io.IO.toFile("{\"time\": "+duration+",\"nb_rec\": "+nb_reco+"}", add, util.Names.recoTimeFile);
		}
//		io.IO.RectoJSON(reco, dataset, add, util.Names.recoFile);
		io.IO.RectoJSON(reco, add, util.Names.recoFile);
		return reco;
	}
	
}
