package util;

public final class IntIntPair {
	public int first_element;
	public int second_element;
	public IntIntPair(int _fst, int _scd) {
		first_element = _fst;
		second_element = _scd;
	}

	@Override
	public String toString() {
		return ("("+first_element+","+second_element+")");
	}
	
	@Override
	public IntIntPair clone() {
		return (new IntIntPair(first_element, second_element));
	}
}
