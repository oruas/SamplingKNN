package util;

import java.util.Set;

public final class Bucket {

	private final Set<Integer> users;
	private final int size;

//	private final int seed;
//	private final int bucket_id;

	public Bucket(Set<Integer> users) {
		this.users=users;
		this.size=users.size();
	}
//	public Bucket(int seed, int bucket_id, Set<Integer> users) {
//		this.seed=seed;
//		this.bucket_id = bucket_id;
//		this.users=users;
//		this.size=users.size();
//	}
	
//	public int get_size() {
//		return size;
//	}
	public Set<Integer> get_users() {
		return users;
	}

	public boolean equals(Bucket b) {
		return (b.size==size && b.users==users);
	}

	public int compare(Bucket b) {
		return (b.size-size);
	}




}
