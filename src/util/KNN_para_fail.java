package util;

import java.util.HashMap;
import java.util.HashSet;

/** PARALLEL VERSION
 * Heap in which the IntDoublePair are stored in an increasing order.
 * Int is actually the neighbor or the item, and the score is the similarity or the rating.
 * The order is the lexicographic order.
 * The size is fixed and there is no suppression.
 */
public final class KNN_para_fail implements KNN_i{

	private final int nb_proc = Runtime.getRuntime().availableProcessors()+1;
	@SuppressWarnings("unchecked")
	private final HashMap<Integer,Double>[] heaps = (HashMap<Integer, Double>[]) (new HashMap[nb_proc]);
	private final double[] mins = new double[nb_proc];
	private final int[] sizes = new int[nb_proc];
	@SuppressWarnings("unchecked")
	private final HashSet<Integer>[] already_added = (HashSet<Integer>[]) (HashSet<Integer>[]) new HashSet[nb_proc]; 
	
	private final int size;

	public KNN_para_fail(int _size) {
		size = _size;
		for(int i=0; i < nb_proc; i++) {
			heaps[i] = new HashMap<Integer,Double>();
			mins[i]=2.;
			sizes[i]=0;
			already_added[i] = new HashSet<Integer>();
		}
	}

	public KNN_para_fail(int _size, double initValue) {
		this(_size);
	}

	public KNN_para_fail(IntDoublePair[] elements) {
		size = elements.length;
		for(int i = 0; i < nb_proc; i++) {
			heaps[i] = new HashMap<Integer,Double>();
			mins[i]=0.;
			sizes[i]=0;
			already_added[i] = new HashSet<Integer>();
		}
		int index=0;
		for(IntDoublePair idp: elements) {

			heaps[index].put(idp.integer, idp.score);
			mins[index]=Math.min(mins[index], idp.score);
			already_added[index].add(idp.integer);
			sizes[index]++;
			
			index = (index+1)%nb_proc;
		}
	}
	
	public KNN_para_fail(int size, HashMap<Integer,Double>[] heaps, double[] mins, int[] sizes,HashSet<Integer>[] already_added) {
		this.size = size;
		for(int i = 0; i < nb_proc; i++) {
			this.heaps[i] = heaps[i];
			this.mins[i] = mins[i];
			this.sizes[i] = sizes[i];
			this.already_added[i] = already_added[i];
		}
	}


	public IntDoublePair[] toArray() {
		KNN knn = new KNN(size);
		for(int i=0; i<nb_proc;i++) {
			for (int user: heaps[i].keySet()) {
				knn.add(new IntDoublePair(user,heaps[i].get(user)));
			}
		}
		return knn.toArray();
	}

	/**
	 * Return the integers of the heap in an array, sorted by ratings, from the highest to the lowest.
	 * @return
	 */
	public int[] itemArray() {
		IntDoublePair[] aux = toSortedArray();
		int[] array = new int[size];
		for(int index = 0; index < size; index++) {
			//			array[size - index - 1] = heap[index].integer;
			array[index] = aux[index].integer;
		}
		return array;
	}


	/**
	 * Adding an int is the same as suppressing the lowest one (which is the root) and replace it by the new one.
	 * @param irp
	 */
	public boolean add(IntDoublePair irp) {
		int threadID = (int) (Thread.currentThread().getId()%nb_proc);
		if ((!already_added[threadID].contains(irp.integer)) && (sizes[threadID] < size || mins[threadID] < irp.score)) {
			heaps[threadID].put(irp.integer, irp.score);
			mins[threadID] = Math.min(mins[threadID], irp.score);
			sizes[threadID]++;
			already_added[threadID].add(irp.integer);
			return true;
		}
		else return false;
	}



	public IntDoublePair[] toSortedArray() {
		IntDoublePair[] sortedArray = new IntDoublePair[size];
		IntDoublePair[] heap_aux = clone().toArray();

		for(int item = 0; item < size; item++) {
			sortedArray[size-1-item] = heap_aux[0];
			heap_aux[0] = heap_aux[size-1 - item];
			IntDoublePair irp = heap_aux[0];
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size-1 - item) {
					break;
				}
				if (child + 1 <= size-1 - item) {
					if (is_lower(heap_aux[child+1], heap_aux[child])) {
						child = child + 1;
					}
				}
				if(is_lower(irp,heap_aux[child])) {
					break;
				}
				else {
					heap_aux[index] = heap_aux[child];
					index = child;
				}
			}
			heap_aux[index] = irp;
		}


		return sortedArray;
	}




	public boolean is_lower(IntDoublePair irp1, IntDoublePair irp2) {
		return (irp1.integer == -1 || (irp1.score == 0 && irp2.integer != -1) || irp1.score < irp2.score);
	}

	public double min_score() {
		double min = 1;
		for (int i=0; i<nb_proc; i++) {
			min = Math.min(min, mins[i]);
		}
		return min;
	}

	public boolean equals(KNN_para_fail hp) {
		return this.itemArray().equals(hp.itemArray());
	}

	@Override
	public String toString() {
		String s = "[";
		boolean b = true;
		for(IntDoublePair idp: this.toArray()) {
			if (b) {
				b = false;
				s = s + "("+idp.integer+","+idp.score+")";
			}
			else {
				s = s + " ("+idp.integer+","+idp.score+")";
			}
			
		}
		s = s + "]";
		return s;
	}

	@Override
	public KNN_para_fail clone() {
		return (new KNN_para_fail(size,heaps,mins,sizes,already_added));
	}

	@Override
	public boolean equals(KNN_i hp) {
		return equals((KNN_para_fail) hp);
	}



}
