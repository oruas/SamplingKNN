package util;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Heap in which Set<Integer> are stored in an increasing order.
 * The set is a bucket such as the ones used in LSH, and the score the size of the set.
 * The indexes of the set in the matrix (if relevant) are also stored.
 * The size is fixed and there is no suppression.
 */
public final class HeapSetInteger {

	private final int size;
	private final IntIntSetInt[] heap;
	private final HashSet<Entry<Integer,Integer>> hashset;

	public HeapSetInteger(int _size) {
		size = _size;
		heap = new IntIntSetInt[size];
		hashset = new HashSet<Entry<Integer,Integer>>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntIntSetInt(-1,-1,null,0);
		}
	}

	public HeapSetInteger(int _size, double initValue) {
		size = _size;
		heap = new IntIntSetInt[size];
		hashset = new HashSet<Entry<Integer,Integer>>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntIntSetInt(-1,-1,null,0);
		}
	}

	public HeapSetInteger(IntIntSetInt[] elements) {
		size = elements.length;
		heap = elements;
		hashset = new HashSet<Entry<Integer,Integer>>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			hashset.add(new AbstractMap.SimpleEntry<Integer,Integer>(heap[index].index_i,heap[index].index_j));
		}
	}


	public IntIntSetInt[] toArray() {
		return heap;
	}

	/**
	 * Return the integers of the heap in an array, sorted by ratings, from the highest to the lowest.
	 * @return
	 */
	public Set<Integer>[] setArray() {
		IntIntSetInt[] aux = toSortedArray();
		@SuppressWarnings("unchecked")
		Set<Integer>[] array = (Set<Integer>[]) new Set[size];
		for(int index = 0; index < size; index++) {
			array[index] = aux[index].set;
		}
		return array;
	}


	/**
	 * Adding an int is the same as suppressing the lowest one (which is the root) and replace it by the new one.
	 * @param irp
	 */
	public /*synchronized*/ boolean add(IntIntSetInt irp) {
		if(can_be_added(irp)){// && !contains(irp)) {
//		if(is_lower(heap[0], irp) && !contains(irp)) {
			
			double min_score = heap[0].score;
			hashset.remove(new AbstractMap.SimpleEntry<Integer,Integer>(heap[0].index_i,heap[0].index_j));
			hashset.add(new AbstractMap.SimpleEntry<Integer,Integer>(irp.index_i,irp.index_j));
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size - 1) {
					break;
				}
				if (child + 1 <= size - 1) {
					if (!is_lower(heap[child], heap[child+1])) {
						child = child + 1;
					}
				}
				if(is_lower(heap[child],irp)) {
					heap[index] = heap[child];
					index = child;
				}
				else {
					break;
				}
			}
			heap[index] = irp;
			return (irp.score != min_score);
//			return true;
		}
		else return false;
	}





	public IntIntSetInt[] toSortedArray() {
		IntIntSetInt[] sortedArray = new IntIntSetInt[size];
		IntIntSetInt[] heap_aux = clone().toArray();

		for(int item = 0; item < size; item++) {
			sortedArray[size-1-item] = heap_aux[0];
			heap_aux[0] = heap_aux[size-1 - item];
			IntIntSetInt irp = heap_aux[0];
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size-1 - item) {
					break;
				}
				if (child + 1 <= size-1 - item) {
					if (is_lower(heap_aux[child+1], heap_aux[child])) {
						child = child + 1;
					}
				}
				if(is_lower(irp,heap_aux[child])) {
					break;
				}
				else {
					heap_aux[index] = heap_aux[child];
					index = child;
				}
			}
			heap_aux[index] = irp;
		}


		return sortedArray;
	}



	public double min_score() {
		return heap[0].score;
	}

	public boolean is_bigger(IntIntSetInt irp1, IntIntSetInt irp2) {
		return (irp2.index_i == -1 || irp2.index_j == -1|| irp1.score > irp2.score || (irp1.score == irp2.score && irp1.index_i > irp2.index_i)
				|| (irp1.score == irp2.score && irp1.index_i == irp2.index_i && irp1.index_j > irp2.index_j)
				);
	}

	public boolean can_be_added(IntIntSetInt irp1) {
		IntIntSetInt irp2 = heap[0];
		return (!hashset.contains(new AbstractMap.SimpleEntry<Integer, Integer>(irp1.index_i,irp1.index_j)) && (irp2.index_i == -1 || irp2.index_j == -1 || irp1.score > irp2.score));
	}

	public boolean is_lower(IntIntSetInt irp1, IntIntSetInt irp2) {
		return (irp1.index_i == -1 || irp1.index_j == -1 || (irp1.score == 0 && irp2.index_i != -1 && irp2.index_j != -1) || irp1.score < irp2.score);
	}

	public boolean equals(HeapSetInteger hp) {
		return this.setArray().equals(hp.setArray());
	}

	public String toString() {
		String s = "[";
		boolean b = true;
		for(IntIntSetInt idp: heap) {
			if (b) {
				b = false;
				s = s + "("+idp.index_i +","+idp.index_j+","+idp.score+")";
			}
			else {
				s = s + " ("+idp.index_i +","+idp.index_j+","+idp.score+")";
			}
			
		}
		s = s + "]";
		return s;
	}

	public HeapSetInteger clone() {
		if (heap != null) {
			IntIntSetInt[] new_heap = new IntIntSetInt[heap.length];
			for(int index = 0; index < heap.length; index++) {
				new_heap[index] = heap[index].clone();
			}
			return (new HeapSetInteger(new_heap));
		}
		else {
			return null;
		}
	}



}
