package util;

public final class IntDoubleBoolTriplet {
	public int integer;
	public double score;
	public boolean bool;
	public IntDoubleBoolTriplet(int _item, double _rating, boolean _bool) {
		integer = _item;
		score = _rating;
		bool = _bool;
	}
	@Override
	public IntDoubleBoolTriplet clone() {
		return (new IntDoubleBoolTriplet(integer, score,bool));
	}


	@Override
	public String toString() {
		return ("("+integer+","+score+","+bool+")");
	}
	
}
