package util;

import java.util.Set;

public class Permutations1024{

	private final int seed;
	private final int nb_index;
	
	public Permutations1024() {
		seed=0;
		nb_index=1024;
	}
	
	public Permutations1024(int seed) {
		this.seed=seed;
		nb_index=1024;
	}
	public Permutations1024(int seed, int nb_index) {
		this.seed=seed;
		this.nb_index = nb_index;
	}
	
	public int getHash(Set<Integer> set) {
		int hash;
		int min=nb_index;
		for(int item: set) {
			hash = util.HashFunctions.get_hash(item, seed+5, nb_index);
			if(hash<min) {
				min = hash;
			}
		}		
		return min;
	}
	
	public String toString() {
		return "Permutation on " + nb_index + " bits with seed equals to " + seed;
	}
	
	
}
