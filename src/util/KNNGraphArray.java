package util;

import java.util.HashSet;
import java.util.Set;

public final class KNNGraphArray implements KNNG{

//	private int[][] knng;
	private KNN[] knng;
	
//	private final int k;
	
	public KNNGraphArray(int k, int nb_users, double score_init) {
//		this.k = k;
		this.knng = new KNN[nb_users];
		for(int i=0; i < nb_users; i++) {
			for(int j=0; j<k; j++) {
				knng[i] = new KNN(k, score_init);
			}
		}
	}
	
	public KNNGraphArray(KNN[] array) {
//		this.k=k;
		this.knng = array;
	}
	
	
	public void init(int user, int k, double score_init) {
		knng[user] = new KNN(k, score_init);
	}
	
	public /*synchronized*/ boolean put(int user, int neighbor, double score) {
		return knng[user].add(new IntDoublePair(neighbor,score));
	}
	
	public /*synchronized*/ void put(int user, KNN knn) {
		knng[user] = knn;
	}
	
	public int[] get_neighbors_ids(int user) {
		return knng[user].itemArray();
	}
	
	
	public IntDoublePair[] get_neighbors(int user) {
		return knng[user].toArray();
	}
	public IntDoublePair[] get_sorted_neighbors(int user) {
		return knng[user].toSortedArray();
	}
	
	public boolean has_neighbors(int user) {
		return (user<knng.length);
	}
	
	public Set<Integer> get_users() {
		HashSet<Integer> users = new HashSet<Integer>(knng.length);
		for(int user=0; user<knng.length; user++) {
			users.add(user);
		}
		return users;
	}
	
	public KNN get_KNN(int user) {
		return knng[user];
	}
	
	
	@Override
	public KNNGraphArray clone() {
		return (new KNNGraphArray(knng.clone()));
	}

	@Override
	public void put(int user, KNN_i knn) {
		put(user, (KNN) knn);
	}

}
