package util;

import java.util.HashSet;
//import java.util.TreeSet;

/**
 * Heap in which the IntDoubleBoolTriplet/KNNFlags are stored in an increasing order.
 * Int is actually the neighbor or the item, and the score is the similarity or the rating.
 * The order is the lexicographic order.
 * The size is fixed and there is no suppression.
 */
public final class KNNFlags {


	private final int size;
	private final IntDoubleBoolTriplet[] heap;
//	private final TreeSet<Integer> tree;
	private final HashSet<Integer> hashset;

	public KNNFlags(int _size) {
		size = _size;
		heap = new IntDoubleBoolTriplet[size];
//		tree = new TreeSet<Integer>();
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntDoubleBoolTriplet(-1,0,false);
		}
	}

	public KNNFlags(int _size, double initValue) {
		size = _size;
		heap = new IntDoubleBoolTriplet[size];
//		tree = new TreeSet<Integer>();
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntDoubleBoolTriplet(-1,initValue,false);
		}
	}
	
	public KNNFlags(IntDoublePair[] heap_, boolean b) {
//		tree = new TreeSet<Integer>();
//		IntDoublePair[] heap_ = knn.toArray();
		size = heap_.length;
		heap = new IntDoubleBoolTriplet[size];
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			IntDoublePair idp = heap_[index];
			heap[index] = new IntDoubleBoolTriplet(idp.integer,idp.score,b);
			hashset.add(idp.integer);
		}
	}
	
	public KNNFlags(KNN knn, boolean b) {
//		tree = new TreeSet<Integer>();
		IntDoublePair[] heap_ = knn.toArray();
		size = heap_.length;
		heap = new IntDoubleBoolTriplet[size];
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			IntDoublePair idp = heap_[index];
			heap[index] = new IntDoubleBoolTriplet(idp.integer,idp.score,b);
			hashset.add(idp.integer);
		}
	}
	
	

	public KNNFlags(IntDoubleBoolTriplet[] elements) {
		size = elements.length;
		heap = elements;
//		tree = new TreeSet<Integer>();
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			hashset.add(heap[index].integer);
		}
	}
	public KNNFlags(IntDoubleBoolTriplet[] elements, HashSet<Integer> hashset) {
		size = elements.length;
		heap = elements;
		this.hashset = hashset;
	}

	
	public IntDoubleBoolTriplet[] toArray() {
		return heap;
	}
	
	/**
	 * Return the integers of the heap in an array, sorted by ratings, from the highest to the lowest.
	 * @return
	 */
	public int[] itemArray() {
		IntDoubleBoolTriplet[] aux = toSortedArray();
		int[] array = new int[size];
		for(int index = 0; index < size; index++) {
//			array[size - index - 1] = heap[index].integer;
			array[index] = aux[index].integer;
		}
		return array;
	}
	
	
	/**
	 * Adding an int is the same as suppressing the lowest one (which is the root) and replace it by the new one.
	 * @param irp
	 */
	
	public synchronized boolean add(IntDoubleBoolTriplet irp) {
		if(can_be_added(irp)) {// && !contains(irp)) {
//		if(is_lower(heap[0], irp) && !contains(irp)) {

			double min_score = heap[0].score;
			if(heap[0].integer != -1) {
				hashset.remove(heap[0].integer);
			}
			hashset.add(irp.integer);
			
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size - 1) {
					break;
				}
				if (child + 1 <= size - 1) {
//					if (!is_lower(heap[child], heap[child+1])) {
//						child = child + 1;
//					}
					if (is_lower(heap[child+1], heap[child])) {
						child = child + 1;
					}
				}
//				if(is_lower(heap[child],irp)) {
//					heap[index] = heap[child];
//					index = child;
//				}
//				else {
//					break;
//				}
				if(is_lower(irp,heap[child])) {
					break;
				}
				else {
					heap[index] = heap[child];
					index = child;
				}
			}
			heap[index] = irp;
//			return true;
			return (irp.score != min_score);
		}
		else return false;
	}
	
	
	
	

	public IntDoubleBoolTriplet[] toSortedArray() {
		IntDoubleBoolTriplet[] sortedArray = new IntDoubleBoolTriplet[size];
		IntDoubleBoolTriplet[] heap_aux = clone().toArray();
		
		for(int item = 0; item < size; item++) {
			sortedArray[size-1-item] = heap_aux[0];
			heap_aux[0] = heap_aux[size-1 - item];
			IntDoubleBoolTriplet irp = heap_aux[0];
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size-1 - item) {
					break;
				}
				if (child + 1 <= size-1 - item) {
					if (is_lower(heap_aux[child+1], heap_aux[child])) {
						child = child + 1;
					}
				}
				if(is_lower(irp,heap_aux[child])) {
					break;
				}
				else {
					heap_aux[index] = heap_aux[child];
					index = child;
				}
			}
			heap_aux[index] = irp;
		}
		
		
		return sortedArray;
	}
	
	
	
	
	
	
	public boolean contains(IntDoubleBoolTriplet irp) {
		boolean b = false;
		for (int index = 0; index < size; index++) {
			b = b || (heap[index].integer == irp.integer);
			if(b) {
				break;
			}
		}
		return b;
	}

	public double min_score() {
		return heap[0].score;
	}

	public boolean is_bigger(IntDoubleBoolTriplet irp1, IntDoubleBoolTriplet irp2) {
		return (irp2.integer == -1 || irp1.score > irp2.score || (irp1.score == irp2.score && irp1.integer > irp2.score));
	}

//	public boolean is_lower(IntDoubleBoolTriplet irp1, IntDoubleBoolTriplet irp2) {
//		return (irp1.integer == -1 || irp1.score < irp2.score || (irp1.score == irp2.score && irp1.integer < irp2.score));
//	}

	public boolean can_be_added(IntDoubleBoolTriplet irp1) {
		IntDoubleBoolTriplet irp2 = heap[0];
//		return (irp2.integer == -1 || (irp2.score == 0 && irp1.integer != -1) || irp1.score > irp2.score);
		return (!hashset.contains(irp1.integer) && (irp2.integer == -1 || irp1.score > irp2.score));
	}

	public boolean is_lower(IntDoubleBoolTriplet irp1, IntDoubleBoolTriplet irp2) {
		return (irp1.integer == -1 || (irp1.score == 0 && irp2.integer != -1) || irp1.score < irp2.score);
//		return (irp1.integer == -1 || irp1.score < irp2.score || (irp1.score == irp2.score && irp1.integer < irp2.integer));
//		return (irp1.integer == -1 || irp1.score < irp2.score || (irp1.score == irp2.score && irp1.integer < irp2.score)); //FRec version
	}

	
	public boolean equals(KNNFlags hp) {
		return this.itemArray().equals(hp.itemArray());
	}
	

	
	public KNN toKNN() {
		IntDoublePair[] heap_idp = new IntDoublePair[size];
		for(int index = 0; index<size; index++) {
			heap_idp[index] = new IntDoublePair(heap[index].integer, heap[index].score);
		}
		return (new KNN(heap_idp));
	}
	
//	public IntDoublePairHeap ToIntDoublePairHeap() {
//		IntDoublePairHeap new_heap = new IntDoublePairHeap(size);
//		for(IntDoubleBoolTriplet idbt: heap) {
//			new_heap.add(new IntDoublePair(idbt.integer, idbt.score));
//		}
//		return new_heap;
//	}
	

	
	@Override
	public KNNFlags clone() {
		if (heap != null) {
		IntDoubleBoolTriplet[] new_heap = new IntDoubleBoolTriplet[heap.length];
		for(int index = 0; index < heap.length; index++) {
			new_heap[index] = heap[index].clone();
		}
		return (new KNNFlags(new_heap,hashset));
		}
		else {
			return null;
		}
	}


	@Override
	public String toString() {
		String s = "[";
		boolean b = true;
		for(IntDoubleBoolTriplet idp: heap) {
			if (b) {
				b = false;
				s = s + "("+idp.integer+","+idp.score+","+idp.bool+")";
			}
			else {
				s = s + " ("+idp.integer+","+idp.score+","+idp.bool+")";
			}
			
		}
		s = s + "]";
		return s;
	}
	
}
