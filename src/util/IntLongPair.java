package util;

public final class IntLongPair {
	public int integer;
	public long score;
	public IntLongPair(int _integer, long _score) {
		integer = _integer;
		score = _score;
	}

	@Override
	public String toString() {
		return ("("+integer+","+score+")");
	}
	
	@Override
	public IntLongPair clone() {
		return (new IntLongPair(integer, score));
	}
}
