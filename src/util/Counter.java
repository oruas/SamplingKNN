package util;

public final class Counter {
	private int counter=0;
	
	public Counter() {
		counter = 0;
	}
	
	public void inc() {
		counter++;
	}
	
	public void inc(int count) {
		counter = counter + count;
	}
	
	public int getValue() {
		return counter;
	}
	
	public void init() {
		counter = 0;
	}
}
