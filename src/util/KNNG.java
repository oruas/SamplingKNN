package util;

import java.util.Set;

public interface KNNG {

	
	public void init(int user, int k, double score_init);
	
	public boolean put(int user, int neighbor, double score);
	
//	public boolean put(int user, IntDoublePair idp);
	
	public void put(int user, KNN_i knn);
	
	public int[] get_neighbors_ids(int user) ;
	
	public IntDoublePair[] get_neighbors(int user);
	
	public boolean has_neighbors(int user);
	
	public Set<Integer> get_users();
	
//	public KNN get_KNN(int user);
	
	public IntDoublePair[] get_sorted_neighbors(int user);
	
	
	
	
	
	public KNNG clone();
}
