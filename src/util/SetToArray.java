package util;

import java.util.Set;

public final class SetToArray {

	public static final int[] setToArray(Set<Integer> users) {
		int[] new_users = new int[users.size()];
		int index = 0;
		for(int user: users) {
			new_users[index] = user;
			index++;
		}
		return new_users;
	}
}
