package util;

import java.util.Comparator;

public class BucketComparator implements Comparator<Bucket>{

	@Override
	public int compare(Bucket b0, Bucket b1) {
		return b0.compare(b1);
	}
//	@Override
	public boolean equals(Bucket b0, Bucket b1) {
		return b0.equals(b1);
	}

}
