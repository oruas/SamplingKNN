package util;

public final class IntDoublePair {
	public int integer;
	public double score;
	public IntDoublePair(int _integer, double _score) {
		integer = _integer;
		score = _score;
	}

	@Override
	public String toString() {
		return ("("+integer+","+score+")");
	}
	
	@Override
	public IntDoublePair clone() {
		return (new IntDoublePair(integer, score));
	}
}
