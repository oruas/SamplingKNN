package util;

//import java.util.HashSet;

/**
 * Heap in which the IntDoublePair are stored in an increasing order.
 * Int is actually the index of the set, and the score is the size of the set.
 * The elements are not suppressed, just the order changed according to the scores.
 * The order is the lexicographic order.
 * The size is fixed and there is no suppression.
 */
public final class HeapIntInt {

	private final int size;
	private final IntLongPair[] heap;
//	private final TreeSet<Integer> tree;
//	private final HashSet<Integer> hashset;

	public HeapIntInt(int _size) {
		size = _size;
		heap = new IntLongPair[size];
//		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntLongPair(index,0);
		}
	}

	public HeapIntInt(int _size, long initValue) {
		size = _size;
		heap = new IntLongPair[size];
//		tree = new TreeSet<Integer>();
//		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntLongPair(-1,initValue);
		}
	}

	public HeapIntInt(IntLongPair[] elements) {
		size = elements.length;
		heap = elements;
//		tree = new TreeSet<Integer>();
//		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
//		for(int index = 0; index < size; index++) {
//			hashset.add(heap[index].integer);
//		}
	}


	public IntLongPair[] toArray() {
		return heap;
	}

	/**
	 * Return the integers of the heap in an array, sorted by ratings, from the highest to the lowest.
	 * @return
	 */
	public int[] itemArray() {
		IntLongPair[] aux = toSortedArray();
		int[] array = new int[size];
		for(int index = 0; index < size; index++) {
			//			array[size - index - 1] = heap[index].integer;
			array[index] = aux[index].integer;
		}
		return array;
	}

	public int get_id() {
		return heap[0].integer;
	}
	
	
	public boolean add(long score) {
		return add(new IntLongPair(heap[0].integer,heap[0].score+score));
	}
	
	
	/**
	 * Adding an int is the same as suppressing the lowest one (which is the root) and replace it by the new one.
	 * @param irp
	 */
	public /*synchronized*/ boolean add(IntLongPair irp) {
		if(can_be_added(irp)){// && !contains(irp)) {
			double min_score = heap[0].score;
//			hashset.remove(heap[0].integer);
//			hashset.add(irp.integer);
			
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size - 1) {
					break;
				}
				if (child + 1 <= size - 1) {
					if (!is_lower(heap[child], heap[child+1])) {
						child = child + 1;
					}
				}
				if(is_lower(heap[child],irp)) {
					heap[index] = heap[child];
					index = child;
				}
				else {
					break;
				}
			}
			heap[index] = irp;
			return (irp.score != min_score);
		}
		else return false;
	}







	public IntLongPair[] toSortedArray() {
		IntLongPair[] sortedArray = new IntLongPair[size];
		IntLongPair[] heap_aux = clone().toArray();

		for(int item = 0; item < size; item++) {
			sortedArray[size-1-item] = heap_aux[0];
			heap_aux[0] = heap_aux[size-1 - item];
			IntLongPair irp = heap_aux[0];
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size-1 - item) {
					break;
				}
				if (child + 1 <= size-1 - item) {
					if (is_lower(heap_aux[child+1], heap_aux[child])) {
						child = child + 1;
					}
				}
				if(is_lower(irp,heap_aux[child])) {
					break;
				}
				else {
					heap_aux[index] = heap_aux[child];
					index = child;
				}
			}
			heap_aux[index] = irp;
		}


		return sortedArray;
	}






	public double min_score() {
		return heap[0].score;
	}

	public boolean is_bigger(IntLongPair irp1, IntLongPair irp2) {
		return (irp2.integer == -1 || irp1.score > irp2.score || (irp1.score == irp2.score && irp1.integer > irp2.integer));
	}

	public boolean can_be_added(IntLongPair irp1) {
		IntLongPair irp2 = heap[0];
		return (irp2.integer == -1 || irp1.score > irp2.score);
//		return (!hashset.contains(irp1.integer) && (irp2.integer == -1 || irp1.score > irp2.score));
	}

	public boolean is_lower(IntLongPair irp1, IntLongPair irp2) {
		return (irp1.integer == -1 || (irp1.score == 0 && irp2.integer != -1) || irp1.score < irp2.score);
	}

	public boolean equals(HeapIntInt hp) {
		return this.itemArray().equals(hp.itemArray());
	}

	public String toString() {
		String s = "[";
		boolean b = true;
		for(IntLongPair idp: heap) {
			if (b) {
				b = false;
				s = s + "("+idp.integer+","+idp.score+")";
			}
			else {
				s = s + " ("+idp.integer+","+idp.score+")";
			}
			
		}
		s = s + "]";
		return s;
	}

	public HeapIntInt clone() {
		if (heap != null) {
			IntLongPair[] new_heap = new IntLongPair[heap.length];
			for(int index = 0; index < heap.length; index++) {
				new_heap[index] = heap[index].clone();
			}
			return (new HeapIntInt(new_heap));
		}
		else {
			return null;
		}
	}




}
