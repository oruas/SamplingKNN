package util;

import java.util.Set;

public final class Bucket_rec {

	private final Set<Integer> users;
	private final int size;

	private final int seed;
	private final int bucket_id;

	public Bucket_rec(int seed, int bucket_id, Set<Integer> users) {
		this.seed=seed;
		this.bucket_id = bucket_id;
		this.users=users;
		this.size=users.size();
	}
	
//	public int get_size() {
//		return size;
//	}
	public Set<Integer> get_users() {
		return users;
	}

	public boolean equals(Bucket_rec b) {
		return (b.size==size && b.users==users);
	}

	public int compare(Bucket_rec b) {
		return (b.size-size);
	}

	public int seed() {
		return seed;
	}
	public int bucket_id() {
		return bucket_id;
	}


}
