package util;

public final class KNN_approx {
	public final int size;
	public final int[] heap1;
	public final double[] sim1;
	public int size1=0;
	public double min1=1.0;
	public double max1=0.;
	public final int[] heap2;
	public final double[] sim2;
	public int size2=0;
	public double min2;
	public double max2;
	public boolean b=true;
	
	
	public KNN_approx(int size) {
		this.size = size;
		this.heap1 = new int[2*size];
		this.sim1 = new double[2*size];
		this.heap2 = new int[2*size];
		this.sim2 = new double[2*size];
		for(int i=0; i<2*size; i++) {
			heap1[i]=-1;
			sim1[i]=0.0; 
			heap2[i]=-1;
			sim2[i]=0.0;
		}
	}
	
	public boolean add(IntDoublePair irp) {
		if(b) { //Necessarily, size2<2*size-1
			if (size1<size) {
				heap1[size1] = irp.integer;
				sim1[size1] = irp.score;
				size1++;
				max1 = Math.max(max1,irp.score);
				min1 = Math.min(min1, irp.score);
				return true;
			}
			else {
				if (max1 < irp.score) {
					heap2[size2] = irp.integer;
					sim2[size2] = irp.score;
					size2++;
					max2 = Math.max(max2,irp.score);
					min2 = Math.min(min2, irp.score);
					if(size2 == 2*size) {
						b = false;
						int max_id=0;
						double max_sim=sim2[0];
						for(int i = 0; i < size; i++) {
							for(int j = 0; j < 2*size; j++) {
								if(max_sim<sim2[j]) {
									max_id = j;
									max_sim = sim2[j];
								}
							}
							heap1[i] = heap2[max_id];
							sim1[i] = max_sim;
							heap2[max_id] = -1;
							sim2[max_id] = 0.0; 
						}
						for(int i = size ; i < 2*size; i++) {
							heap1[i] = -1;
							sim1[i] = 0.;
						}
						for(int i = 0 ; i < 2*size; i++) {
							heap2[i] = -1;
							sim2[i] = 0.;
						}
						max1=max2;
						min1=sim1[size-1];
						size1=size;
						max2=0.;
						min2=1.0;
						size2=0;
						
					}
					return true;
				}
				else {
					if (min1<irp.score) {
						if(size1 < 2*size) {
							heap1[size1] = irp.integer;
							sim1[size1] = irp.score;
							size1++;
							return true;
						}
					}
					else {
						return false;
					}
				}
			}
		}
		else { //Necessarily, size1<2*size-1
			if (size2<size) {
				heap2[size2] = irp.integer;
				sim2[size2] = irp.score;
				size2++;
				max2 = Math.max(max2,irp.score);
				min2 = Math.min(min2, irp.score);
				return true;
			}
			else {
				if (max2 < irp.score) {
					heap1[size1] = irp.integer;
					sim1[size1] = irp.score;
					size1++;
					max1 = Math.max(max1,irp.score);
					min1 = Math.min(min1, irp.score);
					if(size1 == 2*size) {
						b = true;
						int max_id=0;
						double max_sim=sim1[0];
						for(int i = 0; i < size; i++) {
							for(int j = 0; j < 2*size; j++) {
								if(max_sim<sim1[j]) {
									max_id = j;
									max_sim = sim1[j];
								}
							}
							heap2[i] = heap1[max_id];
							sim2[i] = max_sim;
							heap1[max_id] = -1;
							sim1[max_id] = 0.0; 
						}
						for(int i = size ; i < 2*size; i++) {
							heap2[i] = -1;
							sim2[i] = 0.;
						}
						for(int i = 0 ; i < 2*size; i++) {
							heap1[i] = -1;
							sim1[i] = 0.;
						}
						max2=max1;
						min2=sim2[size-1];
						size2=size;
						max1=0.;
						min1=1.0;
						size1=0;
						
					}
					return true;
				}
				else {
					if (min2<irp.score) {
						if(size2 < 2*size) {
							heap2[size1] = irp.integer;
							sim2[size1] = irp.score;
							size2++;
							return true;
						}
					}
					else {
						return false;
					}
				}
			}
		}
		return false;
	}
	
	public double min_score() {
		if(b) {
			return min1;
		}
		else {
			return min2;
		}
	}
	
	public IntDoublePair[] toArray() {
		KNN knn = new KNN(size);
		if (b) {
			for(int i = 0 ; i < size2; i++) {
				knn.add(new IntDoublePair(heap2[i],sim2[i]));
			}
			if(size2 < size) {
				for(int i = 0 ; i < size1; i++) {
					knn.add(new IntDoublePair(heap1[i],sim1[i]));
				}
			}
		}
		else {
			for(int i = 0 ; i < size1; i++) {
				knn.add(new IntDoublePair(heap1[i],sim1[i]));
			}
			if(size1 < size) {
				for(int i = 0 ; i < size2; i++) {
					knn.add(new IntDoublePair(heap2[i],sim2[i]));
				}
			}
		}
		return knn.toArray();
	}

}
