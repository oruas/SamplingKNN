package util;

public final class BitVector {

	private final int size_bitset;
	private final long[] bitset;
	
	public BitVector(int size_bitset) {
		this.size_bitset = size_bitset;
		this.bitset = new long[(int) Math.ceil(((double) size_bitset)/((double)64))];

	}
	
	public void put(int item) {
		int hash = util.HashFunctions.hash(item, size_bitset);
//		int hash = item;
		int index = hash / 64;
		bitset[index] = bitset[index] | (((long) 1) << (hash % 64));
	}
	
	public int get_size() {
		return size_bitset;
	}
	
	public int get_cardinality() {
		return cardinality(bitset);
	}
	
	/** return a copy of the bitset **/
	public long[] get_bitset() {
		return (long[]) (bitset.clone());
	}
	
	public int hamming(long[] bits) {
		return getIntersectionSize(bitset,bits);
	}
	
	
	
	
	
	
	
	
	
	
	
	public static double jaccard(long[] bitset1, long[] bitset2){
		int intersize = getIntersectionSize(bitset1,bitset2);
		return (intersize / (cardinality(bitset1) + cardinality(bitset2) - intersize));
	}
	
	public static double jaccard(long[] bitset1, long[] bitset2, int size1, int size2){
		int intersize = getIntersectionSize(bitset1,bitset2);
		return (intersize / (size1 + size2 - intersize));
	}
	
	
	
	
	public static int cardinality(long[] bitset) {
		int cardinality = 0;
		for(int i=0; i < bitset.length ; i++) {
			cardinality = cardinality + Long.bitCount(bitset[i]);
		}
		return cardinality;
	}
	
	public static int getIntersectionSize(long[] bits1, long[] bits2) {
	    int nBits = 0;
	    for (int i=0; i<bits1.length; i++)
	        nBits += Long.bitCount(bits1[i] & bits2[i]);
	    return nBits;
	}

}
