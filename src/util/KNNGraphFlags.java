package util;

import java.util.Set;

public final class KNNGraphFlags implements KNNGFlags{

	private KNNGFlags knng;
	
	public KNNGraphFlags() {
		this.knng = new KNNGraphHashMapKNNFlags();
	}
	
	public KNNGraphFlags(KNNGraph knng_, boolean b) {
		this.knng = new KNNGraphHashMapKNNFlags(knng_,b);
	}
	
	public KNNGraphFlags(KNNGFlags knng_) {
		this.knng = knng_;
	}
	

	
	public void init(int user, int k, double score_init) {
		this.knng.init(user,k,score_init);
	}
	
	public boolean put(int user, int neighbor, double score, boolean bool) {
		return this.knng.put(user, neighbor,score,bool);
	}
	
	public boolean put_AS(int user, int neighbor, double score, boolean bool) {
		return this.knng.put_AS(user, neighbor,score,bool);
	}
	
//	public boolean put(int user, IntDoubleBoolTriplet idp) {
//		return this.knng.put(user, idp);
//	}
	
//	public void put(int user, KNNFlags knn) {
//		this.knng.put(user, knn);
//	}
	
	public int[] get_neighbors_ids(int user) {
		return this.knng.get_neighbors_ids(user);
	}
	
	
	public IntDoubleBoolTriplet[] get_neighbors(int user) {
		return this.knng.get_neighbors(user);
	}
	
	public boolean has_neighbors(int user) {
		return this.knng.has_neighbors(user);
	}
	
	public Set<Integer> get_users() {
		return this.knng.get_users();
	}
	
//	public KNNFlags get_KNN(int user) {
//		return this.knng.get_KNN(user);
//	}
	
	
	public KNNGraph toKNNGraph() {
		return this.knng.toKNNGraph();
	}
	

	public void changeBoolToFalse(int user, IntDoubleBoolTriplet idbt) {
		this.knng.changeBoolToFalse(user, idbt);
	}
	
	
	
	@Override
	public KNNGraphFlags clone() {
		return (new KNNGraphFlags(this.knng.clone()));
	}
}
