package util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Semaphore;

public final class KNNGraphArray_sem implements KNNG{

	private int[][] knng;
//	private static int corr = 1000000;
//	private static int corr = (int) 1 << 30;
	private static double corr = (double) (~((int) 1 << 31));
	private Semaphore[] sem;
	private final int k;
//	private HashMap<Integer,Integer> ids_index = new HashMap<Integer,Integer>();
//	private HashMap<Integer,Integer> index_ids = new HashMap<Integer,Integer>();
//	private HashSet<Integer>[] knn_sets;
	private HashMap<Integer,HashSet<Integer>> knn_sets;
	
	public KNNGraphArray_sem(int k, int nb_users, double score_init) {
//		int l = 2*k;
		this.k = k;
//		this.knng = new int[nb_users][l];
		this.knng = new int[nb_users][2*k];
//		this.knng = new long[nb_users][(2*k)](score_init);
//		this.knng = new ();
		this.sem = new Semaphore[nb_users];
//		this.knn_sets = new HashSet<Integer>[nb_users];
		this.knn_sets = new HashMap<Integer,HashSet<Integer>>();
		for(int i=0; i < nb_users; i++) {
			for(int j=0; j<k; j++) {
				knng[i][2*j]= -1;
				knng[i][2*j+1]= (int) (score_init*corr);
			}
			sem[i] = new Semaphore(1);
//			knn_sets[i] = new HashSet<Integer>();
			knn_sets.put(i, new HashSet<Integer>());
		}
	}
	
	public KNNGraphArray_sem(int[][] knng) {
		this.knng = knng;
		this.k = knng[0].length/2;
		this.sem = new Semaphore[knng.length];
		this.knn_sets = new HashMap<Integer,HashSet<Integer>>();
		for(int i=0; i < knng.length; i++) {
			sem[i] = new Semaphore(1);
//			knn_sets[i] = new HashSet<Integer>();
			knn_sets.put(i, new HashSet<Integer>());
			for(int j=0; j<k; j++) {
//				knn_sets[i].add(knng[i][2*j]);
				knn_sets.get(i).add(knng[i][2*j]);
			}
		}
	}
	
	
	public void init(int user, int k, double score_init) {
		int l = 2*k;
		for(int j=0; j<l; j++) {
			knng[user][j]=(int) (score_init*corr);
		}
		sem[user] = new Semaphore(1);
		knn_sets.put(user, new HashSet<Integer>());
//		knn_sets[user] = new HashSet<Integer>();
	}
	
	public boolean put(int user, int neighbor, double score) {
//		knng[user][neighbor]= (long) (score*corr);
		boolean b = false;
		try {
			
			sem[user].acquire();
			b = add(user, neighbor, score);
			sem[user].release();
			
		}catch(InterruptedException e) {
	        e.printStackTrace();
	    }
		return b;
		
	}
	
	public void put(int user, KNN knn) {
		try {
			
			sem[user].acquire();
			int i = 0;
			for(IntDoublePair idp: knn.toArray()) {
//				this.knn_sets[user].remove(knng[user][2*i]);
				this.knn_sets.get(user).remove(knng[user][2*i]);
				this.knng[user][2*i] = idp.integer;
				this.knng[user][2*i+1] = (int) (idp.score*corr);
//				this.knn_sets[user].add(knng[user][2*i]);
				this.knn_sets.get(user).add(knng[user][2*i]);
				i++;
			}
			sem[user].release();
			
		}catch(InterruptedException e) {
	        e.printStackTrace();
	    }
	}
	
	public int[] get_neighbors_ids(int user) {
//		int[] neighbors = new int[knng[user].length/2];
		int[] neighbors = new int[k];
		for(int i=0; i<neighbors.length; i++) {
			neighbors[i] = (int) knng[user][2*i];
		}
		return neighbors;
	}
	
	
	public IntDoublePair[] get_neighbors(int user) {
//		IntDoublePair[] idps = new IntDoublePair[knng[user].length/2];
		IntDoublePair[] idps = new IntDoublePair[k];
		for(int i=0; i<k; i++) {
			idps[i] = new IntDoublePair((int) knng[user][2*i],knng[user][2*i+1]/corr);
		}
		return idps;
	}
	public IntDoublePair[] get_sorted_neighbors(int user) {
//		IntDoublePair[] idps = new IntDoublePair[knng[user].length/2];
//		IntDoublePair[] idps = new IntDoublePair[k];
//		for(int i=0; i<k; i++) {
//			idps[i] = new IntDoublePair((int) knng[user][2*i],knng[user][2*i+1]/corr);
//		}
//		return idps;
		return (toSortedArray(user));
	}
	
	public boolean has_neighbors(int user) {
		return (user<knng.length);
	}
	
	public Set<Integer> get_users() {
		HashSet<Integer> users = new HashSet<Integer>(knng.length);
		for(int user=0; user<knng.length; user++) {
			users.add(user);
		}
		return users;
	}
	
	public KNN get_KNN(int user) {
		return new KNN(get_neighbors(user));
	}

	//TODO double score => int score !
	private boolean can_be_added(int user, int neighbor, int score) {
//		boolean contains=false;
		return (!contains(user,neighbor) && (knng[user][0] == -1 || score > knng[user][0]));
	}
	private boolean add(int user, int neighbor, double score) {
		int score_int = ((int) (score*((double) corr)));
//		System.out.println(score);
//		System.out.println(corr);
//		System.out.println(score_int);
		if(can_be_added(user,neighbor,score_int)) {
//			knn_sets[user].remove(knng[user][0]);
//			knn_sets[user].add(neighbor);
			knn_sets.get(user).remove(knng[user][0]);
			knn_sets.get(user).add(neighbor);
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > k - 1) {
					break;
				}
				if (child + 1 <= k - 1) {
					if (!(knng[user][2*child] == -1 || (knng[user][2*child+1] == 0 && knng[user][2*(child+1)] != -1) || knng[user][2*child+1] < knng[user][2*(child+1)+1])) {
						child = child + 1;
					}
				}
				if(knng[user][2*child] == -1 || (knng[user][2*child+1] == 0 && neighbor != -1) || knng[user][2*child+1] < score_int) {
					knng[user][2*index] = knng[user][2*child];
					knng[user][2*index+1] = knng[user][2*child+1];
					index = child;
				}
				else {
					break;
				}
			}
			knng[user][2*index] = neighbor;
			knng[user][2*index+1] = score_int;
			return (score != 0);
		}
		else return false;
	}
	
	private boolean contains(int user, int neighbor) {
//		for (int i = 0; i < knng[user].length/2; i++) {
//			if (knng[user][2*i] == neighbor) {
//				return true;
//			}
//		}
//		return false;
		return knn_sets.get(user).contains(neighbor);
//		return knn_sets[user].contains(neighbor);
	}
	
	

	private IntDoublePair[] toSortedArray(int user) {
		IntDoublePair[] sortedArray = new IntDoublePair[k];
		int[] knn = new int[2*k];
		for (int i=0; i<2*k;i++) {
			knn[i] = knng[user][i];
		}

		for(int item = 0; item < k; item++) {
			sortedArray[k-1-item] = new IntDoublePair((int) knn[0], knn[1]/corr);
			knn[0] = knn[2*(k-1 - item)];
			knn[1] = knn[2*(k-1 - item)+1];
//			IntDoublePair irp = new IntDoublePair((int) knn[0], knn[1]/corr);
			int local_id = knn[0];
			int local_score = knn[1];
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > k-1 - item) {
					break;
				}
				if (child + 1 <= k-1 - item) {
					if (knn[2*(child+1)] == -1 || (knn[2*(child+1)] == 0 && knn[2*(child)] != -1) || knn[2*(child+1)+1] < knn[2*(child)+1]) {
//					if (is_lower(heap_aux[child+1], heap_aux[child])) {
						child = child + 1;
					}
				}
				if(local_id == -1 || (local_id == 0 && knn[2*(child)] != -1) || local_score < knn[2*(child)+1]) {
//				if(irp.integer == -1 || (irp.integer == 0 && knn[2*(child)] != -1) || irp.score*corr < knn[2*(child)+1]) {
//				if(is_lower(irp,heap_aux[child])) {
					break;
				}
				else {
					knn[2*index] = knn[2*child]; 
					knn[2*index+1] = knn[2*child+1];
					index = child;
				}
			}
//			heap_aux[index] = irp;
			knn[2*index] = local_id; 
			knn[2*index+1] = local_score;
//			knn[2*index] = irp.integer; 
//			knn[2*index+1] = (long) (irp.score*corr);
			
		}


		return sortedArray;
	}

	
	
	@Override
	public KNNGraphArray_sem clone() {
		int nb_users_ = knng.length;
		int l_ = knng[0].length;
		int[][] knng_ = new int[nb_users_][l_];
		for(int i=0; i < nb_users_; i++) {
			for(int j=0; j<l_; j++) {
				knng_[i][j]=knng[i][j];
			}
		}
		KNNGraphArray_sem clone = new KNNGraphArray_sem(knng_);
		return clone;
	}

	@Override
	public void put(int user, KNN_i knn) {
		put(user, (KNN) knn);
	}

}
