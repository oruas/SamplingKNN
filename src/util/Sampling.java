package util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import dataset.Dataset;

public final class Sampling {

	public static final int[] get_sample(int[] users, double ratio) {
		int size_sampling = (int) (((double) users.length) * ratio);
		return (get_sample(users, size_sampling));
	}
	public static final int[] get_sample(int[] users, int size_sampling) {
		int[] sampledUsers = new int[size_sampling];
		Random randomGenerator = new Random();
		HashSet<Integer> set_sampling = new HashSet<Integer>();
		while(set_sampling.size() != size_sampling) {
			set_sampling.add(users[randomGenerator.nextInt(size_sampling)]);
		}
//		for(int i = 0; i < size_sampling; i++) {
//			sampledUsers[i] = users[randomGenerator.nextInt(size_sampling)];
//		}
		Object[] set_sampling_array = set_sampling.toArray();
		for(int i = 0; i < size_sampling; i++) {
			sampledUsers[i] = (int) set_sampling_array[i];
		}
		
		return sampledUsers;
	}
	
	
	
	
	
	
	
	public static final int doSamplingB(Dataset dataset1, Dataset dataset2, int k, double ratio) {//, int size_sampling) {
//		KNNGraph knng1 = new KNNGraph();
//		KNNGraph knng2 = new KNNGraph();
		KNNGraph knng1 = new KNNGraph(k,dataset1.getUsers().size(),dataset1.getInitValue());
		KNNGraph knng2 = new KNNGraph(k,dataset2.getUsers().size(),dataset2.getInitValue());
		
		Set<Integer> usersSet = knng1.get_users();
		Integer[] users = new Integer[usersSet.size()];
		usersSet.toArray(users);
		

		int size_sampling = (int) (((double) usersSet.size()) * ratio);
		int new_k = (int) (((double) k) * ratio);
		
		int[] samplingUsers = new int[size_sampling];

		Random randomGenerator = new Random();
		
		for(int i = 0; i < size_sampling; i++) {
			samplingUsers[i] = users[randomGenerator.nextInt(size_sampling)];
		}
		
		for(int user: samplingUsers) {
			knng1.init(user, size_sampling, dataset1.getInitValue());
			knng2.init(user, new_k, dataset2.getInitValue());
		}
		for(int user:samplingUsers) {
			for(int user2: samplingUsers) {
				if(user2 > user) {
					double sim = dataset1.sim(user, user2);
					knng1.put(user, user2, sim);
					knng1.put(user2, user, sim);
					sim = dataset2.sim(user, user2);
					knng2.put(user, user2, sim);
					knng2.put(user2, user, sim);
				}
			}
		}
		
		int[] max_ranking = new int[size_sampling];
		
		for(int i = 0; i<size_sampling; i++){
			int user = samplingUsers[i];
//			IntDoublePair[] ranking1 = knng1.get_KNN(user).toSortedArray();
			IntDoublePair[] ranking1 = knng1.get_neighbors(user);
			int[] ranking2 = knng2.get_neighbors_ids(user);
			Set<Integer> rankedItems = new HashSet<Integer>();
			for(int neighbor: ranking2) {
				rankedItems.add(neighbor);
			}
			for(int j = 0; j<size_sampling; j++) {
				if(rankedItems.contains(ranking1[j].integer)) {
					max_ranking[i] = j;
				}
			}
		}
		
		int sum = 0;
		for (int i = 0; i<size_sampling;i++) {
			sum = sum + max_ranking[i];
		}
		
		double estim_B = ((double) sum) / ((double) size_sampling); //mean? average? max?

		return ((int) (estim_B / (ratio)));
//		return ((int) (B / ((double) (1+new_k))));
	}
	
}
