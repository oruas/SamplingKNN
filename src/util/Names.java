package util;

public final class Names {

	public static final String datasetName = "Name"; //Name of the dataset
	public static final String datasetStructure = "Structure"; //Structure of the dataset
	public static final String datasetBF = "datasetBF"; //Dataset using BloomFilters
	public static final String datasetHM = "datasetHM"; //Full Dataset, using HashMap
	public static final String datasetHMCosine = "datasetHMCosine"; //Full Dataset, using HashMap
	public static final String datasetMinMax = "datasetMinMax"; //Full Dataset, using HashMap
	public static final String datasetSketch = "datasetSketch"; //Dataset using Sketchs
	public static final String datasetBitArray = "datasetBitArray"; //Dataset using Sketchs
	public static final String datasetBitArrayNative = "datasetBitNative"; //Dataset using Sketchs
	public static final String datasetBitArrayNativeArray = "datasetBitNativeArray"; //Dataset using Sketchs
//	public static final String datasetBitSet = "datasetBitSet"; //Dataset using BitSet
	
	public static final String datasetABC = "datasetABC"; //Dataset using Adaptive Bitset Comparisons
	public static final String datasetABC_ = "datasetABC_"; //Dataset using Adaptive Bitset Comparisons
	public static final String datasetLexi = "datasetLexi"; //Dataset using Lexicographic similarity
	
	public static final String BloomFilter = "BloomFilter";
	public static final String HashMap = "HashMap";
	public static final String HashMapFull = "HashMapFull";
	public static final String HashMapCosine = "HashMapCosine";
	public static final String MinMax = "MinMax";
	public static final String Sketch = "Sketch";
	public static final String ABC = "ABC";
	public static final String ABC_ = "ABC_";
	public static final String BitArray = "BitArray";
	public static final String BitArrayAdd = "BitArrayAdd";
	public static final String BitArrayNative = "BitArrayNative";
	public static final String BBMH = "BBMH";
	public static final String BitSet = "BitSet";
	public static final String BitArrayArray = "BitArrayArray";
	
	
	//Alternative datastructures
	public static final String ISCP = "ISCP";
	public static final String ISCPC = "ISCPC";
	public static final String ISCPI = "ISCPI";
	public static final String ISCPIC = "ISCPIC";
	public static final String MLP = "MLP";
	public static final String MLPC = "MLPC";
	public static final String SCP = "SCP";
	public static final String SCPC = "SCPC";
	public static final String SCS = "SCS";
	public static final String SCSC = "SCSC";
	public static final String LP = "LP";
	public static final String MP = "MP";
	

	public static final String AVRatings = "AVRatings"; //Expected average number of ratings inserted into the BloomFilter
	public static final String pfp = "pfp"; //Percent of false positives of BloomFilter
	public static final String initValue = "initValue"; //Default value of the dataset
	public static final String nb_users = "nb_users"; //Number of users in the dataset
	public static final String nb_items = "nb_items"; //Number of items in the dataset
	public static final String nb_bits = "nb_bits"; //Number of bits in a BloomFilter or in a sketch (the total sketch is this size times nb_hash)
	public static final String nb_hash = "nb_hash"; //Number of hash of a BloomFilter or number of Sketchs used (one for each hash function)
	public static final String nb_hash_try = "nb_hash_try"; //Number of clustering schemes used
	public static final String rec_index = "rec_index"; //Number of recursive calls done while splitting big clusters
//	public static final String nb_bitset = "nb_bitset"; //Number of bitset each user have for ABC datastructure.
//	public static final String min_bitset_size = "min_bitset_size"; //log of the minimum size for a bitset. e.g. 6 => 2^6 = 64
//	public static final String add_size = "add_size"; //The size of the minimum bitset for an user is 2^(log(Profile)+add_size)  e.g. add_size = 3 mean that the profile will fill at most 1/(1+3) = 1/4 of the bitset
	public static final String ratio = "ratio"; //Ratio used in the lexicographic dataset.
	public static final String BBMHb = "BBMHb"; //Ratio used in the lexicographic dataset.
	public static final String BBMHhash = "BBMHhash"; //Ratio used in the lexicographic dataset.

	public static final String algoName = "Name"; //Name of the Algorithm
	public static final String bruteForce = "BruteForce"; //Brute force algorithm
	public static final String hyrec = "Hyrec"; //Hyrec algorithm
	public static final String nndescent = "NNDescent"; //NNDescent algorithm
	public static final String cork = "Cork"; //Cork algorithm
	public static final String random = "Random"; //Random algorithm
	public static final String LSH = "LSH"; //LSH algorithm
	public static final String LSH_fast_hash = "LSH_fast_hash"; //LSH algorithm
	public static final String Hybrid_first_version = "Hybrid_first_version"; //Hybrid algorithm
	public static final String Hybrid_MH = "Hybrid_MH"; //Hybrid algorithm
	public static final String Hybrid_MH_TP = "Hybrid_MH_TP"; //Hybrid algorithm
	public static final String ClusterAndConquer = "ClusterAndConquer"; //Hybrid algorithm
	public static final String ClusterAndConquer_LSHhash = "ClusterAndConquer_LSHhash"; //Hybrid algorithm
	public static final String ClusterAndConquer_ind_buckets = "ClusterAndConquer_ind_buckets"; //Hybrid algorithm
	public static final String ClusterAndConquer_rec = "ClusterAndConquer_rec"; //Hybrid algorithm
	public static final String Hybrid_MH_LSHhash = "Hybrid_MH_LSHhash"; //Hybrid algorithm
	public static final String Hybrid_MH_ind_buckets = "Hybrid_MH_ind_buckets"; //Hybrid algorithm
	public static final String HybridOptiMem = "HybridOptiMem"; //HybridOptiMem algorithm

	public static final String k = "k"; //size of the KNN
	public static final String nb_proc = "nb_proc"; //number of threads in parallel
	public static final String iter = "iterations"; //Number of iterations
	public static final String r = "r";
	public static final String delta = "delta";
	public static final String rho = "rho";

	public static final String time = "time";
	public static final String scanrate = "scanrate";
	public static final String nb_comp = "nb_comp";
	public static final String iteration = "iteration";
	
	public static final String nb_buckets = "nb_buckets"; //Number of buckets in LSH
	
	public static final String prob = "prob"; //Prob for ISCP ISCPI SCP
	public static final String size = "size"; //size for MLP SCS



	public static final String knngFile = "KNNG.txt";
	public static final String timeFile = "time.txt";
	public static final String scanFile = "scanrate.txt";
	public static final String compIterFile = "compIter.txt";
	public static final String changeIterFile = "changeIter.txt";
	public static final String recoFile = "rec.txt";
	public static final String recoTimeFile = "recTime.txt";

	public static final String knngFile1 = "KNNG1.txt";
	public static final String timeFile1 = "time1.txt";
	public static final String scanFile1 = "scanrate1.txt";
	public static final String compIterFile1 = "compIter1.txt";
	public static final String changeIterFile1 = "changeIter1.txt";

	public static final String knngFile2 = "KNNG2.txt";
	public static final String timeFile2 = "time2.txt";
	public static final String scanFile2 = "scanrate2.txt";
	public static final String compIterFile2 = "compIter2.txt";
	public static final String changeIterFile2 = "changeIter2.txt";
	
	public static final String timeProcFile = "time_per_proc.txt";
	public static final String timeBucketFile = "time_bucket.txt";

	public static final String paramFile = "Config.txt";
	public static final String paramFile1 = "Config1.txt";
	public static final String paramFile2 = "Config2.txt";

	public static final String paramDataFile = "ConfigDataset.txt";
	public static final String paramDataFile1 = "ConfigDataset1.txt";
	public static final String paramDataFile2 = "ConfigDataset2.txt";


	public static final String optionAlgoShort = "a";
	public static final String optionAlgoLong = "algo";
	public static final String optionDatasetShort = "d";
	public static final String optionDatasetLong = "Dataset";
	public static final String optionPartitionShort = "part";
	public static final String optionPartitionLong = "partition";
	public static final String optionInitValueShort = "iv";
	public static final String optionInitValueLong = "initValue";
	public static final String optionOutputAddShort = "add";
	public static final String optionOutputAddLong = "outputAdd";
	public static final String optionNbProcShort = "p";
	public static final String optionNbProcLong = "nb_proc";
	public static final String optionkShort = "k";
	public static final String optionkLong = "k";
	public static final String optionBShort = "B";
	public static final String optionBLong = "B";
	public static final String optionIterShort = "i";
	public static final String optionIterLong = "iter";
	public static final String optionIter_Short = "i_";
	public static final String optionIter_Long = "iter_";
	public static final String optionrShort = "r";
	public static final String optionrLong = "r";
	public static final String optionr_Short = "r_";
	public static final String optionr_Long = "r_";
	public static final String optionDeltaShort = "delta";
	public static final String optionDeltaLong = "delta";
	public static final String optionDelta_Short = "delta_";
	public static final String optionDelta_Long = "delta_";
	public static final String optionRhoShort = "rho";
	public static final String optionRhoLong = "rho";
	public static final String optionRho_Short = "rho_";
	public static final String optionRho_Long = "rho_";
	public static final String optionDatastructureShort = "ds";
	public static final String optionDatastructureLong = "Datastructure";
	public static final String optionDatastructure_Short = "ds_";
	public static final String optionDatastructure_Long = "Datastructure_";
	public static final String optionNbHashShort = "hash";
	public static final String optionNbHashLong = "nb_hash";
	public static final String optionNbHash_Short = "hash_";
	public static final String optionNbHash_Long = "nb_hash_";
	public static final String optionNbBitsShort = "bits";
	public static final String optionNbBitsLong = "nb_bits";
	public static final String optionNbBits_Short = "bits_";
	public static final String optionNbBits_Long = "nb_bits_";
	public static final String optionTimeShort = "t";
	public static final String optionTimeLong = "time";
	public static final String optionScanrateShort = "s";
	public static final String optionScanrateLong = "scanrate";
	public static final String optionRecoShort = "rec";
	public static final String optionRecoLong = "recommendation";
//	public static final String optionEstimBShort = "estimB";
//	public static final String optionEstimBLong = "estimationB";
//	public static final String optionEstimNbBitsetShort = "estimNbBitset";
//	public static final String optionEstimNbBitsetLong = "estimationNbBitset";
//	public static final String optionNbBitsetShort = "NbBitset";
//	public static final String optionNbBitsetLong = "NbBitset";
//	public static final String optionMinBitsetSizeShort = "MBS";
//	public static final String optionMinBitsetSizeLong = "min_bitset_size";
//	public static final String optionAddSizeShort = "AS";
//	public static final String optionAddSizeLong = "add_size";

	public static final String optionLSHDatastructureShort = "dsLSH";
	public static final String optionLSHDatastructureLong = "Datastructure2";
	public static final String optionLSHNbHashShort = "LSHhash";
	public static final String optionLSHNbHashLong = "LSHnb_hash";
//	public static final String optionLSHNbHash_Short = "LSHhash_";
//	public static final String optionLSHNbHash_Long = "LSHnb_hash_";
	public static final String optionLSHNbBucketsShort = "LSHbuckets";
	public static final String optionLSHNbBucketsLong = "LSHnb_buckets";
//	public static final String optionLSHNbBuckets_Short = "LSHbuckets_";
//	public static final String optionLSHNbBuckets_Long = "LSHnb_buckets_";
	public static final String optionLSHNbHashTryShort = "LSHhash_try";
	public static final String optionLSHNbHashTryLong = "LSHnb_hash_try";
	
	public static final String optionLSH_ds_NbBitsShort = "LSH_ds_bits";
	public static final String optionLSH_ds_NbBitsLong = "LSH_ds_nb_bits";
	public static final String optionLSH_ds_BBMHNbHashShort = "LSH_ds_BBMHhash";
	public static final String optionLSH_ds_BBMHNbHashLong = "LSH_ds_BBMHnb_hash";
	public static final String optionLSH_ds_BBMHbShort = "LSH_ds_BBMHb";
	public static final String optionLSH_ds_BBMHbLong = "LSH_ds_BBMHb";
	public static final String optionLSH_ds_ProbShort = "LSH_ds_prob";
	public static final String optionLSH_ds_ProbLong = "LSH_ds_probability";	
	public static final String optionLSH_ds_SizeShort = "LSH_ds_size";
	public static final String optionLSH_ds_SizeLong = "LSH_ds_Size";
	
	public static final String optionBBMHNbHashShort = "BBMHhash";
	public static final String optionBBMHNbHashLong = "BBMHnb_hash";
	public static final String optionBBMHNbHash_Short = "BBMHhash_";
	public static final String optionBBMHNbHash_Long = "BBMHnb_hash_";
	public static final String optionBBMHbShort = "BBMHb";
	public static final String optionBBMHbLong = "BBMHb";
	public static final String optionBBMHb_Short = "BBMHb_";
	public static final String optionBBMHb_Long = "BBMHb_";
	
	public static final String optionProbShort = "prob";
	public static final String optionProbLong = "probability";	
	public static final String optionSizeShort = "size";
	public static final String optionSizeLong = "Size";
	
	
	public static final String datasetSplit = " ";

}
