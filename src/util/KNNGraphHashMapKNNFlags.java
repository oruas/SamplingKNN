package util;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class KNNGraphHashMapKNNFlags implements KNNGFlags {

	private Map<Integer, KNNFlags> knng;
	
	public KNNGraphHashMapKNNFlags() {
		this.knng = new ConcurrentHashMap<Integer, KNNFlags>();
	}
	
	public KNNGraphHashMapKNNFlags(KNNGraph knng_, boolean b) {
		knng = new ConcurrentHashMap<Integer,KNNFlags>();
		for(int user:knng_.get_users()) {
//			knng.put(user, new KNNFlags(knng_.get_KNN(user),b));
			knng.put(user, new KNNFlags(knng_.get_neighbors(user),b));
		}
	}
	
	public void init(int user, int k, double score_init) {
		this.knng.put(user, new KNNFlags(k, score_init));
	}
	
	public boolean put(int user, int neighbor, double score, boolean bool) {
		return this.knng.get(user).add(new IntDoubleBoolTriplet(neighbor,score,bool));
	}
	
	public boolean put_AS(int user, int neighbor, double score, boolean bool) {
		return this.knng.get(user).add(new IntDoubleBoolTriplet(neighbor,score,bool));
	}
	
//	public boolean put(int user, IntDoubleBoolTriplet idp) {
//		return this.knng.get(user).add(idp);
//	}
	
	public void put(int user, KNNFlags knn) {
		this.knng.put(user, knn);
	}
	
	public int[] get_neighbors_ids(int user) {
		return this.knng.get(user).itemArray();
	}
	
	
	public IntDoubleBoolTriplet[] get_neighbors(int user) {
		return this.knng.get(user).toArray();
	}
	
	public boolean has_neighbors(int user) {
		return (this.knng.get(user) != null);
	}
	
	public Set<Integer> get_users() {
		return knng.keySet();
	}
	
//	public KNNFlags get_KNN(int user) {
//		return knng.get(user);
//	}
	
	
	public KNNGraph toKNNGraph() {
//		ConcurrentHashMap<Integer,KNN> new_knng = new ConcurrentHashMap<Integer,KNN>();
//		KNNGraph new_knng = new KNNGraph();
		int k=0;
		for (int user: knng.keySet()) {
			k = knng.get(user).toArray().length;
			break;
		}
		KNNGraph new_knng = new KNNGraph(k,knng.size(),0);
		for(int user: knng.keySet()) {
			new_knng.put(user, knng.get(user).toKNN());
		}
		return new_knng;
	}
	

	//In this case, idbt is the object we want to change, user is not useless
	public void changeBoolToFalse(int user, IntDoubleBoolTriplet idbt) {
		idbt.bool = false;
	}
	
	@Override
	public KNNGraphHashMapKNNFlags clone() {
		KNNGraphHashMapKNNFlags clone = new KNNGraphHashMapKNNFlags();
		for(int index: this.knng.keySet()) {
			clone.put(index, this.knng.get(index).clone());
		}
		return clone;
	}
}
