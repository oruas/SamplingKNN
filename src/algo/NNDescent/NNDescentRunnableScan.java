package algo.NNDescent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import parameters.ParametersNNDescent;
import util.Counter;
import util.KNNGraphFlags;


public final class NNDescentRunnableScan implements Runnable {

	private final ParametersNNDescent params;
	
	private final int[] users;
	private final KNNGraphFlags B;
	private final Map<Integer, Set<Integer>> newneighbors;
	private final Map<Integer, Set<Integer>> oldneighbors;
	private final Map<Integer, Set<Integer>> newneighbors_;
	private final Map<Integer, Set<Integer>> oldneighbors_;
	
//	AtomicInteger counter;
	private final Counter counter;
	private final Counter counter_scanrate;
	
	private final int loop;

//	AtomicLong scanrateCounter;
	
	
	public NNDescentRunnableScan(ParametersNNDescent params, int[] users, KNNGraphFlags B, Map<Integer, Set<Integer>> newneighbors, Map<Integer, Set<Integer>> oldneighbors, Map<Integer, Set<Integer>> newneighbors_, Map<Integer, Set<Integer>> oldneighbors_, Counter counter, Counter counter_scanrate, int loop) {
		this.params = params;
		this.users = users;
		this.B = B;
		this.newneighbors = newneighbors;
		this.oldneighbors = oldneighbors;
		this.newneighbors_ = newneighbors_;
		this.oldneighbors_ = oldneighbors_;
		this.counter = counter;
		this.loop = loop;
		this.counter_scanrate = counter_scanrate;

//		this.scanrateCounter = atomcounter;


		
		
	}
	
	
	@Override
	public void run() {
		
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);

		
//
		Map<Integer, Set<Integer>> newneighbors_bis = new HashMap<Integer, Set<Integer>>();
		Map<Integer, Set<Integer>> oldneighbors_bis = new HashMap<Integer, Set<Integer>>();
		
		for(int user : user_ids) {
//			newneighbors_bis.put(user, idbt_set_clone(newneighbors.get(user)));
//			oldneighbors_bis.put(user, idbt_set_clone(oldneighbors.get(user)));
			newneighbors_bis.put(user, set_clone(newneighbors.get(user)));
			oldneighbors_bis.put(user, set_clone(oldneighbors.get(user)));
		}
		
		Random randomGenerator = new Random();
		Set<Integer> old_v_set;
		Set<Integer> new_v_set;
		int[] old_v;
		int[] new_v;
		int index;
		double sim;
		int count = 0;
		int count_scanrate = 0;
		for(int user: user_ids) {
			old_v_set = oldneighbors_.get(user);
			
			if(old_v_set != null) {

				old_v = new int[old_v_set.size()];
				index = 0;
				for(int idbt: old_v_set) {
					old_v[index] = idbt;
					index = index + 1;
				}
				
				for(int nb_random = 0; nb_random < params.rho() * params.k(); nb_random++) {
					if(oldneighbors_bis.get(user) == null) {
						oldneighbors_bis.put(user, new HashSet<Integer>());
					}
					oldneighbors_bis.get(user).add(old_v[randomGenerator.nextInt(old_v.length)]);
				}
			}


			new_v_set = newneighbors_.get(user);

			if(new_v_set != null) {
				new_v = new int[new_v_set.size()];
				index = 0;
				for(int idbt: new_v_set) {
					new_v[index] = idbt;
					index = index + 1;
				}
				int[] index_table = new int[new_v.length];
				for(int i = 0; i < new_v.length; i++) {
					index_table[i] = i;
				}
				for(int nb_random = 0; nb_random < params.rho() * params.k(); nb_random++) {
					if(newneighbors_bis.get(user) == null) {
						newneighbors_bis.put(user, new HashSet<Integer>());
					}
					newneighbors_bis.get(user).add(new_v[randomGenerator.nextInt(new_v.length)]);
				}
			}


			for(int u1:newneighbors_bis.get(user)) {
				for(int u2:newneighbors_bis.get(user)) {
					if(u1 < u2) {
						sim = params.dataset().sim(u1, u2);

//						if(ParametersStats.measure_scanrate) {
//							paramStats.inc();
//						}
						count_scanrate++;
						
//						scanrateCounter.incrementAndGet();
						if(B.put(u1, u2, sim , true)) {
//						if(B.get_KNN(u1).add(new IntDoubleBoolTriplet(u2, sim , true))) {
//							counter.inc();
							count++;
						}
						if(B.put(u2, u1, sim, true)) {
//						if(B.get_KNN(u2).add(new IntDoubleBoolTriplet(u1, sim, true))) {
//							counter.inc();
							count++;
						}
					}
				}
				for(int u2:oldneighbors_bis.get(user)) {
					if(u2 != u1 && u2 != -1) {
						sim = params.dataset().sim(u1, u2);

//						if(ParametersStats.measure_scanrate) {
//							paramStats.inc();
//						}
						count_scanrate++;
						
//						scanrateCounter.incrementAndGet();
						if(B.put(u1, u2, sim , true)) {
//						if(B.get_KNN(u1).add(new IntDoubleBoolTriplet(u2, sim , true))) {
//							counter.inc();
							count++;
						}
						if(B.put(u2, u1, sim, true)) {
//						if(B.get_KNN(u2).add(new IntDoubleBoolTriplet(u1, sim, true))) {
//							counter.inc();
							count++;
						}
					}
				}
			}
		}

		counter.inc(count);
//		if(ParametersStats.measure_scanrate) {
//			paramStats.inc();
		counter_scanrate.inc(count_scanrate);
//		}
		
		
	}
	
	
	private static Set<Integer> set_clone(Set<Integer> set) {
		Set<Integer> clone = new HashSet<Integer>();
		for(int item:set) {
			clone.add(item);
		}
		return clone;
	}
	

//	public Set<IntDoubleBoolTriplet> idbt_set_clone(Set<IntDoubleBoolTriplet> set) {
//		Set<IntDoubleBoolTriplet> clone = new HashSet<IntDoubleBoolTriplet>();
//		for(IntDoubleBoolTriplet idbt : set) {
//			clone.add(idbt.clone());
//		}
//		return clone;
//	}

}
