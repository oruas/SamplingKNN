package algo.NNDescent;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import parameters.ParametersNNDescent;
import util.IntDoubleBoolTriplet;
import util.KNNGraphFlags;

public final class NNDescentRunnableOldNew implements Runnable {

	private final int[] users;
	private final KNNGraphFlags B;
	private final Map<Integer, Set<Integer>> newneighbors;
	private final Map<Integer, Set<Integer>> oldneighbors;
	private final ParametersNNDescent params;
	private final int loop;
	
	
	public NNDescentRunnableOldNew(ParametersNNDescent params, int[] users, KNNGraphFlags B, Map<Integer, Set<Integer>> newneighbors, Map<Integer, Set<Integer>> oldneighbors, int loop) {
		this.users = users;
		this.B = B;
		this.newneighbors = newneighbors;
		this.oldneighbors = oldneighbors;
		this.loop = loop;
		this.params = params;
	}
	
	
	@Override
	public void run() {


		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		int nb;
		for(int user: user_ids) {
			oldneighbors.put(user, new HashSet<Integer>());
			newneighbors.put(user, new HashSet<Integer>());
			nb = 0;
//			for(IntDoubleBoolTriplet idbt: B.get_KNN(user).toArray()){
			for(IntDoubleBoolTriplet idbt: B.get_neighbors(user)){
				if(idbt.integer != -1) {
					if(idbt.bool) {
						//TODO IMPROVE THE SAMPLE... -> change the flag...
						if(nb < params.rho() * params.k()) {
//							idbt.bool = false;
							B.changeBoolToFalse(user, idbt);
							newneighbors.get(user).add(idbt.integer);
							nb = nb + 1;
						}
					}
					else {
						oldneighbors.get(user).add(idbt.integer);
					}
				}
			}
		}
		
	}

}
