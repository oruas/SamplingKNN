package algo.HyrecFlags;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import parameters.ParametersHyrec;
import parameters.ParametersStats;
//import util.IntDoubleBoolTriplet;
//import util.KNNFlags;
import util.KNNGraphFlags;

public final class HyrecFlagsRunnableSamplingInit implements Runnable{

	private final int[] users;
	private final ParametersHyrec params;
	private final ParametersStats paramStats;
	private final KNNGraphFlags knng;
	private final int loop;
	
	public HyrecFlagsRunnableSamplingInit(ParametersHyrec params, ParametersStats paramStats, int[] users, KNNGraphFlags knng, int loop) {
		this.users = users;
		this.knng = knng;
		this.loop = loop;
		this.params = params;
		this.paramStats = paramStats;
	}
	
	
	@Override
	public void run() {

		Random randomGenerator = new Random();

		int nb_user = users.length;
		
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		Set<Integer> random_neighbors;
		int random_user;
//		KNNFlags neighbors;
		for(int user: user_ids) {
			knng.init(user, params.k(), params.dataset().getInitValue()-1);
			random_neighbors = new HashSet<Integer>();
			while (random_neighbors.size() != params.k()) {
				random_user = users[randomGenerator.nextInt(nb_user)];
				while(random_user == user) {
					random_user = users[randomGenerator.nextInt(nb_user)];
				}
				random_neighbors.add(random_user);
			}
			
//			neighbors = new KNNFlags(params.k(), params.dataset().getInitValue());
			for(int random_user_id: random_neighbors) {
				
//				neighbors.add(new IntDoubleBoolTriplet(random_user_id, params.dataset().sim(user, random_user_id),true));
				knng.put_AS(user,random_user_id, params.dataset().sim(user, random_user_id),true);
				if(ParametersStats.measure_scanrate) {
					paramStats.inc();
				}
			}
			
//			knng.put(user, neighbors);
		}
		
	}

}
