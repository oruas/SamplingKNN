package algo.LSH;

import java.util.HashSet;
import util.Permutations;

public final class LSHCreationPermutationsRunnable implements Runnable{
	private final HashSet<Integer> items;
	private Permutations permutation;
	private final int loop;
	
	
	public LSHCreationPermutationsRunnable(HashSet<Integer> items, int loop) {
		this.items = items;
		this.loop = loop;
	}


	@Override
	public void run() {
		permutation = new Permutations(items,loop);
	}
	
	public Permutations get_permutation() {
		return this.permutation;
	}

}
