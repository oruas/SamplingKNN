package algo.LSH;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersLSH;
import parameters.ParametersStats;
import util.Counter;
import util.KNN;
import util.KNNGraph;
import util.Permutations;

public final class LSH implements Algo {

	private final ParametersLSH params;
	private final ParametersStats paramStats;
	
	public LSH(ParametersLSH params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
	}

	@Override
	public KNNGraph doKNN(){
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

//		long start_LSH = System.currentTimeMillis();
		
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		
		@SuppressWarnings("unchecked")
		HashMap<Integer,Set<Integer>>[] buckets = (HashMap<Integer,Set<Integer>>[]) new HashMap[params.nb_hash()]; //HashSet<Integer>[]???
		for(int i = 0; i < params.nb_hash(); i++) {
			buckets[i] = new HashMap<Integer,Set<Integer>>();
		}
		Permutations[] permutations = new Permutations[params.nb_hash()];
		
		HashSet<Integer> items = new HashSet<Integer>();
		for(int user:params.datasetHash().getUsers()) {
			items.addAll(params.datasetHash().getRatedItems(user));
		}
		
		//PARALLEL CREATION
//		Thread[] threads = new Thread[params.nb_hash()];
//		LSHCreationPermutationsRunnable[] runnables = new LSHCreationPermutationsRunnable[params.nb_hash()];
//		for(int i = 0; i < params.nb_hash(); i++) {
//			runnables[i] = new LSHCreationPermutationsRunnable(items,i);
//			threads[i] = new Thread(runnables[i]);
//			threads[i].start();
//		}
//
//		for(int i = 0; i < params.nb_hash(); i++) {
//			try {
//				threads[i].join();
//				permutations[i] = runnables[i].get_permutation();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
		
		//SEQUENTIAL CREATION
		for(int permutation = 0; permutation < params.nb_hash(); permutation++) {
			permutations[permutation] = new Permutations(items,permutation);
		}
		
		
//		System.out.println("Creation of buckets: " + (System.currentTimeMillis() - start_LSH) + "ms.");
//		start_LSH = System.currentTimeMillis();
		
		
		
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		
//		long bucket_start = System.currentTimeMillis();
		//To create a synchronized set, use ConcurrentHashMap.newKeySet();
		Set<Integer> user_aux = params.dataset().getUsers();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
			users[index] = user;
			index++;
		}
		
		for(int permutation = 0; permutation < params.nb_hash(); permutation++) {
			//PARALLEL FILLING
//			threads = new Thread[params.nb_proc()];
//			for(int i = 0; i < params.nb_proc(); i++) {
//				threads[i] = new Thread(new LSHFillingBucketsRunnable(params, buckets[permutation], permutations[permutation], users, i));
//				threads[i].start();
//			}
//
//			for(int i = 0; i < params.nb_proc(); i++) {
//				try {
//					threads[i].join();
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			}
			
			//SEQUENTIAL FILLING
			for(int user: users) {
				int bucket_id = permutations[permutation].getHash(params.datasetHash().getRatedItems(user));
				if(buckets[permutation].get(bucket_id)==null) {
					Set<Integer> set = new HashSet<Integer>();//ConcurrentHashMap.newKeySet();
					buckets[permutation].put(bucket_id, set);
				}
//				System.out.println(bucket_id);
//				System.out.println(buckets.get(bucket_id));
//				System.out.println(user);
				buckets[permutation].get(bucket_id).add(user);
//				System.out.println(buckets.get(bucket_id));
			}
		}
//		System.out.println("filling bucket in:");
//		System.out.println(System.currentTimeMillis() - bucket_start);

//		System.out.println("Filling buckets: " + (System.currentTimeMillis() - start_LSH) + "ms.");
//		start_LSH = System.currentTimeMillis();
		
		
		/*
		 * CREATION OF THE KNN
		 */
		
		
//		bucket_start = System.currentTimeMillis();
		
		Counter[] counters = new Counter[params.nb_proc()];
		
		
		

		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				threads[i] = new Thread(new LSHRunnableScan(params, knng, users, counters[i], i, buckets, permutations, params.dataset()));
			}
			else {
				threads[i] = new Thread(new LSHRunnable(params, knng, users, i, buckets, permutations, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}

//		System.out.println("Doing KNNG: " + (System.currentTimeMillis() - start_LSH)/1000 + "s.");
//		start_LSH = System.currentTimeMillis();

//		System.out.println("doing knn in:");
//		System.out.println(System.currentTimeMillis() - bucket_start);
		
		return knng;
	}

	
	
	
	
	
	
	
	
	
	
	
	/*
	 * doKNN(int[] users) will do the KNN of each user in users, using the users of ALL the dataset
	 * */
	
	@Override
	public KNNGraph doKNN(int[] users){
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

		@SuppressWarnings("unchecked")
		HashMap<Integer,Set<Integer>>[] buckets = (HashMap<Integer,Set<Integer>>[]) new Map[params.nb_hash()]; //HashSet<Integer>[]???
		for(int i = 0; i < params.nb_hash(); i++) {
			buckets[i] = new HashMap<Integer,Set<Integer>>();
		}
		Permutations[] permutations = new Permutations[params.nb_hash()];
		
		HashSet<Integer> items = new HashSet<Integer>();
		for(int user:params.datasetHash().getUsers()) {
			items.addAll(params.datasetHash().getRatedItems(user));
		}
		
		
		Thread[] threads = new Thread[params.nb_hash()];
		LSHCreationPermutationsRunnable[] runnables = new LSHCreationPermutationsRunnable[params.nb_hash()];
		for(int i = 0; i < params.nb_hash(); i++) {
			runnables[i] = new LSHCreationPermutationsRunnable(items,i);
			threads[i] = new Thread(runnables[i]);
			threads[i].start();
		}

		for(int i = 0; i < params.nb_hash(); i++) {
			try {
				threads[i].join();
				permutations[i] = runnables[i].get_permutation();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		

		for(int permutation = 0; permutation < params.nb_hash(); permutation++) {
			threads = new Thread[params.nb_proc()];
			for(int i = 0; i < params.nb_proc(); i++) {
				threads[i] = new Thread(new LSHFillingBucketsRunnable(params, buckets[i], permutations[i], users, i));
				threads[i].start();
			}

			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		
		/*
		 * CREATION OF THE KNN
		 */
		
		
		
		
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		for(int user: users) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
		}
		
//		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];

		
		
		
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				threads[i] = new Thread(new LSHRunnableScan(params, knng, users, counters[i], i, buckets, permutations, params.dataset()));
			}
			else {
				threads[i] = new Thread(new LSHRunnable(params, knng, users, i, buckets, permutations, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		
//		if(ParametersStats.scanrate) {
//			paramStats.increase_scanrate();
//		}

		return knng;
	}

	
	
	
	
	
	
	
	@Override
	public KNNGraph doKNN(KNNGraph anng) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

		@SuppressWarnings("unchecked")
		HashMap<Integer,Set<Integer>>[] buckets = (HashMap<Integer,Set<Integer>>[]) new Map[params.nb_hash()]; //HashSet<Integer>[]???
		for(int i = 0; i < params.nb_hash(); i++) {
			buckets[i] = new HashMap<Integer,Set<Integer>>();
		}
		Permutations[] permutations = new Permutations[params.nb_hash()];
		
		HashSet<Integer> items = new HashSet<Integer>();
		for(int user:params.datasetHash().getUsers()) {
			items.addAll(params.datasetHash().getRatedItems(user));
		}
		
		
		Thread[] threads = new Thread[params.nb_hash()];
		LSHCreationPermutationsRunnable[] runnables = new LSHCreationPermutationsRunnable[params.nb_hash()];
		for(int i = 0; i < params.nb_hash(); i++) {
			runnables[i] = new LSHCreationPermutationsRunnable(items,i);
			threads[i] = new Thread(runnables[i]);
			threads[i].start();
		}

		for(int i = 0; i < params.nb_hash(); i++) {
			try {
				threads[i].join();
				permutations[i] = runnables[i].get_permutation();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		
		//To create a synchronized set, use ConcurrentHashMap.newKeySet();
		Set<Integer> user_aux = anng.get_users();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		
		for(int permutation = 0; permutation < params.nb_hash(); permutation++) {
			threads = new Thread[params.nb_proc()];
			for(int i = 0; i < params.nb_proc(); i++) {
				threads[i] = new Thread(new LSHFillingBucketsRunnable(params, buckets[i], permutations[i], users, i));
				threads[i].start();
			}

			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		
		/*
		 * CREATION OF THE KNN
		 */
		
		
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		
		Counter[] counters = new Counter[params.nb_proc()];
		
		
		int index = 0;
		for(int user: users_) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
			users[index] = user;
			index++;
		}
		
		
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				threads[i] = new Thread(new LSHRunnableANNScan(params, knng, anng, users, counters[i], i, buckets, permutations, params.dataset()));
			}
			else {
				threads[i] = new Thread(new LSHRunnableANN(params, knng, anng, users, i, buckets, permutations, params.dataset()));
			}
			threads[i].start();
		}

		
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		

		return knng;
	}



//	public void SetParameters(Parameters params) {
//		this.params = (ParametersBruteForce) params;
//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}
}
