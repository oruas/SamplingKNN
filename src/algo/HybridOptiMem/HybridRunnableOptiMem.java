package algo.HybridOptiMem;

import java.util.LinkedList;
import java.util.Set;

//import algo.Hyrec.HyrecRunnable;
//import algo.Hyrec.HyrecRunnableSamplingInit;
import dataset.DatasetBitArrayNativeSize;
import dataset.DatasetBitArrayNativeSize_array;
import parameters.ParametersHybridOptiMem;
import parameters.ParametersHyrec;
import util.Counter;
import util.IntDoublePair;
import util.KNN;
import util.KNNGraph;
import util.KNNGraphArray;

public final class HybridRunnableOptiMem implements Runnable{

	private final ParametersHybridOptiMem params;
	private final DatasetBitArrayNativeSize dataset;
	
	private final LinkedList<KNNGraph> knngs;
	private final LinkedList<Set<Integer>> knn_to_compute;
	
	
	public HybridRunnableOptiMem(ParametersHybridOptiMem params, LinkedList<KNNGraph> knngs, LinkedList<Set<Integer>> knn_to_compute, DatasetBitArrayNativeSize dataset) {
		this.knngs = knngs;
		this.params = params;
		this.knn_to_compute = knn_to_compute;
		this.dataset = dataset;
	}


	@Override
	public void run() {
		Integer[] users_;
		int[] users;
		int index;
		
		DatasetBitArrayNativeSize_array local_dataset;
		int[] users_compacted;
		KNNGraphArray knng;
		KNNGraphArray old_knng;
		KNNGraph new_knng;
		
		ParametersHyrec params_;
		HyrecRunnableSamplingInit hyrecInit;
		HyrecRunnable hyrecRun;
		

		double sim;
		
		for (Set<Integer> user_aux: knn_to_compute) {
			
			//Getting the corresponding users
			//The conversion from aux to array is nearly free in term of comp time
//			user_aux = buckets[index_i][index_j];
			users_compacted = new int[user_aux.size()];
			users_ = new Integer[user_aux.size()];
			users = new int[user_aux.size()];
			user_aux.toArray(users_);
			index = 0;
			for(int user: users_) {
				users_compacted[index]=index;
				users[index] = user;
				index++;
			}
			
			//Creating the corresponding KNNGraph
			knng = new KNNGraphArray(params.k(),users.length,params.dataset().getInitValue()-1);
			
			//Getting the corresponding dataset => needed?
			local_dataset = new DatasetBitArrayNativeSize_array((DatasetBitArrayNativeSize) dataset.subDataset(users));
			
			//If 5 * k * k < n then perform Hyrec, else perform BruteForce

			if(5*params.k()*params.k()<users.length) {
				//HYREC
				params_ = new ParametersHyrec(params.k(), params.ParametersToMap().get("Name"), local_dataset, 1);
				params_.set_r(0);
				params_.set_iterations(30);
				params_.set_delta(0.001);
				old_knng = new KNNGraphArray(params_.k(),users.length,local_dataset.getInitValue()-1);
				hyrecInit = new HyrecRunnableSamplingInit(params_, users_compacted, knng, old_knng, 0);
				hyrecInit.run();

				
				Counter counter = new Counter();
				for (int iter_hyrec = 0; iter_hyrec < params_.iterations(); iter_hyrec++) {
					counter.init();
					hyrecRun = new HyrecRunnable(params_, users_compacted, knng, old_knng, counter, 0);
					hyrecRun.run();
					if(counter.getValue() < params_.delta() * params_.k() * users.length) {
						break;
					}
					old_knng = knng.clone();
				}
			}
			else { //BRUTE FORCE
				for(int user_id: local_dataset.getUsers()) {
					knng.put(user_id, new KNN(params.k(), params.dataset().getInitValue()-1));
				}

				for(int user_id: local_dataset.getUsers()) {
					for(int user: local_dataset.getUsers()) {
						if(user < user_id) {
							sim = local_dataset.sim(user_id, user);
							knng.put(user_id,user,sim);
							knng.put(user, user_id,sim);
						}
					}
				}
			}

			new_knng= new KNNGraph(params.k(),users.length,0);
			int user_id;
			for(int user:knng.get_users()) {
				user_id = local_dataset.get_id(user);
				IntDoublePair[] idp = knng.get_neighbors(user);
				for(int s=0;s<params.k();s++) {
					if(idp[s].integer != -1) {
						idp[s].integer=local_dataset.get_id(idp[s].integer);
					}
				}
				new_knng.put(user_id, new KNN(idp));
			}
			knngs.add(new_knng);
		}
		
		
		
		
	}

}
