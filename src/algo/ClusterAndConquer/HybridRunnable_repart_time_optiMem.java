package algo.ClusterAndConquer;

//import java.io.IOException;
import java.util.LinkedList;
import java.util.Set;

import algo.Hyrec.HyrecRunnable;
import algo.Hyrec.HyrecRunnableSamplingInit;
//import dataset.Dataset;
import dataset.DatasetBitArrayNativeSize;
import dataset.DatasetBitArrayNativeSize_array;
//import io.IO;
import parameters.ParametersHybrid_MH;
import parameters.ParametersHyrec;
import util.Counter;
import util.IntDoublePair;
import util.KNN;
import util.KNNGraph;
import util.KNNGraphHashMapKNN;

public final class HybridRunnable_repart_time_optiMem implements Runnable{

	private final ParametersHybrid_MH params;
//	private final int loop;
	private final DatasetBitArrayNativeSize dataset;
	
//	private final LinkedList<KNNGraph> knngs;
	private final LinkedList<KNNGraphHashMapKNN> knngs;
//	private final int nb_hash;
//	private final int nb_bits;
//	private final Set<Integer>[][] buckets;
	private final LinkedList<Set<Integer>> knn_to_compute;
	private long comptime=-1;
	
	
//	public HybridRunnable(ParametersHybrid params, LinkedList<KNNGraph> knngs, int loop, Set<Integer>[][] buckets, Dataset dataset, int nb_hash, int nb_bits) {
//	public HybridRunnable_repart_time_optiMem(ParametersHybrid_MH params, LinkedList<KNNGraph> knngs, LinkedList<Set<Integer>> knn_to_compute, DatasetBitArrayNativeSize dataset) {
	public HybridRunnable_repart_time_optiMem(ParametersHybrid_MH params, LinkedList<KNNGraphHashMapKNN> knngs, LinkedList<Set<Integer>> knn_to_compute, DatasetBitArrayNativeSize dataset) {
//	public HybridRunnable(ParametersHybrid_MH params, LinkedList<KNNGraph> knngs, int loop, LinkedList<Set<Integer>> knn_to_compute, Dataset dataset) {
		this.knngs = knngs;
		this.params = params;
//		this.loop = loop;
//		this.buckets = buckets;
		this.knn_to_compute = knn_to_compute;
		this.dataset = dataset;
//		this.nb_hash = nb_hash;
//		this.nb_bits = nb_bits;
	}
	
	public long get_time() {
		return comptime;
	}


	@Override
	public void run() {
		
		long start = System.currentTimeMillis();
//		String s = "";
//		long sum=0;
//		long sum_square=0;
//		int max=0;
		
//		int nb_knng_computed = 0;
//		int aux;
//		for(int i=0; i<loop; i++) {
//			aux =(nb_bits * nb_hash) / params.nb_proc();
//			if ((params.nb_proc() % params.nb_proc()) > (params.nb_proc()  - (i+1))) {
//				aux++;
//			}
//			nb_knng_computed = nb_knng_computed + aux;
//		}
		
//		int nb_knng_to_compute  = (nb_bits * nb_hash) / params.nb_proc();
//		if((params.nb_proc() % params.nb_proc()) > (params.nb_proc()  - (loop+1))) {
//			nb_knng_to_compute++;
//		}
//		int nb_knng_to_compute  = knn_to_compute.size();
//		int index_i;
//		int index_j;
		
//		Set<Integer> user_aux;
		Integer[] users_;
		int[] users;
		int index;
		
//		Dataset local_dataset;
		DatasetBitArrayNativeSize_array local_dataset;
		int[] users_compacted;
//		DatasetBitArrayNativeSize local_big_dataset;
		KNNGraph knng;
//		KNNGraph new_knng;
		KNNGraphHashMapKNN new_knng;

		double sim;
		
		long measure_time = 0;
		int nb=0;
		long measure_time_bis = 0;
		int nb_bis=0;
		
//		@SuppressWarnings("unchecked")
//		Set<Integer>[] knn_ = (Set<Integer>[]) new Set[knn_to_compute.size()];
//		knn_to_compute.toArray(knn_);
		
//		for (Set<Integer> user_aux: knn_) {
//		for (int iter=0; iter < nb_knng_to_compute; iter++) {
		for (Set<Integer> user_aux: knn_to_compute) {
			
			//Setting the indexes of the bucket
//			index_i = nb_knng_computed / params.nb_bits();
//			index_j = nb_knng_computed % params.nb_bits();
//			nb_knng_computed++;
//			if(index_j >= params.nb_bits()) {
//				break;
//			}

			
			
			//Getting the corresponding users
			//The conversion from aux to array is nearly free in term of comp time
//			user_aux = buckets[index_i][index_j];
			users_compacted = new int[user_aux.size()];
			users_ = new Integer[user_aux.size()];
			users = new int[user_aux.size()];
			user_aux.toArray(users_);
			index = 0;
			for(int user: users_) {
				users_compacted[index]=index;
				users[index] = user;
				index++;
			}
			
//			s = s + users.length + " ";
//			sum = sum + users.length;
//			if(10*params.k()*params.k()<users.length) {
//				sum_square = sum_square + ((long) 5*params.k()*params.k()*((long)users.length));
//			}
//			else {
//				sum_square = sum_square + ((long) users.length-1)*((long) users.length/2);
//			}
//			if(users.length > max) {
//				max = users.length;
//			}
			
			//Creating the corresponding KNNGraph
			knng = new KNNGraph(params.k(),users.length,params.dataset().getInitValue()-1);
			
			//Getting the corresponding dataset => needed?
//			local_dataset = dataset.subDataset(users);
//			local_dataset = dataset;
			local_dataset = new DatasetBitArrayNativeSize_array((DatasetBitArrayNativeSize) dataset.subDataset(users));
			
			//If 5 * k * k < n then perform Hyrec, else perform BruteForce

			if(5*params.k()*params.k()<users.length) {
//			if(10*params.k()*params.k()<users.length) {
				//HYREC
				long start_ = System.currentTimeMillis();
//				local_big_dataset = (DatasetBitArrayNativeSize) dataset.subDataset(users);
				ParametersHyrec params_ = new ParametersHyrec(params.k(), params.ParametersToMap().get("Name"), local_dataset, 1);
				params_.set_r(0);
				params_.set_iterations(30);
				params_.set_delta(0.001);
				KNNGraph old_knng = new KNNGraph(params_.k(),users.length,local_dataset.getInitValue()-1);
				HyrecRunnableSamplingInit hyrecInit = new HyrecRunnableSamplingInit(params_, users_compacted, knng, old_knng, 0);
				hyrecInit.run();

				
				Counter counter = new Counter();
				for (int iter_hyrec = 0; iter_hyrec < params_.iterations(); iter_hyrec++) {
					counter.init();
					HyrecRunnable hyrecRun = new HyrecRunnable(params_, users_compacted, knng, old_knng, counter, 0);
					hyrecRun.run();
					if(counter.getValue() < params_.delta() * params_.k() * users.length) {
						break;
					}
					old_knng = knng.clone();
				}
				nb++;
				measure_time = measure_time + (System.currentTimeMillis() - start_);
			}
			else { //BRUTE FORCE
				long start_bis = System.currentTimeMillis();
//				local_dataset = new DatasetBitArrayNativeSize_array((DatasetBitArrayNativeSize) dataset.subDataset(users));
				for(int user_id: local_dataset.getUsers()) {
					knng.put(user_id, new KNN(params.k(), params.dataset().getInitValue()-1));
				}

				for(int user_id: local_dataset.getUsers()) {
					for(int user: local_dataset.getUsers()) {
						if(user < user_id) {
							sim = local_dataset.sim(user_id, user);
							knng.put(user_id,user,sim);
							knng.put(user, user_id,sim);
						}
					}
				}
				

//				knngs.add(knng);
				nb_bis++;
				measure_time_bis = measure_time_bis + (System.currentTimeMillis() - start_bis);
			}

			new_knng= new KNNGraphHashMapKNN(users.length);
//			new_knng= new KNNGraph(params.k(),users.length,0);
			int user_id;
			for(int user:knng.get_users()) {
				user_id = local_dataset.get_id(user);
				IntDoublePair[] idp = knng.get_neighbors(user);
				for(int s=0;s<params.k();s++) {
					if(idp[s].integer != -1) {
						idp[s].integer=local_dataset.get_id(idp[s].integer);
					}
				}
				new_knng.put(user_id, new KNN(idp));
			}
			knngs.add(new_knng);
		}
		
		comptime = System.currentTimeMillis()-start;
		
		System.out.println("measure time: " + measure_time);
		System.out.println("nb: " + nb);
		System.out.println("measure time bis: " + measure_time_bis);
		System.out.println("nb bis: " + nb_bis);
		
		
		
//		try {
//			IO.toFile(s, "./Files/graphs/", "essai_proc_nb_"+loop);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		System.out.println("proc: " + loop + " comp time: " + (System.currentTimeMillis()-start));
//		System.out.println("proc: " + loop + " nb_KNN: " + knn_to_compute.size());
//		System.out.println("proc: " + loop + " max: " + max);
//		System.out.println("proc: " + loop + " sum: " + sum);
//		System.out.println("proc: " + loop + " sum_square: " + sum_square);
//		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

}
