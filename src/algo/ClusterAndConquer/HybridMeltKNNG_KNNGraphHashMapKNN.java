package algo.ClusterAndConquer;

import java.util.LinkedList;
import java.util.Set;

import parameters.ParametersHybrid_MH;
import util.IntDoublePair;
//import util.KNN;
import util.KNNGraphHashMapKNN;

public final class HybridMeltKNNG_KNNGraphHashMapKNN implements Runnable{

	private final LinkedList<KNNGraphHashMapKNN> knngs;
	private final KNNGraphHashMapKNN knng;
	
	
	public HybridMeltKNNG_KNNGraphHashMapKNN(ParametersHybrid_MH params, LinkedList<KNNGraphHashMapKNN> knngs) {
		if(knngs !=null) {
			this.knngs = knngs;
			this.knng = knngs.remove();
		}
		else {
			this.knngs = new LinkedList<KNNGraphHashMapKNN>();
			this.knng = null;
		}
	}


	@Override
	public void run() {
		KNNGraphHashMapKNN knng_;
		Set<Integer> users = knng.get_users();
//		KNN knn;
		while(!knngs.isEmpty()) {
			knng_ = knngs.remove();
			if(knng_ != null) {
				for(int user : knng_.get_users()) {
					if(!users.contains(user)) {
						knng.init(user, knng_.get_neighbors(user).length, -1);
					}
					for(IntDoublePair idp : knng_.get_neighbors(user)) {
						knng.put(user, idp.integer, idp.score);
					}
				}
			}
		}
		
//		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

	public KNNGraphHashMapKNN getKNNG() {
		return knng;
	}
	
}
