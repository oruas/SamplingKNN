package algo.ClusterAndConquer;

//import java.util.HashMap;
//import java.util.HashSet;
//import java.io.IOException;
import java.util.LinkedList;
import java.util.Set;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.PriorityBlockingQueue;

import algo.Hyrec.HyrecRunnable;
import algo.Hyrec.HyrecRunnableSamplingInit;
import dataset.Dataset;
//import io.IO;
import parameters.ParametersHybrid_MH_rec;
import parameters.ParametersHyrec;
import util.Bucket;
import util.Counter;
//import util.IntIntPair;
import util.KNN;
import util.KNNGraph;

public final class HybridRunnable implements Runnable{

	private final ParametersHybrid_MH_rec params;
//	private final int loop;
	private final Dataset dataset;
	
	private final LinkedList<KNNGraph> knngs;
//	private final int nb_hash;
//	private final int nb_bits;
//	private final Set<Integer>[][] buckets;
//	private final LinkedList<Set<Integer>> knn_to_compute;
//	private final HashMap<Long,ConcurrentLinkedQueue<IntIntPair>> nbSimToID;
//	private final PriorityBlockingQueue<Long> nextClusterNbSim;
//	private final HashSet<Integer>[][] buckets;
	private final PriorityBlockingQueue<Bucket> nextCluster;
	
	
//	public HybridRunnable(ParametersHybrid params, LinkedList<KNNGraph> knngs, int loop, Set<Integer>[][] buckets, Dataset dataset, int nb_hash, int nb_bits) {
//	public HybridRunnable(ParametersHybrid_MH params, LinkedList<KNNGraph> knngs, LinkedList<Set<Integer>> knn_to_compute, Dataset dataset) {
//	public HybridRunnable(ParametersHybrid_MH params, LinkedList<KNNGraph> knngs, int loop, LinkedList<Set<Integer>> knn_to_compute, Dataset dataset) {
//	public HybridRunnable(ParametersHybrid_MH params, LinkedList<KNNGraph> knngs, HashMap<Long,ConcurrentLinkedQueue<IntIntPair>> nbSimToID, PriorityBlockingQueue<Long> nextClusterNbSim, HashSet<Integer>[][] buckets, Dataset dataset) {
	public HybridRunnable(ParametersHybrid_MH_rec params, LinkedList<KNNGraph> knngs, PriorityBlockingQueue<Bucket> nextCluster, Dataset dataset) {
		this.knngs = knngs;
		this.params = params;
//		this.loop = loop;
//		this.buckets = buckets;
//		this.knn_to_compute = knn_to_compute;
//		this.nbSimToID=nbSimToID;
//		this.nextClusterNbSim=nextClusterNbSim;
//		this.buckets=buckets;
		this.nextCluster=nextCluster;
		this.dataset = dataset;
//		this.nb_hash = nb_hash;
//		this.nb_bits = nb_bits;
	}


	@Override
	public void run() {
		
//		long start = System.currentTimeMillis();
//		String s = "";
//		long sum=0;
//		long sum_square=0;
//		int max=0;
		
//		int nb_knng_computed = 0;
//		int aux;
//		for(int i=0; i<loop; i++) {
//			aux =(nb_bits * nb_hash) / params.nb_proc();
//			if ((params.nb_proc() % params.nb_proc()) > (params.nb_proc()  - (i+1))) {
//				aux++;
//			}
//			nb_knng_computed = nb_knng_computed + aux;
//		}
		
//		int nb_knng_to_compute  = (nb_bits * nb_hash) / params.nb_proc();
//		if((params.nb_proc() % params.nb_proc()) > (params.nb_proc()  - (loop+1))) {
//			nb_knng_to_compute++;
//		}
//		int nb_knng_to_compute  = knn_to_compute.size();
//		int index_i;
//		int index_j;
		
//		Set<Integer> user_aux;
		Integer[] users_;
		int[] users;
		int index;
		
		Dataset local_dataset;
		KNNGraph knng;

		double sim;
		
//		Long nbSimToCompute;
//		IntIntPair iip;
		Set<Integer> user_aux;
		Bucket bucket;

//		@SuppressWarnings("unchecked")
//		Set<Integer>[] knn_ = (Set<Integer>[]) new Set[knn_to_compute.size()];
//		knn_to_compute.toArray(knn_);
		
//		for (Set<Integer> user_aux: knn_) {
//		for (int iter=0; iter < nb_knng_to_compute; iter++) {
//		for (Set<Integer> user_aux: knn_to_compute) {
//		nbSimToCompute=nextClusterNbSim.poll()
//		while ((nbSimToCompute=nextClusterNbSim.poll())!=null) {
		while ((bucket=nextCluster.poll())!=null) {

			user_aux=bucket.get_users();
			
//			iip=(nbSimToID.get(nbSimToCompute)).poll();
//			if (iip==null) {
//				break;
//			}
//			
//			user_aux=buckets[iip.first_element][iip.second_element];
			
			//Setting the indexes of the bucket
//			index_i = nb_knng_computed / params.nb_bits();
//			index_j = nb_knng_computed % params.nb_bits();
//			nb_knng_computed++;
//			if(index_j >= params.nb_bits()) {
//				break;
//			}

			
			
			//Getting the corresponding users
//			user_aux = buckets[index_i][index_j];
			users_ = new Integer[user_aux.size()];
			users = new int[user_aux.size()];
			user_aux.toArray(users_);
			index = 0;
			for(int user: users_) {
				users[index] = user;
				index++;
			}
			
//			s = s + users.length + " ";
//			sum = sum + users.length;
//			if(10*params.k()*params.k()<users.length) {
//				sum_square = sum_square + ((long) 5*params.k()*params.k()*((long)users.length));
//			}
//			else {
//				sum_square = sum_square + ((long) users.length-1)*((long) users.length/2);
//			}
//			if(users.length > max) {
//				max = users.length;
//			}
			
			//Creating the corresponding KNNGraph
			knng = new KNNGraph(params.k(),users.length,params.dataset().getInitValue()-1);
			
			//Getting the corresponding dataset => needed?
			local_dataset = dataset.subDataset(users);
//			local_dataset = dataset;
			
			//If 5 * k * k < n then perform Hyrec, else perform BruteForce

			if(5*params.k()*params.k()<users.length) {
//			if(10*params.k()*params.k()<users.length) {
				//HYREC
				ParametersHyrec params_ = new ParametersHyrec(params.k(), params.ParametersToMap().get("Name"), local_dataset, 1);
				params_.set_r(0);
				params_.set_iterations(30);
				params_.set_delta(0.001);
				KNNGraph old_knng = new KNNGraph(params_.k(),users.length,local_dataset.getInitValue()-1);
				HyrecRunnableSamplingInit hyrecInit = new HyrecRunnableSamplingInit(params_, users, knng, old_knng, 0);
				hyrecInit.run();

				
				Counter counter = new Counter();
				for (int iter_hyrec = 0; iter_hyrec < params_.iterations(); iter_hyrec++) {
					counter.init();
					HyrecRunnable hyrecRun = new HyrecRunnable(params_, users, knng, old_knng, counter, 0);
					hyrecRun.run();
					if(counter.getValue() < params_.delta() * params_.k() * users.length) {
						break;
					}
					old_knng = knng.clone();
				}
			}
			else { //BRUTE FORCE
				for(int user_id: users) {
					knng.put(user_id, new KNN(params.k(), params.dataset().getInitValue()-1));
				}

				for(int user_id: users) {
					for(int user: users) {
						if(user < user_id) {
							sim = local_dataset.sim(user_id, user);
							knng.put(user_id,user,sim);
							knng.put(user, user_id,sim);
						}
					}
				}
//				knngs.add(knng);
			}
			knngs.add(knng);
		}
		
//		try {
//			IO.toFile(s, "./Files/graphs/", "essai_proc_nb_"+loop);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		System.out.println("proc: " + loop + " comp time: " + (System.currentTimeMillis()-start));
//		System.out.println("proc: " + loop + " nb_KNN: " + knn_to_compute.size());
//		System.out.println("proc: " + loop + " max: " + max);
//		System.out.println("proc: " + loop + " sum: " + sum);
//		System.out.println("proc: " + loop + " sum_square: " + sum_square);
//		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

}
