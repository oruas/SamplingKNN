package algo;


import parameters.Parameters;
import parameters.ParametersStats;
//import util.KNN;
import util.KNNGraph;

public interface Algo {

	public KNNGraph doKNN();
	public KNNGraph doKNN(KNNGraph anng);
	public KNNGraph doKNN(int[] users);
//	public Map<Integer, KNN> doKNN();
//	public Map<Integer, KNN> doKNN(Map<Integer, KNN> ann);
//	public Map<Integer, IntDoublePairHeap> doKNN(Dataset dataset, int k);
//	public Map<Integer, IntDoublePairHeap> doKNN(Dataset dataset, int k, Map<Integer, IntDoublePairHeap> ann);
//	public void SetParameters(Parameters params);
	public Parameters GetParameters();
	public ParametersStats GetStatsParameters();
}
