package algo.LSH_fast_hash;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import dataset.Dataset;
import parameters.ParametersLSH_fast_hash;
import util.Counter;
import util.KNNGraph;
import util.Permutations1024;

public final class LSHRunnableANNScan implements Runnable{

	private final KNNGraph knng;
	private final KNNGraph anng;
	private final int[] users;
	private final ParametersLSH_fast_hash params;
	private final int loop;
	private final Counter counter;
	private final HashMap<Integer,Set<Integer>>[] buckets;
	private final Permutations1024[] permutations;
	private final Dataset dataset;


	public LSHRunnableANNScan(ParametersLSH_fast_hash params, KNNGraph knng, KNNGraph anng, int[] users, Counter counter, int loop, HashMap<Integer,Set<Integer>>[] buckets, Permutations1024[] permutations, Dataset dataset) {
		this.knng = knng;
		this.anng = anng;
		this.users = users;
		this.params = params;
		this.loop = loop;
		this.counter = counter;
		this.permutations = permutations;
		this.buckets = buckets;
		this.dataset = dataset;
	}


	@Override
	public void run() {

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		double sim;
		HashSet<Integer> candidates = new HashSet<Integer>();
		HashSet<Integer> emptyset = new HashSet<Integer>();
		int hash;

		int count = 0;
		for(int user_id: user_ids) {

			candidates.clear();
			for(int candidate:anng.get_neighbors_ids(user_id)) {
				candidates.add(candidate);
			}
			for(int i = 0; i < permutations.length; i++) {
				hash = permutations[i].getHash(params.dataset().getRatedItems(user_id));
				candidates.addAll(buckets[i].getOrDefault(hash,emptyset));
			}
			

			for(int user: candidates) {
				if(user < user_id) {
					sim = dataset.sim(user_id, user);
					knng.put(user, user_id,sim);
					knng.put(user_id, user,sim);
					count++;
				}
			}
		}
		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

}
