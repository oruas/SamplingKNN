package algo.LSH_fast_hash;

import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import parameters.ParametersLSH_fast_hash;
import util.Permutations1024;

public final class LSHFillingBucketsRunnable implements Runnable{

	private final int[] users;
	private final ParametersLSH_fast_hash params;
	private final int loop;
	private final HashMap<Integer,Set<Integer>> buckets;
	private final Permutations1024 permutation;
	
	
	public LSHFillingBucketsRunnable(ParametersLSH_fast_hash params, HashMap<Integer,Set<Integer>> buckets, Permutations1024 permutation, int[] users, int loop) {
		this.buckets = buckets;
		this.permutation = permutation;
		this.users = users;
		this.params = params;
		this.loop = loop;
	}


	@Override
	public void run() {

//		int[] total_users = util.SetToArray.setToArray(params.dataset().getUsers());
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		for(int user: user_ids) {
			int bucket_id = permutation.getHash(params.datasetHash().getRatedItems(user));
			if(buckets.get(bucket_id)==null) {
				Set<Integer> set = ConcurrentHashMap.newKeySet();
				buckets.put(bucket_id, set);
			}
//			System.out.println(bucket_id);
//			System.out.println(buckets.get(bucket_id));
//			System.out.println(user);
			buckets.get(bucket_id).add(user);
//			System.out.println(buckets.get(bucket_id));
		}
		
		
	}

}
