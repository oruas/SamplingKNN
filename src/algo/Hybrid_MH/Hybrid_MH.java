package algo.Hybrid_MH;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersHybrid_MH;
import parameters.ParametersStats;
import util.Counter;
import util.CounterLong;
import util.HeapIntInt;
import util.HeapSetInteger;
import util.IntDoublePair;
import util.IntIntSetInt;
import util.KNN;
import util.KNNGraph;

public final class Hybrid_MH implements Algo {

	/*
	 * Hybrid algorithm with Multiple Hash
	 * The difference with before is that we try more hash functions and we keep the best ones
	 */
	
	private final ParametersHybrid_MH params;
	private final ParametersStats paramStats;
	private final int nb_bits;
	
	public Hybrid_MH(ParametersHybrid_MH params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
		this.nb_bits = params.nb_bits();
	}

	@Override
	public KNNGraph doKNN(){
		
//		int nb_bits = 1024;
		
		long start = 0;
		long start_=0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		if(ParametersStats.measure_scanrate) {
			paramStats.init_time_proc(params.nb_proc());
			start_ = start;
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

//		long start_LSH = System.currentTimeMillis();

		@SuppressWarnings("unchecked")
		LinkedList<KNNGraph>[] knngs = (LinkedList<KNNGraph>[]) new LinkedList[params.nb_proc()];
		

		
		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets_try = (HashSet<Integer>[][]) new HashSet[params.nb_hash_try()][nb_bits];
		for(int i = 0; i < params.nb_hash_try(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets_try[i][j] = new HashSet<Integer>();
			}
		}
		

		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_creating_bucket(System.currentTimeMillis()-start_);
			start_ = System.currentTimeMillis();
		}
		
//		System.out.println("Creation of buckets: " + (System.currentTimeMillis() - start_LSH) + "ms.");
//		start_LSH = System.currentTimeMillis();
		
		
		
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		
//		long bucket_start = System.currentTimeMillis();
		//To create a synchronized set, use ConcurrentHashMap.newKeySet();
		Set<Integer> user_aux = params.dataset().getUsers();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			users[index] = user;
			index++;
		}
		
		
			
		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash_try(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets_try[permutation][bucket_id].add(user);
				
//				buckets[permutation][bucket_id].add(user);
			}
		}
//		System.out.println("filling bucket in:");
//		System.out.println(System.currentTimeMillis() - bucket_start);

//		System.out.println("Filling buckets: " + (System.currentTimeMillis() - start_LSH) + "ms.");
//		start_LSH = System.currentTimeMillis();
		


		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new HashSet[params.nb_hash()][nb_bits];
		KNN min_buckets = new KNN(params.nb_hash());
		
		for(int i = 0; i < params.nb_hash_try(); i++) {
			int max = 0;
			for(int j=0; j < nb_bits; j++) {
				max = Math.max(max, buckets_try[i][j].size());
			}
			min_buckets.add(new IntDoublePair(i, (double) (users.length - max)));
		}
		int j_=0;
		for(int i:min_buckets.itemArray()) {
			buckets[j_]=buckets_try[i];
			j_++;
		}
		buckets_try=null;
		
		
		
		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_filling_bucket(System.currentTimeMillis()-start_);
			start_ = System.currentTimeMillis();
		}
		
		
		/*
		 *DISTRIBUTION OF THE BUCKETS TO EACH PROC
		 */

		@SuppressWarnings("unchecked")
		LinkedList<Set<Integer>>[] distribution = (LinkedList<Set<Integer>>[]) new LinkedList[params.nb_proc()];

		HeapSetInteger heap_set = new HeapSetInteger(params.nb_hash() * params.nb_proc());
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				heap_set.add(new IntIntSetInt(i,j,buckets[i][j]));
			}
		}
		HeapIntInt heap_add = new HeapIntInt(params.nb_proc());
//		int set_added=0;
		int proc_id;
		
		for(int i=0; i<params.nb_proc();i++) {
			distribution[i] = new LinkedList<Set<Integer>>();
		}
		

		long score;
		for(IntIntSetInt iisi: heap_set.toArray()) {
			buckets[iisi.index_i][iisi.index_j] = null;
			proc_id = heap_add.get_id();
			distribution[proc_id].add(iisi.set);
//			heap_add.add(iisi.score);
			score = iisi.score;
			if(5*params.k()*params.k()<score) {
				score = ((long) 5*params.k()*params.k()*((long)score/2));
			}
			else {
				score = ((long) score-1)*((long) score/2);
			}
			heap_add.add(iisi.score);
		}
		Set<Integer> set_aux;
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				set_aux = buckets[i][j];
				if(set_aux != null && set_aux.size()>1) {
					proc_id = heap_add.get_id();
					distribution[proc_id].add(set_aux);
					score = set_aux.size();
					if(5*params.k()*params.k()<score) {
						score = ((long) 5*params.k()*params.k()*((long)score/2));
					}
					else {
						score = ((long) score-1)*((long) score/2);
					}
					heap_add.add(score);
//					heap_add.add(set_aux.size());
				}
			}
		}
		

		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_distribution_bucket(System.currentTimeMillis()-start_);
//			start_ = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE KNN
		 */
		
		
//		bucket_start = System.currentTimeMillis();
		
		Counter[] counters = new Counter[params.nb_proc()];

		CounterLong[] counters_time = new CounterLong[params.nb_proc()];
		
		
		

		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			knngs[i] = new LinkedList<KNNGraph>();
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], counters_time[i], distribution[i], params.dataset()));
//				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], i, buckets, params.dataset2(), params.nb_hash(), nb_bits));
			}
			else {
				threads[i] = new Thread(new HybridRunnable(params, knngs[i], distribution[i], params.dataset()));
//				threads[i] = new Thread(new HybridRunnable(params, knngs[i], i, distribution[i], params.dataset2()));
//				threads[i] = new Thread(new HybridRunnable(params, knngs[i], i, buckets, params.dataset2(), params.nb_hash(), nb_bits));
//				System.out.println("ho ho ho");
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_scanrate) {
//			paramStats.set_time_distribution_bucket(System.currentTimeMillis()-start_);
			start_ = System.currentTimeMillis();
		}
//		System.out.println("Doing KNNG: " + (System.currentTimeMillis() - start_LSH)/1000 + "s.");
//		start_LSH = System.currentTimeMillis();

//		System.out.println("doing knn in:");
//		System.out.println(System.currentTimeMillis() - bucket_start);
		
		HybridMeltKNNG[] melt = new HybridMeltKNNG[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			melt[i] = new HybridMeltKNNG(params, knngs[i]);
			threads[i] = new Thread(melt[i]);
			threads[i].start();
		}

		LinkedList<KNNGraph> knngs_ = new LinkedList<KNNGraph>();
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
				knngs_.add(melt[i].getKNNG());
//				System.out.println("hé hé hé");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		melt[0] = new HybridMeltKNNG(params, knngs_);
		melt[0].run();
		

		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
//			paramStats.set_time_merging_bucket(end-start_);
		}
		
		if(ParametersStats.measure_scanrate) {
//			System.out.println(params.nb_proc());
			paramStats.set_time_merging_bucket(System.currentTimeMillis()-start_);
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
				paramStats.set_time_proc(i, counters_time[i].getValue());
			}
			paramStats.increase_scanrate(scanrate);
		}

		
		return melt[0].getKNNG();
	}

	
	
	
	
	
	
	
	
	
	
	
	/*
	 * doKNN(int[] users) will do the KNN of each user in users, using the users of ALL the dataset
	 * */
	
	@Override
	public KNNGraph doKNN(int[] users){
		
//		int nb_bits = 1024;
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

//		long start_LSH = System.currentTimeMillis();

		@SuppressWarnings("unchecked")
		LinkedList<KNNGraph>[] knngs = (LinkedList<KNNGraph>[]) new Object[params.nb_proc()];
//		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		

		
		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets_try = (HashSet<Integer>[][]) new HashSet[params.nb_hash_try()][nb_bits];
		for(int i = 0; i < params.nb_hash_try(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets_try[i][j] = new HashSet<Integer>();
			}
		}
		
		
		
		
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
			
		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash_try(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets_try[permutation][bucket_id].add(user);
//				buckets[permutation][bucket_id].add(user);
			}
		}
		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new Object[params.nb_hash()][nb_bits];
		KNN min_buckets = new KNN(params.nb_hash());
		
		for(int i = 0; i < params.nb_hash_try(); i++) {
			int max = 0;
			for(int j=0; j < nb_bits; j++) {
				max = Math.max(max, buckets_try[i][j].size());
			}
			min_buckets.add(new IntDoublePair(i, (double) (users.length - max)));
		}
		int j_=0;
		for(int i:min_buckets.itemArray()) {
			buckets[j_]=buckets_try[i];
			j_++;
		}
		buckets_try=null;

		/*
		 *DISTRIBUTION OF THE BUCKETS TO EACH PROC
		 */

		@SuppressWarnings("unchecked")
		LinkedList<Set<Integer>>[] distribution = (LinkedList<Set<Integer>>[]) new LinkedList[params.nb_proc()];

		HeapSetInteger heap_set = new HeapSetInteger(params.nb_hash() * params.nb_proc());
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				heap_set.add(new IntIntSetInt(i,j,buckets[i][j]));
			}
		}
		HeapIntInt heap_add = new HeapIntInt(params.nb_proc());
//		int set_added=0;
		int proc_id;
		
		for(int i=0; i<params.nb_proc();i++) {
			distribution[i] = new LinkedList<Set<Integer>>();
		}
		
		
		for(IntIntSetInt iisi: heap_set.toArray()) {
			buckets[iisi.index_i][iisi.index_j] = null;
			proc_id = heap_add.get_id();
			distribution[proc_id].add(iisi.set);
			heap_add.add(iisi.score); 
		}
		Set<Integer> set_aux;
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				set_aux = buckets[i][j];
				if(set_aux != null && set_aux.size()>1) {
					proc_id = heap_add.get_id();
					distribution[proc_id].add(set_aux);
					heap_add.add(set_aux.size());
				}
			}
		}
		
		/*
		 * CREATION OF THE KNN
		 */
		
		
//		bucket_start = System.currentTimeMillis();
		
		Counter[] counters = new Counter[params.nb_proc()];
		CounterLong[] counters_time = new CounterLong[params.nb_proc()];
		
		
		

		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], counters_time[i], distribution[i], params.dataset()));
//				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], distribution[i], params.dataset2()));
//				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], i, buckets, params.dataset2(), params.nb_hash(), nb_bits));
			}
			else {
				threads[i] = new Thread(new HybridRunnable(params, knngs[i], distribution[i], params.dataset()));
//				threads[i] = new Thread(new HybridRunnable(params, knngs[i], i, distribution[i], params.dataset2()));
//				threads[i] = new Thread(new HybridRunnable(params, knngs[i], i, buckets, params.dataset2(), params.nb_hash(), nb_bits));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		HybridMeltKNNG[] melt = new HybridMeltKNNG[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			melt[i] = new HybridMeltKNNG(params, knngs[i]);
			threads[i] = new Thread(melt[i]);
			threads[i].start();
		}

		LinkedList<KNNGraph> knngs_ = new LinkedList<KNNGraph>();
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
				knngs_.add(melt[i].getKNNG());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		melt[0] = new HybridMeltKNNG(params, knngs_);
		melt[0].run();

		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		
		return melt[0].getKNNG();
	}

	
	
	
	
	
	
	
	@Override
	public KNNGraph doKNN(KNNGraph anng) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

//		long start_LSH = System.currentTimeMillis();

		@SuppressWarnings("unchecked")
		LinkedList<KNNGraph>[] knngs = (LinkedList<KNNGraph>[]) new Object[params.nb_proc()];
		

		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets_try = (HashSet<Integer>[][]) new HashSet[params.nb_hash_try()][nb_bits];
		for(int i = 0; i < params.nb_hash_try(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets_try[i][j] = new HashSet<Integer>();
			}
		}
		
		
//		System.out.println("Creation of buckets: " + (System.currentTimeMillis() - start_LSH) + "ms.");
//		start_LSH = System.currentTimeMillis();
		
		
		
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		
//		long bucket_start = System.currentTimeMillis();
		//To create a synchronized set, use ConcurrentHashMap.newKeySet();
		Set<Integer> user_aux = anng.get_users();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			users[index] = user;
			index++;
		}
			
		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash_try(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				
				buckets_try[permutation][bucket_id].add(user);
//				buckets[permutation][bucket_id].add(user);
			}
		}
//		System.out.println("filling bucket in:");
//		System.out.println(System.currentTimeMillis() - bucket_start);

//		System.out.println("Filling buckets: " + (System.currentTimeMillis() - start_LSH) + "ms.");
//		start_LSH = System.currentTimeMillis();

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new Object[params.nb_hash()][nb_bits];
		KNN min_buckets = new KNN(params.nb_hash());
		
		for(int i = 0; i < params.nb_hash_try(); i++) {
			int max = 0;
			for(int j=0; j < nb_bits; j++) {
				max = Math.max(max, buckets_try[i][j].size());
			}
			min_buckets.add(new IntDoublePair(i, (double) (users.length - max)));
		}
		int j_=0;
		for(int i:min_buckets.itemArray()) {
			buckets[j_]=buckets_try[i];
			j_++;
		}
		buckets_try=null;
		
		

		/*
		 *DISTRIBUTION OF THE BUCKETS TO EACH PROC
		 */

		@SuppressWarnings("unchecked")
		LinkedList<Set<Integer>>[] distribution = (LinkedList<Set<Integer>>[]) new LinkedList[params.nb_proc()];

		HeapSetInteger heap_set = new HeapSetInteger(params.nb_hash() * params.nb_proc());
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				heap_set.add(new IntIntSetInt(i,j,buckets[i][j]));
			}
		}
		HeapIntInt heap_add = new HeapIntInt(params.nb_proc());
//		int set_added=0;
		int proc_id;
		
		for(int i=0; i<params.nb_proc();i++) {
			distribution[i] = new LinkedList<Set<Integer>>();
		}
		
		
		for(IntIntSetInt iisi: heap_set.toArray()) {
			buckets[iisi.index_i][iisi.index_j] = null;
			proc_id = heap_add.get_id();
			distribution[proc_id].add(iisi.set);
			heap_add.add(iisi.score);
		}
		Set<Integer> set_aux;
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				set_aux = buckets[i][j];
				if(set_aux != null && set_aux.size()>1) {
					proc_id = heap_add.get_id();
					distribution[proc_id].add(set_aux);
					heap_add.add(set_aux.size());
				}
			}
		}
		
		
		/*
		 * CREATION OF THE KNN
		 */
		
		
//		bucket_start = System.currentTimeMillis();
		
		Counter[] counters = new Counter[params.nb_proc()];
		CounterLong[] counters_time = new CounterLong[params.nb_proc()];
		
		
		

		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], counters_time[i], distribution[i], params.dataset()));
//				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], distribution[i], params.dataset2()));
//				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], i, buckets, params.dataset2(), params.nb_hash(), nb_bits));
			}
			else {
				threads[i] = new Thread(new HybridRunnable(params, knngs[i], distribution[i], params.dataset()));
//				threads[i] = new Thread(new HybridRunnable(params, knngs[i], i, distribution[i], params.dataset2()));
//				threads[i] = new Thread(new HybridRunnable(params, knngs[i], i, buckets, params.dataset2(), params.nb_hash(), nb_bits));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

//		System.out.println("Doing KNNG: " + (System.currentTimeMillis() - start_LSH)/1000 + "s.");
//		start_LSH = System.currentTimeMillis();

//		System.out.println("doing knn in:");
//		System.out.println(System.currentTimeMillis() - bucket_start);
		
		HybridMeltKNNG[] melt = new HybridMeltKNNG[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			melt[i] = new HybridMeltKNNG(params, knngs[i]);
			threads[i] = new Thread(melt[i]);
			threads[i].start();
		}

		LinkedList<KNNGraph> knngs_ = new LinkedList<KNNGraph>();
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
				knngs_.add(melt[i].getKNNG());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		melt[0] = new HybridMeltKNNG(params, knngs_);
		melt[0].run();

		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		
		return melt[0].getKNNG();
	}



//	public void SetParameters(Parameters params) {
//		this.params = (ParametersBruteForce) params;
//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}
}
