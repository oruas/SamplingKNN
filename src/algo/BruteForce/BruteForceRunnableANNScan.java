package algo.BruteForce;

import parameters.ParametersBruteForce;
import util.Counter;
import util.KNNGraph;

public final class BruteForceRunnableANNScan implements Runnable{

	private final KNNGraph knng;
	private final KNNGraph anng;
	private final int[] users;
	private final ParametersBruteForce params;
	private final int loop;
	private final Counter counter;


	public BruteForceRunnableANNScan(ParametersBruteForce params, KNNGraph knng, KNNGraph anng, int[] users, Counter counter, int loop) {
		this.knng = knng;
		this.anng = anng;
		this.users = users;
		this.params = params;
		this.loop = loop;
		this.counter = counter;
	}


	@Override
	public void run() {

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);

		int count = 0;
		for(int user_id: user_ids) {
			for(int user: anng.get_neighbors_ids(user_id)) {
				if(user != -1) {

					
					knng.put(user_id, user, params.dataset().sim(user_id, user));
					count++;
				}
			}
		}
		counter.inc(count);
	}

}
