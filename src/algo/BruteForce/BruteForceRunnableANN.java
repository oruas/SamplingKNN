package algo.BruteForce;

import parameters.ParametersBruteForce;
import util.KNNGraph;

public final class BruteForceRunnableANN implements Runnable{

	private final KNNGraph knng;
	private final KNNGraph anng;
	private final int[] users;
	private final ParametersBruteForce params;
	private final int loop;


	public BruteForceRunnableANN(ParametersBruteForce params, KNNGraph knng, KNNGraph anng, int[] users, int loop) {
		this.knng = knng;
		this.anng = anng;
		this.users = users;
		this.params = params;
		this.loop = loop;
	}


	@Override
	public void run() {

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);

		for(int user_id: user_ids) {
			for(int user: anng.get_neighbors_ids(user_id)) {
				if(user != -1) {
					
					knng.put(user_id, user, params.dataset().sim(user_id, user));
				}
			}
		}
	}

}
