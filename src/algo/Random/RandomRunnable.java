package algo.Random;

import java.util.HashSet;
import java.util.Set;
import java.util.Random;

import parameters.ParametersBruteForce;
//import util.IntDoublePair;
import util.KNNGraph;

public final class RandomRunnable implements Runnable {

	private final ParametersBruteForce params;
	private final int loop;
	private final int[] users;
	private final KNNGraph knng;
	
	
	public RandomRunnable(ParametersBruteForce params, KNNGraph knng, int[] users, int loop) {
		this.params = params;
		this.knng = knng;
		this.users = users;
		this.loop = loop;
	}
	
	@Override
	public void run() {
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		int[] total_users = util.SetToArray.setToArray(params.dataset().getUsers());
		
		Random randomGenerator = new Random();

		Set<Integer> neighbors;
		int random_user;
		for(int user: user_ids) {
			neighbors = new HashSet<Integer>();
			while (neighbors.size() < params.k()) {
				random_user = randomGenerator.nextInt(total_users.length);
				if(random_user != user) {
					neighbors.add(total_users[random_user]);
				}
			}
			for(int neighbor:neighbors) {
//				knng.put(user, new IntDoublePair(neighbor, params.dataset().sim(user, neighbor)));
				knng.put(user, neighbor, params.dataset().sim(user, neighbor));
//				if(ParametersStats.measure_scanrate) {
//					paramStats.inc();
//				}
//				knng.get(user).add(new IntDoublePair(neighbor, params.dataset().sim(user, neighbor)));
			}
		}
	}

}
