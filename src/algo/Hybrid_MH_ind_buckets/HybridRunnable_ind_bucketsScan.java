package algo.Hybrid_MH_ind_buckets;

import java.util.HashSet;

import dataset.Dataset;
import parameters.ParametersHybrid_MH;
import util.Counter;
import util.CounterLong;
import util.KNNGraph;

public final class HybridRunnable_ind_bucketsScan implements Runnable{

	private final ParametersHybrid_MH params;
	private final int loop;
	private final int[] users;
	private final Dataset dataset;
	private final KNNGraph knng;
	private final int[] permutations;
	private final HashSet<Integer>[][] buckets;
	

	private final Counter counter_scanrate;
	private final CounterLong counter_time;
	
	public HybridRunnable_ind_bucketsScan(ParametersHybrid_MH params,  KNNGraph knng, int[] users, int loop, HashSet<Integer>[][] buckets, int[] permutations, Dataset dataset, Counter counter, CounterLong counter_time) {
		this.knng = knng;
		this.users = users;
		this.params = params;
		this.loop = loop;
		this.permutations = permutations;
		this.buckets = buckets;
		this.dataset = dataset;
		
		this.counter_scanrate = counter;
		this.counter_time = counter_time;
	}


	@Override
	public void run() {

		long start=System.currentTimeMillis();
		

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		double sim;
		HashSet<Integer> candidates = new HashSet<Integer>();
		int hash;
		int bucket_id;
		
		for(int user_id: user_ids) {

			candidates.clear();
			for(int i:permutations) {
				bucket_id = params.nb_bits();
				for(int item : params.datasetHash().getRatedItems(user_id)) {
					hash = util.HashFunctions.get_hash(item, i, params.nb_bits());
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				candidates.addAll(buckets[i][bucket_id]);
			}
			
			
			for(int user: candidates) {
				if(user < user_id) {
					sim = dataset.sim(user_id, user);
					knng.put(user_id,user,sim);
					knng.put(user, user_id,sim);
					counter_scanrate.inc();
				}
			}
		}
		counter_time.inc(System.currentTimeMillis()-start);
		
	}

}
