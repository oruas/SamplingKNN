package algo.ClusterAndConquer_ind_buckets;

import java.util.HashSet;
import java.util.Set;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersHybrid_MH;
import parameters.ParametersStats;
import util.Counter;
import util.CounterLong;
import util.IntDoublePair;
import util.KNN;
import util.KNNGraph;

public final class ClusterAndConquer_ind_buckets implements Algo {

	/*
	 * Hybrid algorithm with Multiple Hash
	 * The difference with Hybrid_HM is that KNNG is computed by user and not by bucket, as in LSH
	 */
	
	private final ParametersHybrid_MH params;
	private final ParametersStats paramStats;
	private final int nb_bits;
	
	public ClusterAndConquer_ind_buckets(ParametersHybrid_MH params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
		this.nb_bits = params.nb_bits();
	}

	@Override
	public KNNGraph doKNN(){
		
//		int nb_bits = 1024;
		
		long start = 0;
		long start_=0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		if(ParametersStats.measure_scanrate) {
			paramStats.init_time_proc(params.nb_proc());
			start_ = start;
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

		

		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		
		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets_try = (HashSet<Integer>[][]) new HashSet[params.nb_hash_try()][nb_bits];
		for(int i = 0; i < params.nb_hash_try(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets_try[i][j] = new HashSet<Integer>();
			}
		}
		

		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_creating_bucket(System.currentTimeMillis()-start_);
			start_ = System.currentTimeMillis();
		}
		
		
		
		
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		
//		long bucket_start = System.currentTimeMillis();
		//To create a synchronized set, use ConcurrentHashMap.newKeySet();
		Set<Integer> user_aux = params.dataset().getUsers();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
			users[index] = user;
			index++;
		}
		
		
			
		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash_try(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets_try[permutation][bucket_id].add(user);
				
			}
		}
		


		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new HashSet[params.nb_hash()][nb_bits];
		KNN min_buckets = new KNN(params.nb_hash());
		int[] hash_functions=new int[params.nb_hash()];
		
		for(int i = 0; i < params.nb_hash_try(); i++) {
			int max = 0;
			for(int j=0; j < nb_bits; j++) {
				max = Math.max(max, buckets_try[i][j].size());
			}
			min_buckets.add(new IntDoublePair(i, (double) (users.length - max)));
		}
		int j_=0;
		for(int i:min_buckets.itemArray()) {
			buckets[j_]=buckets_try[i];
			hash_functions[j_] = i;
			j_++;
		}
		buckets_try=null;
		
		
		
		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_filling_bucket(System.currentTimeMillis()-start_);
			start_ = System.currentTimeMillis();
		}
		
		
		

		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_distribution_bucket(System.currentTimeMillis()-start_);
		}
		
		/*
		 * CREATION OF THE KNN
		 */
		
		
		
		Counter[] counters = new Counter[params.nb_proc()];

		CounterLong[] counters_time = new CounterLong[params.nb_proc()];
		
		
		

		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnable_ind_bucketsScan(params,  knng, users, i, buckets, hash_functions, params.dataset(), counters[i], counters_time[i]));
			}
			else {
				threads[i] = new Thread(new HybridRunnable_ind_buckets(params,  knng, users, i, buckets, hash_functions, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_scanrate) {
			start_ = System.currentTimeMillis();
		}
		

		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
//			paramStats.set_time_merging_bucket(end-start_);
		}
		
		if(ParametersStats.measure_scanrate) {
//			System.out.println(params.nb_proc());
			paramStats.set_time_merging_bucket(System.currentTimeMillis()-start_);
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
				paramStats.set_time_proc(i, counters_time[i].getValue());
			}
			paramStats.increase_scanrate(scanrate);
		}

		
		return knng;
	}

	
	
	
	
	
	
	
	
	
	
	
	/*
	 * doKNN(int[] users) will do the KNN of each user in users, using the users of ALL the dataset
	 * */
	
	@Override
	public KNNGraph doKNN(int[] users){
		
//		int nb_bits = 1024;
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */
		

		
		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets_try = (HashSet<Integer>[][]) new HashSet[params.nb_hash_try()][nb_bits];
		for(int i = 0; i < params.nb_hash_try(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets_try[i][j] = new HashSet<Integer>();
			}
		}
		
		
		
		
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
			
		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash_try(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets_try[permutation][bucket_id].add(user);
//				buckets[permutation][bucket_id].add(user);
			}
		}
		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new Object[params.nb_hash()][nb_bits];
		KNN min_buckets = new KNN(params.nb_hash());
		int[] hash_functions=new int[params.nb_hash()];
		
		for(int i = 0; i < params.nb_hash_try(); i++) {
			int max = 0;
			for(int j=0; j < nb_bits; j++) {
				max = Math.max(max, buckets_try[i][j].size());
			}
			min_buckets.add(new IntDoublePair(i, (double) (users.length - max)));
		}
		int j_=0;
		for(int i:min_buckets.itemArray()) {
			buckets[j_]=buckets_try[i];
			hash_functions[j_] = i;
			j_++;
		}
		buckets_try=null;

		
		/*
		 * CREATION OF THE KNN
		 */
		
		
//		bucket_start = System.currentTimeMillis();


		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		for(int user: users) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
		}
		Counter[] counters = new Counter[params.nb_proc()];
		CounterLong[] counters_time = new CounterLong[params.nb_proc()];
		
		
		

		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnable_ind_bucketsScan(params,  knng, users, i, buckets, hash_functions, params.dataset(), counters[i], counters_time[i]));
			}
			else {
				threads[i] = new Thread(new HybridRunnable_ind_buckets(params,  knng, users, i, buckets, hash_functions, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		
		return knng;
	}

	
	
	
	
	
	
	
	@Override
	public KNNGraph doKNN(KNNGraph anng) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */


		

		

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets_try = (HashSet<Integer>[][]) new HashSet[params.nb_hash_try()][nb_bits];
		for(int i = 0; i < params.nb_hash_try(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets_try[i][j] = new HashSet<Integer>();
			}
		}
		
		
		
		
		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		
//		long bucket_start = System.currentTimeMillis();
		//To create a synchronized set, use ConcurrentHashMap.newKeySet();
		Set<Integer> user_aux = anng.get_users();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			users[index] = user;
			index++;
		}
			
		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash_try(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				
				buckets_try[permutation][bucket_id].add(user);
//				buckets[permutation][bucket_id].add(user);
			}
		}
//		System.out.println("filling bucket in:");
//		System.out.println(System.currentTimeMillis() - bucket_start);

//		System.out.println("Filling buckets: " + (System.currentTimeMillis() - start_LSH) + "ms.");
//		start_LSH = System.currentTimeMillis();

		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new Object[params.nb_hash()][nb_bits];
		KNN min_buckets = new KNN(params.nb_hash());
		int[] hash_functions=new int[params.nb_hash()];
		
		for(int i = 0; i < params.nb_hash_try(); i++) {
			int max = 0;
			for(int j=0; j < nb_bits; j++) {
				max = Math.max(max, buckets_try[i][j].size());
			}
			min_buckets.add(new IntDoublePair(i, (double) (users.length - max)));
		}
		int j_=0;
		for(int i:min_buckets.itemArray()) {
			hash_functions[j_] = i;
			buckets[j_]=buckets_try[i];
			j_++;
		}
		buckets_try=null;
		
		
		
		
		/*
		 * CREATION OF THE KNN
		 */
		

		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		for(int user: users) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
		}
//		bucket_start = System.currentTimeMillis();
		
		Counter[] counters = new Counter[params.nb_proc()];
		CounterLong[] counters_time = new CounterLong[params.nb_proc()];
		
		
		

		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnable_ind_bucketsScan(params,  knng, users, i, buckets, hash_functions, params.dataset(), counters[i], counters_time[i]));
			}
			else {
				threads[i] = new Thread(new HybridRunnable_ind_buckets(params,  knng, users, i, buckets, hash_functions, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		
		return knng;
	}



//	public void SetParameters(Parameters params) {
//		this.params = (ParametersBruteForce) params;
//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}
}
