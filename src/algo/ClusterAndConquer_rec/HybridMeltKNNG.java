package algo.ClusterAndConquer_rec;

import java.util.LinkedList;
import java.util.Set;

import parameters.ParametersHybrid_MH_rec;
import util.IntDoublePair;
//import util.KNN;
import util.KNNGraph;

public final class HybridMeltKNNG implements Runnable{

	private final LinkedList<KNNGraph> knngs;
	private final KNNGraph knng;
	
	
	public HybridMeltKNNG(ParametersHybrid_MH_rec params, LinkedList<KNNGraph> knngs) {
		if(knngs !=null && !knngs.isEmpty()) {
			this.knngs = knngs;
			this.knng = knngs.remove();
		}
		else {
			this.knngs = new LinkedList<KNNGraph>();
//			this.knng = null;
			this.knng = new KNNGraph(0, 0, -1);
		}
	}


	@Override
	public void run() {
		if (knng != null) {
			KNNGraph knng_;
			Set<Integer> users = knng.get_users();
			while(!knngs.isEmpty()) {
				knng_ = knngs.remove();
				if(knng_ != null) {
					for(int user : knng_.get_users()) {
						if(!users.contains(user)) {
							knng.init(user, knng_.get_neighbors(user).length, -1);
						}
						for(IntDoublePair idp : knng_.get_neighbors(user)) {
							knng.put(user, idp.integer, idp.score);
						}
					}
				}
			}
		}
		
//		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

	public KNNGraph getKNNG() {
		return knng;
	}
	
}
