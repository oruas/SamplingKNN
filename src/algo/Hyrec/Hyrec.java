package algo.Hyrec;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersHyrec;
import parameters.ParametersStats;
import util.Counter;
import util.KNNGraph;

public final class Hyrec implements Algo{
	
	private final ParametersHyrec params;
	private final ParametersStats paramStats;
	
	public Hyrec(ParametersHyrec params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
	}

	@Override
	public KNNGraph doKNN() {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		Counter[] counters_scanrate = new Counter[params.nb_proc()];
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		KNNGraph old_knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
//		KNNGraph knng = new KNNGraph();
//		KNNGraph old_knng = new KNNGraph();

		int nb_user = params.dataset().getUsers().size();

		Object[] users_aux = params.dataset().getUsers().toArray();
		
		int[] users = new int[nb_user];
		for(int index = 0; index < nb_user; index++) {
			users[index] = (int) users_aux[index];
		}
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new HyrecRunnableSamplingInit(params, paramStats, users, knng, old_knng, i));
//			t.start();
//			threads[i] = t;
			counters[i] = new Counter();
			if(ParametersStats.measure_scanrate) {
				counters_scanrate[i] = new Counter();
				threads[i] = new Thread(new HyrecRunnableSamplingInitScan(params, users, knng, old_knng, counters_scanrate[i], i));
			}
			else {
				threads[i] = new Thread(new HyrecRunnableSamplingInit(params, users, knng, old_knng, i));				
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters_scanrate[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
			paramStats.add_iter();
		}
		for (int iter = 0; iter < params.iterations(); iter++) {

			for(int i = 0; i < params.nb_proc(); i++) {

//				Counter counter = new Counter();
//				Thread t = new Thread(new HyrecRunnable(params, paramStats, users, knng, old_knng, counter, i));
//				t.start();
//				threads[i] = t;
//				counters[i] = counter;

				counters[i].init();
				if(ParametersStats.measure_scanrate) {
					counters_scanrate[i].init();
					threads[i] = new Thread(new HyrecRunnableScan(params, users, knng, old_knng, counters[i], counters_scanrate[i], i));
				}
				else {
					threads[i] = new Thread(new HyrecRunnable(params, users, knng, old_knng, counters[i], i));
				}
				threads[i].start();
				
			}

			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			int nb_changes = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				nb_changes = nb_changes + counters[i].getValue();
			}


			if(ParametersStats.measure_scanrate) {
				long scanrate = 0;
				for(int i = 0; i < params.nb_proc(); i++) {
					scanrate = scanrate + counters_scanrate[i].getValue();
				}
				paramStats.increase_scanrate(scanrate);
				paramStats.add_iter();
				paramStats.add_iter_changes(nb_changes);
			}
//			System.out.println(nb_changes + " out of " + params.delta() * params.k() * nb_user);
			if(nb_changes < params.delta() * params.k() * nb_user) {
				break;
			}
			

			old_knng = knng.clone();
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return knng;
	}

	/*
	 * doKNN(int[] users) will do the KNN of the user in users, using ONLY the users of in users
	 * */
	@Override
	public KNNGraph doKNN(int[] users) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		Counter[] counters_scanrate = new Counter[params.nb_proc()];
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		KNNGraph old_knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
//		KNNGraph knng = new KNNGraph();
//		KNNGraph old_knng = new KNNGraph();

		int nb_user = params.dataset().getUsers().size();

//		Object[] users_aux = params.dataset().getUsers().toArray();
//		
//		int[] users = new int[nb_user];
//		for(int index = 0; index < nb_user; index++) {
//			users[index] = (int) users_aux[index];
//		}
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new HyrecRunnableSamplingInit(params, paramStats, users, knng, old_knng, i));
//			t.start();
//			threads[i] = t;
			counters[i] = new Counter();
			if(ParametersStats.measure_scanrate) {
				counters_scanrate[i] = new Counter();
				threads[i] = new Thread(new HyrecRunnableSamplingInitScan(params, users, knng, old_knng, counters_scanrate[i], i));
			}
			else {
				threads[i] = new Thread(new HyrecRunnableSamplingInit(params, users, knng, old_knng, i));				
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters_scanrate[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
			paramStats.add_iter();
		}
		for (int iter = 0; iter < params.iterations(); iter++) {

			for(int i = 0; i < params.nb_proc(); i++) {

//				Counter counter = new Counter();
//				Thread t = new Thread(new HyrecRunnable(params, paramStats, users, knng, old_knng, counter, i));
//				t.start();
//				threads[i] = t;
//				counters[i] = counter;

				counters[i].init();
				if(ParametersStats.measure_scanrate) {
					counters_scanrate[i].init();
					threads[i] = new Thread(new HyrecRunnableScan(params, users, knng, old_knng, counters[i], counters_scanrate[i], i));
				}
				else {
					threads[i] = new Thread(new HyrecRunnable(params, users, knng, old_knng, counters[i], i));
				}
				threads[i].start();
				
			}

			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			int nb_changes = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				nb_changes = nb_changes + counters[i].getValue();
			}


			if(ParametersStats.measure_scanrate) {
				long scanrate = 0;
				for(int i = 0; i < params.nb_proc(); i++) {
					scanrate = scanrate + counters_scanrate[i].getValue();
				}
				paramStats.increase_scanrate(scanrate);
				paramStats.add_iter();
				paramStats.add_iter_changes(nb_changes);
			}
//			System.out.println(nb_changes + " out of " + params.delta() * params.k() * nb_user);
			if(nb_changes < params.delta() * params.k() * nb_user) {
				break;
			}
			

			old_knng = knng.clone();
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return knng;
	}
	
	
	
	
	
	

	@Override
	public KNNGraph doKNN(KNNGraph ann) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}

		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		KNNGraph old_knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
//		KNNGraph knng =  new KNNGraph();
//		KNNGraph old_knng =  new KNNGraph();
		
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		Counter[] counters_scanrate = new Counter[params.nb_proc()];

		int nb_user = params.dataset().getUsers().size();

//		Object[] users_aux = params.dataset().getUsers().toArray();
		Object[] users_aux = ann.get_users().toArray();
		
		int[] users = new int[nb_user];
		
		for(int index = 0; index < nb_user; index++) {
			users[index] = (int) users_aux[index];
		}
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new HyrecRunnableSamplingInitANN(params, paramStats, users, ann, knng, old_knng, i));
//			t.start();
//			threads[i] = t;
			counters[i] = new Counter();
			if(ParametersStats.measure_scanrate) {
				counters_scanrate[i] = new Counter();
				threads[i] = new Thread(new HyrecRunnableSamplingInitANNScan(params, users, ann, knng, old_knng, counters_scanrate[i], i));
			}
			else {
				threads[i] = new Thread(new HyrecRunnableSamplingInitANN(params, users, ann, knng, old_knng, i));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters_scanrate[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
			paramStats.add_iter();
		}

		for (int iter = 0; iter < params.iterations(); iter++) {
			for(int i = 0; i < params.nb_proc(); i++) {
//				Counter counter = new Counter();
//				Thread t = new Thread(new HyrecRunnable(params, paramStats, users, knng, old_knng, counter, i));
//				t.start();
//				threads[i] = t;
//				counters[i] = counter;

				counters[i].init();
				if(ParametersStats.measure_scanrate) {
					counters_scanrate[i].init();
					threads[i] = new Thread(new HyrecRunnableScan(params, users, knng, old_knng, counters[i], counters_scanrate[i], i));
				}
				else {
					threads[i] = new Thread(new HyrecRunnable(params, users, knng, old_knng, counters[i], i));
				}
				threads[i].start();
			}

			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			int nb_changes = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				nb_changes = nb_changes + counters[i].getValue();
			}
			if(ParametersStats.measure_scanrate) {
				long scanrate = 0;
				for(int i = 0; i < params.nb_proc(); i++) {
					scanrate = scanrate + counters_scanrate[i].getValue();
				}
				paramStats.increase_scanrate(scanrate);
				paramStats.add_iter();
				paramStats.add_iter_changes(nb_changes);
			}
			
			if(nb_changes < params.delta() * params.k() * nb_user) {
				break;
			}

			old_knng = knng.clone();
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return knng;
	}

	
//	public void SetParameters(Parameters params) {
//		this.params = (ParametersHyrec) params;
//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}

}
