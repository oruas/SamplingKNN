package algo.Hyrec;

import parameters.ParametersHyrec;
import util.IntDoublePair;
import util.KNN;
import util.KNNGraph;

public final class HyrecRunnableSamplingInitANN implements Runnable{

	private final int[] users;
	private final KNNGraph knng;
	private final KNNGraph old_knng;
	private final KNNGraph anng;
	private final int loop;
	private final ParametersHyrec params;
	
	public HyrecRunnableSamplingInitANN(ParametersHyrec params, int[] users, KNNGraph anng, KNNGraph knng, KNNGraph old_knng, int loop) {
		this.users = users;
		this.anng = anng;
		this.knng = knng;
		this.old_knng = old_knng;
		this.loop = loop;
		this.params = params;
	}
	
	
	@Override
	public void run() {
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);

		KNN neighbors;
		for(int user: user_ids) {
			neighbors = new KNN(params.k(), params.dataset().getInitValue()-1);
			for(int neighbor: anng.get_neighbors_ids(user)) {
				neighbors.add(new IntDoublePair(neighbor, params.dataset().sim(user, neighbor)));
//				if(ParametersStats.measure_scanrate) {
//					paramStats.inc();
//				}
				
			}
			knng.put(user, neighbors);
			old_knng.put(user, neighbors.clone());
		}
		
	}

}
