package launch;

import java.io.IOException;
import java.util.Map;

import algo.Random.Random;
import io.IO;
import parameters.Parameters;
import parameters.ParametersBruteForce;
import parameters.ParametersStats;
import util.KNNGraph;

public final class LaunchRandom {

	public final static KNNGraph launch(ParametersBruteForce params, ParametersStats paramStats, String Add) throws IOException {
		Map<String,String> map = params.DatasetToMap();
		map.putAll(params.ParametersToMap());
		IO.toFile(Parameters.ParametersToString(map), Add, util.Names.paramFile);
//		IO.toFile(params.DatasetToString(), Add, util.Names.paramDataFile);
//		IO.toFile(params.ParametersToString(), Add, util.Names.paramFile);
		Random algo = new Random(params,paramStats);
		KNNGraph knng = algo.doKNN();
//		IO.KNNGraphtoJSON(knng, params.dataset(), Add, util.Names.knngFile);
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
		if(ParametersStats.measure_time) {
			IO.toFile(paramStats.TimeToString(), Add, util.Names.timeFile);
		}
		if(ParametersStats.measure_scanrate) {
			IO.toFile(paramStats.ScanRateToString(), Add, util.Names.scanFile);
		}
		return knng;
	}
	
}
