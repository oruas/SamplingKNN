package launch;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import algo.BruteForce.BruteForce;
import algo.ClusterAndConquer.ClusterAndConquer;
import algo.ClusterAndConquer_rec.ClusterAndConquer_rec;
import algo.ClusterAndConquer_LSHhash.ClusterAndConquer_LSHhash;
import algo.ClusterAndConquer_ind_buckets.ClusterAndConquer_ind_buckets;
import algo.HybridOptiMem.HybridOptiMem;
import algo.Hybrid_MH.Hybrid_MH;
import algo.Hybrid_MH_LSHhash.Hybrid_MH_LSHhash;
import algo.Hybrid_MH_TP.Hybrid_MH_TP;
import algo.Hybrid_MH_ind_buckets.Hybrid_MH_ind_buckets;
import algo.Hybrid_first_version.Hybrid_first_version;
import algo.Hyrec.Hyrec;
import algo.LSH.LSH;
import algo.NNDescent.NNDescent;
import algo.Random.Random;
import cork.Cork;
import io.IO;
import parameters.Parameters;
import parameters.ParametersBruteForce;
import parameters.ParametersHybrid;
import parameters.ParametersHybridOptiMem;
import parameters.ParametersHybrid_MH;
import parameters.ParametersHybrid_MH_rec;
import parameters.ParametersHyrec;
import parameters.ParametersLSH;
import parameters.ParametersLSH_fast_hash;
import parameters.ParametersNNDescent;
import parameters.ParametersStats;
import util.KNNGraph;

public final class LaunchCork {

	public final static KNNGraph launch(ParametersBruteForce params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		BruteForce algo1 = new BruteForce(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);
		return knng;
	}
	

	
	public final static KNNGraph launch(ParametersHyrec params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hyrec algo1 = new Hyrec(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
	
		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	public final static KNNGraph launch(ParametersBruteForce params1, ParametersStats paramStats1, ParametersHyrec params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		BruteForce algo1 = new BruteForce(params1, paramStats1);
		Hyrec algo2 = new Hyrec(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);
		return knng;
	}	
	public final static KNNGraph launch(ParametersHyrec params1, ParametersStats paramStats1, ParametersHyrec params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hyrec algo1 = new Hyrec(params1, paramStats1);
		Hyrec algo2 = new Hyrec(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);

		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	

	
	public final static KNNGraph launch(ParametersNNDescent params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		NNDescent algo1 = new NNDescent(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);

		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	public final static KNNGraph launch(ParametersBruteForce params1, ParametersStats paramStats1, ParametersNNDescent params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		BruteForce algo1 = new BruteForce(params1, paramStats1);
		NNDescent algo2 = new NNDescent(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);
		return knng;
	}
	public final static KNNGraph launch(ParametersNNDescent params1, ParametersStats paramStats1, ParametersNNDescent params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		NNDescent algo1 = new NNDescent(params1, paramStats1);
		NNDescent algo2 = new NNDescent(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);

		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	

	public final static KNNGraph launchRandom(ParametersBruteForce params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Random algo1 = new Random(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);

		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);
		return knng;
	}
	public final static KNNGraph launchRandom(ParametersBruteForce params1, ParametersStats paramStats1, ParametersHyrec params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Random algo1 = new Random(params1, paramStats1);
		Hyrec algo2 = new Hyrec(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	public final static KNNGraph launchRandom(ParametersBruteForce params1, ParametersStats paramStats1, ParametersNNDescent params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Random algo1 = new Random(params1, paramStats1);
		NNDescent algo2 = new NNDescent(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	
	

	public final static KNNGraph launch(ParametersHyrec params1, ParametersStats paramStats1, ParametersNNDescent params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hyrec algo1 = new Hyrec(params1, paramStats1);
		NNDescent algo2 = new NNDescent(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	public final static KNNGraph launch(ParametersNNDescent params1, ParametersStats paramStats1, ParametersHyrec params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		NNDescent algo1 = new NNDescent(params1, paramStats1);
		Hyrec algo2 = new Hyrec(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate(paramStats1, paramStats2, Add);
		return knng;
	}
	
	
	
	


	public final static KNNGraph launch(ParametersLSH params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		LSH algo1 = new LSH(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);
		return knng;
	}
	public final static KNNGraph launch(ParametersLSH_fast_hash params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		algo.LSH_fast_hash.LSH algo1 = new algo.LSH_fast_hash.LSH(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);
		return knng;
	}
	


	public final static KNNGraph launch(ParametersHybrid params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hybrid_first_version algo1 = new Hybrid_first_version(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		
		return knng;
	}


	public final static KNNGraph launch(ParametersHybrid_MH params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hybrid_MH algo1 = new Hybrid_MH(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		
		return knng;
	}
	
	public final static KNNGraph launch_Hybrid_TP(ParametersHybrid_MH params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hybrid_MH_TP algo1 = new Hybrid_MH_TP(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		
		return knng;
	}
	
	public final static KNNGraph launch(ParametersHybrid_MH_rec params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		ClusterAndConquer algo1 = new ClusterAndConquer(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		
		return knng;
	}
	
	public final static KNNGraph launchCC_rec(ParametersHybrid_MH_rec params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		ClusterAndConquer_rec algo1 = new ClusterAndConquer_rec(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		
		return knng;
	}


	public final static KNNGraph launch(ParametersHybridOptiMem params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		HybridOptiMem algo1 = new HybridOptiMem(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params2.dataset(), Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		
		return knng;
	}
	



	public final static KNNGraph launch_Hybrid_MH_LSHhash(ParametersHybrid_MH params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hybrid_MH_LSHhash algo1 = new Hybrid_MH_LSHhash(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		return knng;
	}
	public final static KNNGraph launch_ClusterAndConquer_LSHhash(ParametersHybrid_MH_rec params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		ClusterAndConquer_LSHhash algo1 = new ClusterAndConquer_LSHhash(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		return knng;
	}


	public final static KNNGraph launch_Hybrid_MH_ind_buckets(ParametersHybrid_MH params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		Hybrid_MH_ind_buckets algo1 = new Hybrid_MH_ind_buckets(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		return knng;
	}

	public final static KNNGraph launch_ClusterAndConquer_ind_buckets(ParametersHybrid_MH params1, ParametersStats paramStats1, ParametersBruteForce params2, ParametersStats paramStats2, String Add) throws IOException {
		print_params(params1,params2,Add);
		
		ClusterAndConquer_ind_buckets algo1 = new ClusterAndConquer_ind_buckets(params1, paramStats1);
		BruteForce algo2 = new BruteForce(params2, paramStats2);
		Cork algo = new Cork(algo1, algo2);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
		
		print_time(paramStats1, paramStats2, Add);
		print_scanrate_BruteForce(paramStats1, paramStats2, Add);

		if(ParametersStats.measure_scanrate) {
			if(paramStats1.has_time_proc()) {
				IO.toFile(paramStats1.TimeNbProcToString(), Add, util.Names.timeProcFile);	
			}
			IO.toFile(paramStats1.timeBucketToString(), Add, util.Names.timeBucketFile);	
		}
		return knng;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public final static void print_params(Parameters params1, Parameters params2, String Add) throws IOException {
		IO.toFile(MapToString(params1.ParametersToMap(),params2.ParametersToMap(),params1.DatasetToMap(),params2.DatasetToMap()), Add, util.Names.paramFile);
//		IO.toFile(MapToString(params1.ParametersToMap(),params2.ParametersToMap()), Add, util.Names.paramFile);
//		IO.toFile(MapToString(params1.DatasetToMap(),params2.DatasetToMap()), Add, util.Names.paramDataFile);
//		IO.toFile(params1.DatasetToString(), Add, util.Names.paramDataFile1);
//		IO.toFile(params1.ParametersToString(), Add, util.Names.paramFile1);
//		IO.toFile(params2.DatasetToString(), Add, util.Names.paramDataFile2);
//		IO.toFile(params2.ParametersToString(), Add, util.Names.paramFile2);
	}
	
	public final static void print_time(ParametersStats paramStats1, ParametersStats paramStats2, String Add) throws IOException {
		if(ParametersStats.measure_time) {
			IO.toFile(paramStats1.TimeToString(), Add, util.Names.timeFile1);
			IO.toFile(paramStats2.TimeToString(), Add, util.Names.timeFile2);
			String s = "{\"" + util.Names.time + "\": " + Long.toString((paramStats1.get_time() + paramStats2.get_time())) + "}";
			IO.toFile(s, Add, util.Names.timeFile);
		}
	}

	
	public final static void print_scanrate_BruteForce(ParametersStats paramStats1, ParametersStats paramStats2, String Add) throws IOException {
		if(ParametersStats.measure_scanrate) {
			IO.toFile(paramStats1.ScanRateToString(), Add, util.Names.scanFile1);
			IO.toFile(paramStats2.ScanRateToString(), Add, util.Names.scanFile2);
		}
	}
	
	public final static void print_scanrate(ParametersStats paramStats1, ParametersStats paramStats2, String Add) throws IOException {
		if(ParametersStats.measure_scanrate) {
			IO.toFile(paramStats1.ScanRateToString(), Add, util.Names.scanFile1);
			IO.toFile(paramStats1.CompByIterToString(), Add, util.Names.compIterFile1);
			IO.toFile(paramStats1.ChangesByIterToString(), Add, util.Names.changeIterFile1);
			IO.toFile(paramStats2.ScanRateToString(), Add, util.Names.scanFile2);
			IO.toFile(paramStats2.CompByIterToString(), Add, util.Names.compIterFile2);
			IO.toFile(paramStats2.ChangesByIterToString(), Add, util.Names.changeIterFile2);
		}
	}
	
//	private final static String ParametersToString(Parameters params1, Parameters params2) {
//		Map<String,String> map = params2.ParametersToMap();
//		map.putAll(alterMapParameters(params1.ParametersToMap()));
//		return (ParametersToString(map));
//	}
	
	private final static String MapToString(Map<String,String> map1, Map<String,String> map2, Map<String,String> map3, Map<String,String> map4/*, boolean estimB*/) {
		map2.putAll(alterMapParameters(map1));
		map2.putAll(alterMapParameters(map3));
		map2.putAll(map4);
//		map2.put("estimB", Boolean.toString(estimB));
		return (Parameters.ParametersToString(map2));
	}
	
//	private final static String MapToString(Map<String,String> map1, Map<String,String> map2) {
//		map2.putAll(alterMapParameters(map1));
//		return (ParametersToString(map2));
//	}
	
	private final static Map<String,String> alterMapParameters(Map<String,String> map) {
		Map<String,String> new_map = new HashMap<String,String>();
		for (String param: map.keySet()) {
			if(!param.equals(util.Names.datasetName))
				new_map.put(param+"_preprocessing", map.get(param));
		}
		return new_map;
	}
	
//	private final static String ParametersToString(Map<String,String> map) {
//		String s = "{";
//		boolean b = true;
//		for(String name: map.keySet()) {
//			if (b) {
//				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
//				b = false;
//			}
//			else {
//				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
//			}
//		}
//		s = s + "}";
//		return s;
//	}
	
	
}
