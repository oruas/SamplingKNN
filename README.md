If you have any question, don't hesitate to email me: olivier dot ruas at inria dot fr

To compute a KNN graph, launch the main function in main.KNNmain.java
It will compute a KNN graph and the associated recommendations.

The library commons-math3-3.6.1 is required: http://commons.apache.org/proper/commons-math/download_math.cgi

Arguments:

-d or -dataset : chose the dataset used. The dataset must be localised in ./Files/Datasets/DatasetName/ the name should be TestSetX.data, the X part being the number of the testing set. Default movielens1M.
-part : the number of the testing set. Default 0.
-a or -algo : The algorithm used. Each algorithm has its own parameters. Possible values:
	BruteForce
	Hyrec params: -r -iter -delta
	NNDescent: -rho -iter -delta
	LSH: -LSHhash
	Random
	CorkAlgo1Algo2: will execute Algo1 with the dataset -ds_ to create a KNNG of size B (set by -B), and then execute Algo2 with the dataset -ds, starting from the KNNG obtained by Algo1.
		For example if we want to execute Hyrec, using the dataset LP but we want the final similarity being the regular one (e.g. to compute the quality), the parameters will be -a CorkHyrecBruteForce -ds_ LP.
-k : the size of the KNN graph.
-ds : the datastructure used. Each datastructure has its own parameters. Possible values:
	HashMap : only the positive ratings (>3), computes Jaccards
	HashMapFull : all the ratings, computes Jaccards (still on positive ratings)
	HashMapCosine : computes cosine, include all ratings.
	LP : keeps the s least popular items, -size to set s. Computes Jaccards.
	MP : keeps the s most popular items, -size to set s. Computes Jaccards.
	SCS : keeps the s random items, -size to set s. Computes Jaccards.
	ISCP : keeps every item in the item universe with prob p, -prob to set p. Computes Jaccards.
-add : the file in which the results will be written. The file will be in ./Files/results/
-s : to compute the scanrate
-rec : to desactivate the computation of recommendations
-t : to desactivate the measure of the computation time

To use ClusterAndConquer with 8 hash functions with 4096 buckets (before splitting) each, with a maximum size of 4000, and with GoldFinger of size 1024 we use:
-ds_ BitArrayNative -size 1024 -a CorkClusterAndConquer_recBruteForce -LSHhash 8 -LSHbuckets 4096 -LSHhash_try 4000
The use of CorkClusterAndConquer_recBruteForce ensures that the final KNN graphs will be rewritten with the real jaccard similarity and not the approximated one.

Organisation of the implementation:
-algo contains the interface Algo and several implementations of KNNG algorithms. They rely on the interface Parameters.
-dataset contains the interface Dataset and several implementations of datasets, they all have a function sim(u1,u2) which computes the similarity of 2 users u1 and u2.
	In particular, dataset.gen contains functions to transform the dataset to be in the good format, and functions to create the TestSets/TrainingSets.
-io contains IO functions.
-launch contains functions to launch the algorithms given some params.
-main contains the main functions to execute, including KNNmain.
-parameters contains the definitions of Parameters, which contains the parameters AND the datasets which caracterize the computation of a KNNG.
-recommendations contains all the functions to do item recommendations, given a KNNG.
-utils contains generic object like KNNG objects.



To download the movielens datasets: http://files.grouplens.org/datasets/movielens/
To download the AmazonMovies dataset: https://snap.stanford.edu/data/web-Amazon.html
